`timescale 1ns/1ps

/**
 *
 * generic_axis_hdshake_conv.v
 *
 * Short Description: The module implemented a protocol converter for bridging
 *  modules transmitting AXI-Stream transactions without tready sideband signal
 *  with modules receiving the transactions with tready sideband signal.
 *
 * Long Description: The module implemented a protocol converter for bridging
 *  modules transmitting AXI-Stream transactions without tready sideband signal
 *  with modules receiving the transactions with tready sideband signal. The
 *  disarming of handshake agreement of an AXI-Stream bus connection causes AXI-
 *  Stream transactions to drop gracefully when subsequent stages of AXI-Stream
 *  pipeline stall and unable to catch out-going transactions. The principle
 *  function of this module is to ensure atomicity of AXI-Stream transactions.
 *  It is required that the module immediately connecting this module be able to
 *  drop incompletely transmitted AXI-Stream transactions either automatically
 *  upon pipeline stall or by accepting the tdrop sideband channel provided by
 *  this module.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

/**
 *
 * GENERIC_AXIS_HDSHAKE_CONV
 *
 * GENERIC PARAMETERS
 * --------------------
 * AXIS_TDATA_WIDTH    THE NUMBER OF BYTES IN THE AXI-STREAM DATA CHANNEL
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_axis_*            VAR   INCOMING AXI-STREAM INTERFACE
 * m_axis_*            VAR   OUT-GOING AXI-STREAM INTERFACE
 * m_axis_tdrop        OUT   A SIDEBAND CHANNEL MOCKING TO BE A STANDARD AXI-
 *                            STREAM INTERFACE THAT MANDATES MODULE CONNECTED TO
 *                            THE AXI-STREAM BUS TO DROP INCOMPLETED AXI-STREAM
 *                            TRANSACTION, IF EXISTS, PRIOR TO THE CYCLE OF
 *                            ASSERTION
 *
 */
module generic_axis_hdshake_conv #(
        parameter AXIS_TDATA_WIDTH                          = 8
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]          s_axis_tdata,
        input  wire [AXIS_TDATA_WIDTH     - 1 : 0]          s_axis_tkeep,
        input  wire                                         s_axis_tlast,
        input  wire                                         s_axis_tvalid,
        output wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]          m_axis_tdata,
        output wire [AXIS_TDATA_WIDTH     - 1 : 0]          m_axis_tkeep,
        output wire                                         m_axis_tlast,
        output wire                                         m_axis_tdrop,
        input  wire                                         m_axis_tready,
        output wire                                         m_axis_tvalid
    );

    reg  [3 : 0]                                            STATE;
    localparam STATE_IDLE                                   = 4'b0000;
    localparam STATE_PASSTHROUGH                            = 4'b0001;
    localparam STATE_ONESHOT                                = 4'b0010;
    localparam STATE_LAST                                   = 4'b0011;
    localparam STATE_DROP_IDLE                              = 4'b0100;
    localparam STATE_DROP_LEADIN                            = 4'b0101;
    localparam STATE_DROP_PASSTHROUGH                       = 4'b0110;
    localparam STATE_DROP_ONESHOT                           = 4'b0111;
    localparam STATE_DROP_LAST                              = 4'b1000;
    localparam STATE_RESET                                  = 4'b1111;

    reg  [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     stage_axis_tdata;
    reg  [AXIS_TDATA_WIDTH     - 1 : 0]                     stage_axis_tkeep;
    reg                                                     stage_axis_tvalid;

// BEGIN

    initial
    begin
        STATE                                               = STATE_RESET;

        stage_axis_tdata                                    = {AXIS_TDATA_WIDTH{8'h0}};
        stage_axis_tkeep                                    = {AXIS_TDATA_WIDTH{1'b0}};
        stage_axis_tvalid                                   = 1'b0;
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0) begin
            stage_axis_tdata                                <= {AXIS_TDATA_WIDTH{8'h0}};
            stage_axis_tkeep                                <= {AXIS_TDATA_WIDTH{1'b0}};
            stage_axis_tvalid                               <= 1'b0;
        end
        else begin
            stage_axis_tdata                                <= s_axis_tdata;
            stage_axis_tkeep                                <= s_axis_tkeep;
            stage_axis_tvalid                               <= s_axis_tvalid;
        end
    end

    //
    //        clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    //      tdata |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //      tlast |________________________________/-------\_______________________________________
    //       full |________________________________/-------\_______________________________________
    //      tdrop |_______________________________________/-------\________________________________
    //      input |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //
    //        clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    //      tdata |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //      tlast |________________________________/---------------\_______________________________
    //       full |________________________________/-------\_______________________________________
    //      tdrop |_______________________________________/-------\________________________________
    //      input |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //
    //        clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    //      tdata |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //      tlast |________________________/-----------------------\_______________________________
    //       full |________________________________/-------\_______________________________________
    //      tdrop |_______________________________________/-------\________________________________
    //      input |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //
    //        clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    //      tdata |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //      tlast |________________________/---------------\_______________________________________
    //       full |________________________________/-------\_______________________________________
    //      tdrop |_______________________________________/-------\________________________________
    //      input |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //
    //        clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    //      tdata |<   0  ><   1  ><   2  ><   3  ><   4  ><   5  ><   6  ><   7  ><   8  ><   9  >
    //      tlast |________________________________________________/-------\_______________________
    //       full |________________________________/-------\_______________________________________
    //      tdrop |_______________________________________/-------\________________________________
    //      input |<   0  ><   1  ><   2  ><   3  ><   4  ><   X  ><   X  ><   7  ><   8  ><   9  >
    //
    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            STATE                                           <= STATE_RESET;
        else
            case(STATE)
                STATE_RESET:
                    STATE                                   <= STATE_IDLE;
                STATE_IDLE, STATE_DROP_IDLE:
                    if(s_axis_tvalid == 1'b1)
                        if(s_axis_tlast == 1'b1)
                            STATE                           <= STATE_ONESHOT;
                        else
                            STATE                           <= STATE_PASSTHROUGH;
                    else
                        STATE                               <= STATE_IDLE;
                STATE_PASSTHROUGH, STATE_DROP_LEADIN:
                    if(m_axis_tready == 1'b1) begin
                        if(s_axis_tvalid == 1'b1 && s_axis_tlast == 1'b1)
                            STATE                           <= STATE_LAST;
                        else
                            STATE                           <= STATE_PASSTHROUGH;
                    end
                    else
                        if(s_axis_tvalid == 1'b1 && s_axis_tlast == 1'b1)
                            STATE                           <= STATE_DROP_LAST;
                        else
                            STATE                           <= STATE_DROP_PASSTHROUGH;
                STATE_ONESHOT, STATE_LAST:
                    if(m_axis_tready == 1'b1)
                        if(s_axis_tvalid == 1'b1)
                            if(s_axis_tlast == 1'b1)
                                STATE                       <= STATE_ONESHOT;
                            else
                                STATE                       <= STATE_PASSTHROUGH;
                        else
                            STATE                           <= STATE_IDLE;
                    else
                        if(s_axis_tvalid == 1'b1)
                            if(s_axis_tlast == 1'b1)
                                STATE                       <= STATE_DROP_ONESHOT;
                            else
                                STATE                       <= STATE_DROP_LEADIN;
                        else
                            STATE                           <= STATE_DROP_IDLE;
                STATE_DROP_PASSTHROUGH:
                    if(s_axis_tvalid == 1'b1 && s_axis_tlast == 1'b1)
                        STATE                               <= STATE_DROP_LAST;
                STATE_DROP_ONESHOT, STATE_DROP_LAST:
                    if(s_axis_tvalid == 1'b1)
                        if(s_axis_tlast == 1'b1)
                            STATE                           <= STATE_ONESHOT;
                        else
                            STATE                           <= STATE_PASSTHROUGH;
                    else
                        STATE                               <= STATE_IDLE;
            endcase
    end

    assign m_axis_tdata                                     = STATE == STATE_PASSTHROUGH || STATE == STATE_ONESHOT || STATE == STATE_LAST
                                                                || STATE == STATE_DROP_LEADIN || STATE == STATE_DROP_ONESHOT
                                                                ? stage_axis_tdata : {AXIS_TDATA_WIDTH{8'h0}};
    assign m_axis_tkeep                                     = STATE == STATE_PASSTHROUGH || STATE == STATE_ONESHOT || STATE == STATE_LAST
                                                                || STATE == STATE_DROP_LEADIN || STATE == STATE_DROP_ONESHOT
                                                                ? stage_axis_tkeep : {AXIS_TDATA_WIDTH{1'b0}};
    assign m_axis_tlast                                     = STATE == STATE_LAST || STATE == STATE_ONESHOT;
    assign m_axis_tdrop                                     = STATE == STATE_DROP_IDLE || STATE == STATE_DROP_LEADIN
                                                                || STATE == STATE_DROP_ONESHOT || STATE == STATE_DROP_PASSTHROUGH
                                                                || STATE == STATE_DROP_LAST;
    assign m_axis_tvalid                                    = STATE == STATE_PASSTHROUGH || STATE == STATE_ONESHOT || STATE == STATE_LAST
                                                                || STATE == STATE_DROP_LEADIN || STATE == STATE_DROP_ONESHOT
                                                                ? stage_axis_tvalid : 1'b0;
endmodule