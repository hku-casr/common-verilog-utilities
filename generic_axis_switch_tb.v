`timescale 1ns/1ps

/**
 *
 * generic_axis_switch_tb.v
 *
 * Short Description:
 *
 * Long Description:
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

module generic_axis_switch_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    parameter CLK_PERIOD                                    = 10;
    parameter MSB_ALIGN                                     = 1;

// NOTE
// ====================
// MUTUALLY EXCLUSIVE SWITCH TO RUN THE SCHEDULE THAT PRODUCT BURSTY AXI STREAM
//  TRANSACTIONS OR ONE-BEAT TRANSACTIONS.
//
`define SCHEDULE_BURST
//`define SCHEDULE_SPARSE

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter NUM_WAY                                       = 2;
    parameter AXIS_TSBAND_ENABLE                            = 1;
    parameter AXIS_TWSBAND_ENABLE                           = 1;
    parameter AXIS_TDATA_WIDTH                              = 8;
    parameter AXIS_TDEST_WIDTH                              = 8;
    parameter AXIS_TSBAND_WIDTH                             = 32;
    parameter AXIS_TWSBAND_WIDTH                            = 32;

    reg                                                     clk;
    reg                                                     aresetn;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     in_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     in_axis_tkeep;
    wire                                                    in_axis_tlast;
`ifdef SCHEDULE_BURST
    wire [AXIS_TDEST_WIDTH     - 1 : 0]                     in_axis_tdest;
`elsif SCHEDULE_SPARSE
    reg  [AXIS_TDEST_WIDTH     - 1 : 0]                     in_axis_tdest;
`endif
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     in_axis_tsband;
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     in_axis_twsband;
    wire                                                    in_axis_tready;
    wire                                                    in_axis_tvalid;
    wire [NUM_WAY * AXIS_TDATA_WIDTH * 8 - 1 : 0]           tmp_axis_tdata;
    wire [NUM_WAY * AXIS_TDATA_WIDTH     - 1 : 0]           tmp_axis_tkeep;
    wire [NUM_WAY - 1 : 0]                                  tmp_axis_tlast;
    wire [NUM_WAY * AXIS_TSBAND_WIDTH    - 1 : 0]           tmp_axis_tsband;
    wire [NUM_WAY * AXIS_TWSBAND_WIDTH   - 1 : 0]           tmp_axis_twsband;
    wire [NUM_WAY - 1 : 0]                                  tmp_axis_tready;
    wire [NUM_WAY - 1 : 0]                                  tmp_axis_tvalid;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     out_axis_tdata[NUM_WAY - 1 : 0];
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     out_axis_tkeep[NUM_WAY - 1 : 0];
    wire                                                    out_axis_tlast[NUM_WAY - 1 : 0];
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     out_axis_tsband[NUM_WAY - 1 : 0];
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     out_axis_twsband[NUM_WAY - 1 : 0];
    reg                                                     out_axis_tready[NUM_WAY - 1 : 0];
    wire                                                    out_axis_tvalid[NUM_WAY - 1 : 0];

    integer                                                 schedule_j;

`ifdef SCHEDULE_BURST
    reg  [AXIS_TDEST_WIDTH      - 1 : 0]                    trans_dest[10 - 1 : 0];
`endif

// BEGIN

`include "axis_data_gen.vh"

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    // CYCLE-BASED STATIC SCHEDULE
    // ----------------------------------------
    //
`ifdef SCHEDULE_BURST
    // This testbench focuses on testing the channel switching capability of the
    //  module by streaming packets to different destination address and observe
    //  the atomicity of transactions.
    //
    initial
    begin
        schedule_j                                          = 0;

        out_axis_tready[0]                                  = 1'b0;
        out_axis_tready[1]                                  = 1'b0;

        trans_dest[0]                                       = 0;
        trans_dest[1]                                       = 1;
        trans_dest[2]                                       = 0;
        trans_dest[3]                                       = 0;
        trans_dest[4]                                       = 1;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
        endcase

        case(schedule_j - 15)
            8:  out_axis_tready[0]                          <= 1'b1;
            9:  out_axis_tready[1]                          <= 1'b1;
            18: out_axis_tready[1]                          <= 1'b0;
            19: out_axis_tready[1]                          <= 1'b1;
            27: out_axis_tready[1]                          <= 1'b0;
            28: out_axis_tready[1]                          <= 1'b1;
            36: out_axis_tready[0]                          <= 1'b0;
            37: out_axis_tready[0]                          <= 1'b1;
            43:
            begin
                out_axis_tready[0]                          <= 1'b0;
                out_axis_tready[1]                          <= 1'b0;
            end
            44:
            begin
                out_axis_tready[0]                          <= 1'b1;
                out_axis_tready[1]                          <= 1'b1;
            end
            52:
            begin
                out_axis_tready[0]                          <= 1'b0;
                out_axis_tready[1]                          <= 1'b0;
            end
            53:
            begin
                out_axis_tready[0]                          <= 1'b1;
                out_axis_tready[1]                          <= 1'b1;
            end
            60:
            begin
                out_axis_tready[0]                          <= 1'b0;
                out_axis_tready[1]                          <= 1'b0;
            end
            61:
            begin
                out_axis_tready[0]                          <= 1'b1;
                out_axis_tready[1]                          <= 1'b1;
            end
            67:
            begin
                out_axis_tready[0]                          <= 1'b0;
                out_axis_tready[1]                          <= 1'b0;
            end
            68:
            begin
                out_axis_tready[0]                          <= 1'b1;
                out_axis_tready[1]                          <= 1'b1;
            end
        endcase
    end
`elsif SCHEDULE_SPARSE
    // This testbench focuses on testing the channel integrity of the module by
    //  streaming short data stream while toggling the ready signal at the
    //  destination side to simulate unstable endpoint environment to ensure
    //  stateful responses.
    //
    initial
    begin
        schedule_j                                          = 0;

        out_axis_tready[0]                                  = 1'b1;
        out_axis_tready[1]                                  = 1'b1;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:
            begin
                aresetn                                     <= 1'b1;

                in_axis_tdest                               <= 0;
            end
            4:  out_axis_tready[0]                          <= 1'b0;
            5:  out_axis_tready[0]                          <= 1'b1;
            7:  out_axis_tready[0]                          <= 1'b0;
            9:  out_axis_tready[0]                          <= 1'b1;
            10: out_axis_tready[0]                          <= 1'b0;
            11: out_axis_tready[0]                          <= 1'b1;
            16: out_axis_tready[0]                          <= 1'b0;
            17: out_axis_tready[0]                          <= 1'b1;
            20: out_axis_tready[0]                          <= 1'b0;
            21: out_axis_tready[0]                          <= 1'b1;
            25: out_axis_tready[0]                          <= 1'b0;
            26: out_axis_tready[0]                          <= 1'b1;
            31: out_axis_tready[0]                          <= 1'b0;
            32: out_axis_tready[0]                          <= 1'b1;
            37: out_axis_tready[0]                          <= 1'b0;
            38: out_axis_tready[0]                          <= 1'b1;
            43: out_axis_tready[0]                          <= 1'b0;
            44: out_axis_tready[0]                          <= 1'b1;
            50: out_axis_tready[0]                          <= 1'b0;
            51: out_axis_tready[0]                          <= 1'b1;
            57: out_axis_tready[0]                          <= 1'b0;
            58: out_axis_tready[0]                          <= 1'b1;
            60: $stop;
        endcase
    end
`endif

`ifdef SCHEDULE_BURST
    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // A. FIRST STAGE, TESTS CHANNEL SWITCHING
    //  1. TRANSACTION 1, DESTINATION 0
    //  2. TRANSACTION 2, DESTINATION 1
    //
    // B. SECOND STAGE, TESTS FOR HANDLING OF READY SIGNAL TOGGLING
    //  3. 3RD PACKET (DUMMY DESTINATION 0)
    //      READY SIGNAL TOGGLES AT DESTINATION 0, SHOULD BLOCK TRANSMISSION
    //      FOR 1 CYCLE
    //  4. 4TH PACKET (DUMMY DESTINATION 1)
    //      READY SIGNAL TOGGLES AT DESTINATION 0, THE SIGNAL SHOULD NOT BLOCK
    //       TRANSMISSION AS READY ZEROED AT IRRELEVANT CHANNEL.
    //  5. 5TH PACKET (DUMMY DESTINATION 1)
    //      READY SIGNAL TOGGLES AT DESTINATION 1, SHOULD BLOCK TRANSMISSION
    //       FOR 1 CYCLE
    //
    always @(posedge clk)
    begin
        if(aresetn == 1'b0)
            `bus_null(0)
        else begin
            `bus_adv(0)

            case(`bus_step(0))
                0:  `bus_out(0, 44, 8 * AXIS_TDATA_WIDTH, 0)
                2:  `bus_out(0, 64, 7 * AXIS_TDATA_WIDTH, 0)
                4:  `bus_out(0, 89, 6 * AXIS_TDATA_WIDTH, 0)
                6:  `bus_out(0, 57, 9 * AXIS_TDATA_WIDTH, 0)
                8:  `bus_out(0, 26, 8 * AXIS_TDATA_WIDTH, 0)
                9:  `bus_out(0, 38, 5 * AXIS_TDATA_WIDTH, 0)
                10: `bus_out(0, 92, 9 * AXIS_TDATA_WIDTH, 0)
                11: `bus_out(0, 77, 3 * AXIS_TDATA_WIDTH, 0)
                30: $stop;
                default: `bus_null(0)
            endcase
        end
    end

    assign in_axis_tdest                                    = `trans_astep(0) < 0 || `trans_astep(0) - 1 >= 4
                                                                ? schedule_j % 2 : trans_dest[`trans_astep(0) - 1];
`elsif SCHEDULE_SPARSE
    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // A. FIRST STAGE, TESTS READY SIGNAL TOGGLING ON ONESHOT TRANSACTIONS
    //  1. TRANSACTION 1, READY = '0' BEFORE ONESHOT
    //  2. TRANSACTION 2, READY = '0' BEFORE TRANSACTION 3 ONESHOT
    // B. SECOND STAGE, TESTS READY SIGNAL TOGGLING ON DECODE-LAST TRANSACTIONS
    //  3. TRANSACTION 1, READY = '0' BEFORE ONESHOT
    //  4. TRANSACTION 2, READY = '0' BEFORE LAST
    //  5. TRANSACTION 3, READY = '0' ONE CYCLE OFF
    // C. THIRD STAGE, TESTS READY SIGNAL TOGGLING ON FULL SET (DECODE-PASS-
    //     THROUGH-LAST) TRANSACTIONS
    //  6. TRANSACTION 1, READY = '0' BEFORE DECODE
    //  7. TRANSACTION 2, READY = '0' BEFORE PASSTHROUGH BYTE 0
    //  8. TRANSACTION 3, READY = '0' BEFORE PASSTHROUGH BYTE 1
    //  9. TRANSACTION 4, READY = '0' BEFORE LAST
    //  10. TRANSACTION 5, READY = '0' ONE CYCLE OFF
    //
    always @(posedge clk)
    begin
        if(aresetn == 1'b0)
            `bus_null(0)
        else begin
            `bus_adv(0)

            case(`bus_step(0))
                0:  `bus_out(0, 24, 1 * AXIS_TDATA_WIDTH, 0)
                1:  `bus_out(0, 32, 1 * AXIS_TDATA_WIDTH, 0)
                3:  `bus_out(0, 72, 1 * AXIS_TDATA_WIDTH, 0)
                9:  `bus_out(0, 34, 2 * AXIS_TDATA_WIDTH, 0)
                10: `bus_out(0, 15, 2 * AXIS_TDATA_WIDTH, 0)
                12: `bus_out(0, 83, 2 * AXIS_TDATA_WIDTH, 0)
                18: `bus_out(0, 2, 4 * AXIS_TDATA_WIDTH, 0)
                19: `bus_out(0, 79, 4 * AXIS_TDATA_WIDTH, 0)
                20: `bus_out(0, 23, 4 * AXIS_TDATA_WIDTH, 0)
                22: `bus_out(0, 42, 4 * AXIS_TDATA_WIDTH, 0)
                24: `bus_out(0, 67, 4 * AXIS_TDATA_WIDTH, 0)
                default: `bus_null(0)
            endcase
        end
    end
`endif

    assign in_axis_tsband                                   = schedule_j;
    assign in_axis_twsband                                  = schedule_j;

    generic_axis_switch #(
        .NUM_WAY                                            (NUM_WAY),
        .AXIS_TSBAND_ENABLE                                 (AXIS_TSBAND_ENABLE),
        .AXIS_TWSBAND_ENABLE                                (AXIS_TWSBAND_ENABLE),
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH),
        .AXIS_TDEST_WIDTH                                   (AXIS_TDEST_WIDTH),
        .AXIS_TSBAND_WIDTH                                  (AXIS_TSBAND_WIDTH),
        .AXIS_TWSBAND_WIDTH                                 (AXIS_TWSBAND_WIDTH)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_axis_tdata                                       (in_axis_tdata),
        .s_axis_tkeep                                       (in_axis_tkeep),
        .s_axis_tlast                                       (in_axis_tlast),
        .s_axis_tdest                                       (in_axis_tdest),
        .s_axis_tsband                                      (in_axis_tsband),
        .s_axis_twsband                                     (in_axis_twsband),
        .s_axis_tready                                      (in_axis_tready),
        .s_axis_tvalid                                      (in_axis_tvalid),
        .m_axis_tdata                                       (tmp_axis_tdata),
        .m_axis_tkeep                                       (tmp_axis_tkeep),
        .m_axis_tlast                                       (tmp_axis_tlast),
        .m_axis_tsband                                      (tmp_axis_tsband),
        .m_axis_twsband                                     (tmp_axis_twsband),
        .m_axis_tready                                      (tmp_axis_tready),
        .m_axis_tvalid                                      (tmp_axis_tvalid)
    );

    axis_data_gen #(
        .MSB_ALIGN                                          (MSB_ALIGN),
        .CLK_ALIGN                                          (1),
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH)
    ) SIG_GEN (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .cmd_bus                                            (`cmd_bus(0)),
        .axis_tdata                                         (in_axis_tdata),
        .axis_tkeep                                         (in_axis_tkeep),
        .axis_tlast                                         (in_axis_tlast),
        .axis_tready                                        (in_axis_tready),
        .axis_tvalid                                        (in_axis_tvalid)
    );

    generate
        genvar siggen_i;

        for(siggen_i = 0; siggen_i < NUM_WAY; siggen_i = siggen_i + 1) begin
            assign out_axis_tdata[siggen_i]                 = tmp_axis_tdata[siggen_i * AXIS_TDATA_WIDTH * 8 +: AXIS_TDATA_WIDTH * 8];
            assign out_axis_tkeep[siggen_i]                 = tmp_axis_tkeep[siggen_i * AXIS_TDATA_WIDTH     +: AXIS_TDATA_WIDTH];
            assign out_axis_tlast[siggen_i]                 = tmp_axis_tlast[siggen_i];
            assign out_axis_tsband[siggen_i]                = tmp_axis_tsband[siggen_i * AXIS_TSBAND_WIDTH +: AXIS_TSBAND_WIDTH];
            assign out_axis_twsband[siggen_i]               = tmp_axis_twsband[siggen_i * AXIS_TWSBAND_WIDTH +: AXIS_TWSBAND_WIDTH];
            assign tmp_axis_tready[siggen_i]                = out_axis_tready[siggen_i];
            assign out_axis_tvalid[siggen_i]                = tmp_axis_tvalid[siggen_i];
        end
    endgenerate
endmodule