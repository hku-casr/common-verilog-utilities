`timescale 1ns/1ps

/**
 *
 * generic_axis_switch.v
 *
 * Short Description: The module switches incoming AXI Stream transactions into
 *  requested outgoing channel.
 *
 * Long Description: The module switches incoming AXI Stream transactions into
 *  requested outgoing channel. At the input end, a sideband channel is to take
 *  the output channel identifier associated with an transaction. The current
 *  implementation assumes the identifier is a consecutive range.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

/**
 *
 * GENERIC_AXIS_SWITCH
 *
 * GENERIC PARAMETERS
 * --------------------
 * NUM_WAY             THE NUMBER OF DRIVING OUTPUTS THE MODULE CAN SWITCH IN-
 *                      COMING TRANSACTIONS TO
 * AXIS_TSBAND_ENABLE  BOOLEAN VARIABLE TO ENABLE THE MODULE TO PROCESS CUSTOM
 *                      AXI-STREAM TRANSACTION-BASED SIDEBAND CHANNEL, TRANS-
 *                      ACTION-BASED SIDEBAND CHANNEL TRANSFERS VALUE THAT KEEPS
 *                      CONSTANT IN A TRANSACTION SPAN
 * AXIS_TWSBAND_ENABLE BOOLEAN VARIABLE TO ENABLE THE MODULE TO PROCESS CUSTOM
 *                      AXI-STREAM WORD-BASED SIDEBAND CHANNEL, WORD-BASED SIDE-
 *                      BAND CHANNEL RUNS VARYING DATA THAT ALIGNS TO INDIVIDUAL
 *                      AXI-STREAM WORDS ACROSS A TRANSACTION SPAN
 * AXIS_TDATA_WIDTH    THE NUMBER OF BYTES IN THE AXI-STREAM DATA CHANNEL
 * AXIS_TDEST_WIDTH    THE NUMBER OF BITS IN THE AXI-STREAM DEST SIDEBAND CHAN-
 *                      NEL
 * AXIS_TSBAND_WIDTH   THE NUMBER OF BITS IN THE CORRESPONDING TRANSACTION-BASED
 *                      SIDEBAND CHANNEL
 * AXIS_TWSBAND_WIDTH  THE NUMBER OF BITS IN THE CORRESPONDING WORD-BASED SIDE-
 *                      BAND CHANNEL
 * MULTISTAGE_ROUTE    BOOLEAN VARIABLE INSTRUCTING THE MODULE TO ROUTE INPUT
 *                      STREAM BY OBSERVING THE DEST SIDEBAND CHANNEL AS A RANGE
 * ROUTE_START_BIT     THE INDEX BIT OF THE STARTING LOCATION OF THE CONTENT IN
 *                      THE DEST SIDEBAND CHANNEL THE MODULE SHOULD HONOUR
 * ROUTE_BIT_WIDTH     THE BIT WIDTH OF THE CONTENT IN THE DEST SIDEBAND CHANNEL
 *                      THE MODULE SHOULD HONOUR
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    CLOCK IN 10 GIGABIT ETHERNET DOMAIN (156.25 MHZ)
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_axis_*            VAR   INCOMING AXI-STREAM INTERFACE
 * s_axis_tsband       IN    INCOMING CUSTOM AXI-STREAM TRANSACTION-BASED SIDE-
 *                            BAND CHANNEL, TRANSACTION-BASED SIDEBAND CHANNEL
 *                            TRANSFERS VALUES THAT REMAIN CONSTANT IN THE WHOLE
 *                            TRANSACTION SPAN
 * s_axis_twsband      IN    INCOMING CUSTOM AXI-STREAM WORD-BASED SIDEBAND
 *                            CHANNEL, WORD-BASED SIDEBAND CHANNEL RUNS VARYING
 *                            DATA THAT ALIGNS TO AXI-STREAM WORDS THROUGHOUT
 *                            THE TRANSACTION SPAN
 * m_axis_*            VAR   MULTIPLEXED OUT-GOING AXI-STREAM INTERFACE
 * m_axis_tsband       OUT   MULTIPLEXED OUT-GOING CUSTOM AXI-STREAM TRANSACTION
 *                            -BASED SIDEBAND CHANNEL
 * m_axis_twsband      OUT   MULTIPLEXED OUT-GOING CUSTOM AXI-STREAM WORD-BASED
 *                            SIDEBAND CHANNEL
 *
 */
module generic_axis_switch #(
        parameter NUM_WAY                                   = 2,
        parameter AXIS_TSBAND_ENABLE                        = 0,
        parameter AXIS_TWSBAND_ENABLE                       = 0,
        parameter AXIS_TDATA_WIDTH                          = 8,
        parameter AXIS_TDEST_WIDTH                          = 8,
        parameter AXIS_TSBAND_WIDTH                         = 64,
        parameter AXIS_TWSBAND_WIDTH                        = 64,
        parameter MULTISTAGE_ROUTE                          = 0,
        parameter ROUTE_START_BIT                           = 0,
        parameter ROUTE_BIT_WIDTH                           = 0
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]          s_axis_tdata,
        input  wire [AXIS_TDATA_WIDTH     - 1 : 0]          s_axis_tkeep,
        input  wire                                         s_axis_tlast,
        input  wire [AXIS_TDEST_WIDTH     - 1 : 0]          s_axis_tdest,
        input  wire [AXIS_TSBAND_WIDTH    - 1 : 0]          s_axis_tsband,
        input  wire [AXIS_TWSBAND_WIDTH   - 1 : 0]          s_axis_twsband,
        output wire                                         s_axis_tready,
        input  wire                                         s_axis_tvalid,
        output wire [NUM_WAY * AXIS_TDATA_WIDTH * 8 - 1 : 0]
                                                            m_axis_tdata,
        output wire [NUM_WAY * AXIS_TDATA_WIDTH     - 1 : 0]
                                                            m_axis_tkeep,
        output wire [NUM_WAY - 1 : 0]                       m_axis_tlast,
        output wire [NUM_WAY * AXIS_TDEST_WIDTH     - 1 : 0]
                                                            m_axis_tdest,
        output wire [NUM_WAY * AXIS_TSBAND_WIDTH    - 1 : 0]
                                                            m_axis_tsband,
        output wire [NUM_WAY * AXIS_TWSBAND_WIDTH   - 1 : 0]
                                                            m_axis_twsband,
        input  wire [NUM_WAY - 1 : 0]                       m_axis_tready,
        output wire [NUM_WAY - 1 : 0]                       m_axis_tvalid
    );

    localparam SWITCH_START_BIT                             = MULTISTAGE_ROUTE > 0 ? ROUTE_START_BIT : 0;
    localparam SWITCH_BIT_WIDTH                             = MULTISTAGE_ROUTE > 0 ? ROUTE_BIT_WIDTH : AXIS_TDEST_WIDTH;

    reg  [2 : 0] STATE                                      = 3'b0;
    localparam STATE_IDLE                                   = 3'b000;
    localparam STATE_ONESHOT                                = 3'b001;
    localparam STATE_PASSTHROUGH                            = 3'b010;
    localparam STATE_LAST                                   = 3'b011;
    localparam STATE_RESET                                  = 3'b111;

    reg  [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     stage_out_axis_tdata;
    reg  [AXIS_TDATA_WIDTH     - 1 : 0]                     stage_out_axis_tkeep;
    reg                                                     stage_out_axis_tlast;
    reg  [AXIS_TDEST_WIDTH     - 1 : 0]                     stage_out_axis_tdest;
    reg  [AXIS_TSBAND_WIDTH    - 1 : 0]                     stage_out_axis_tsband;
    reg  [AXIS_TWSBAND_WIDTH   - 1 : 0]                     stage_out_axis_twsband;
    reg                                                     stage_out_axis_tvalid;

// BEGIN

    initial
    begin
        stage_out_axis_tdata                                <= {AXIS_TDATA_WIDTH{8'h0}};
        stage_out_axis_tkeep                                <= {AXIS_TDATA_WIDTH{1'b0}};
        stage_out_axis_tlast                                <= 1'b0;
        stage_out_axis_tvalid                               <= 1'b0;
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0) begin
            stage_out_axis_tdata                            <= {AXIS_TDATA_WIDTH{8'h0}};
            stage_out_axis_tkeep                            <= {AXIS_TDATA_WIDTH{1'b0}};
            stage_out_axis_tlast                            <= 1'b0;
            stage_out_axis_tvalid                           <= 1'b0;
        end
        else
            if(STATE == STATE_IDLE || m_axis_tready[stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH]] == 1'b1) begin
                stage_out_axis_tdata                        <= s_axis_tdata;
                stage_out_axis_tkeep                        <= s_axis_tkeep;
                stage_out_axis_tlast                        <= s_axis_tlast;
                stage_out_axis_tvalid                       <= s_axis_tvalid;
            end
    end

    generate
        if(AXIS_TSBAND_ENABLE > 0) begin
            initial
                stage_out_axis_tsband                       = {AXIS_TSBAND_WIDTH{1'b0}};

            always @(posedge clk or negedge aresetn)
                if(aresetn == 1'b0)
                    stage_out_axis_tsband                   <= {AXIS_TSBAND_WIDTH{1'b0}};
                else
                    if(STATE == STATE_IDLE || (STATE == STATE_ONESHOT || STATE == STATE_LAST)
                        && m_axis_tready[stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH]] == 1'b1)
                        stage_out_axis_tsband               <= s_axis_tsband;
        end

        if(AXIS_TWSBAND_ENABLE > 0) begin
            initial
                stage_out_axis_twsband                      = {AXIS_TWSBAND_WIDTH{1'b0}};

            always @(posedge clk or negedge aresetn)
                if(aresetn == 1'b0)
                    stage_out_axis_twsband                  <= {AXIS_TWSBAND_WIDTH{1'b0}};
                else
                    if(STATE == STATE_IDLE || m_axis_tready[stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH]] == 1'b1)
                        stage_out_axis_twsband              <= s_axis_twsband;
        end
    endgenerate

    initial
        STATE                                               <= STATE_RESET;

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            STATE                                           <= STATE_RESET;
        else begin
            case(STATE)
                STATE_RESET:
                    STATE                                   <= STATE_IDLE;
                STATE_IDLE:
                    if(s_axis_tvalid == 1'b1) begin
                        if(s_axis_tlast == 1'b1)
                            STATE                           <= STATE_ONESHOT;
                        else
                            STATE                           <= STATE_PASSTHROUGH;

                        stage_out_axis_tdest                <= s_axis_tdest;
                    end
                STATE_PASSTHROUGH:
                    if(m_axis_tready[stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH]] == 1'b1)
                        if(s_axis_tvalid == 1'b1 && s_axis_tlast == 1'b1)
                            STATE                           <= STATE_LAST;
                STATE_ONESHOT, STATE_LAST:
                    if(m_axis_tready[stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH]] == 1'b1)
                        if(s_axis_tvalid == 1'b1) begin
                            if(s_axis_tlast == 1'b1)
                                STATE                       <= STATE_ONESHOT;
                            else
                                STATE                       <= STATE_PASSTHROUGH;

                            stage_out_axis_tdest            <= s_axis_tdest;
                        end
                        else
                            STATE                           <= STATE_IDLE;
            endcase
        end
    end

    generate
        genvar i;

        for (i = 0; i < NUM_WAY; i = i + 1) begin
            assign m_axis_tdata[i * AXIS_TDATA_WIDTH * 8 +: AXIS_TDATA_WIDTH * 8]
                                                            = stage_out_axis_tdata;
            assign m_axis_tkeep[i * AXIS_TDATA_WIDTH     +: AXIS_TDATA_WIDTH]
                                                            = stage_out_axis_tkeep;
            assign m_axis_tlast[i]                          = stage_out_axis_tlast;
            assign m_axis_tdest[i * AXIS_TDEST_WIDTH     +: AXIS_TDEST_WIDTH]
                                                            = stage_out_axis_tdest;
            assign m_axis_tvalid[i]                         = STATE != STATE_RESET && STATE != STATE_IDLE
                                                                ? stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH] == i
                                                                ? stage_out_axis_tvalid : 1'b0 : 1'b0;

            if(AXIS_TSBAND_ENABLE > 0)
                assign m_axis_tsband[i * AXIS_TSBAND_WIDTH +: AXIS_TSBAND_WIDTH]
                                                            = stage_out_axis_tsband;

            if(AXIS_TWSBAND_ENABLE > 0)
                assign m_axis_twsband[i * AXIS_TWSBAND_WIDTH +: AXIS_TWSBAND_WIDTH]
                                                            = stage_out_axis_twsband;
        end
    endgenerate

    assign s_axis_tready                                    = STATE == STATE_RESET ? 1'b0 : STATE == STATE_IDLE ? 1'b1
                                                                : stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH] < NUM_WAY
                                                                ? m_axis_tready[stage_out_axis_tdest[SWITCH_START_BIT +: SWITCH_BIT_WIDTH]] : 1'b1;
endmodule