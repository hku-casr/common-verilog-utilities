`timescale 1ns/1ps

/**
 *
 * miscellaneous_tb.v
 *
 * Short Description:
 *
 * Long Description:
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

module generic_timer_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    localparam CLK_PERIOD                                   = 10;

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    localparam MODE_WATCHDOG                                = 1;
    localparam DEFAULT_LIMIT                                = 4;
    localparam PULSE_HDSHAKE                                = 1;

    reg                                                     clk;
    reg                                                     aresetn;
    reg                                                     in_arm;
    reg                                                     in_disarm;
    reg  [`INTEGER_WIDTH * 8 - 1 : 0]                       in_limit;
    reg                                                     out_pulse_ack;
    wire                                                    out_pulse;
    wire                                                    out_assert;
    wire [`INTEGER_WIDTH * 8 - 1 : 0]                       var_timer_cycle;

    integer                                                 schedule_j;

// BEGIN

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    initial
    begin
        schedule_j                                          = 0;

        in_arm                                              = 1'b0;
        in_disarm                                           = 1'b0;
        in_limit                                            = {`INTEGER_WIDTH{8'h0}};

        out_pulse_ack                                       = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            4:  in_arm                                      <= 1'b1;
            6:  in_arm                                      <= 1'b0;
            13: out_pulse_ack                               <= 1'b1;
            15: out_pulse_ack                               <= 1'b0;
            20:
            begin
                in_arm                                      <= 1'b1;
                in_limit                                    <= 32'h0000008;
            end
            21: in_arm                                      <= 1'b0;
            29: out_pulse_ack                               <= 1'b1;
            30: out_pulse_ack                               <= 1'b0;
            31: in_arm                                      <= 1'b1;
            32: in_arm                                      <= 1'b0;
            35:
            begin
                in_arm                                      <= 1'b1;
                in_limit                                    <= 32'h0000002;
            end
            36: in_arm                                      <= 1'b0;
            40: $stop;
        endcase
    end

    generic_timer #(
        .MODE_WATCHDOG                                      (MODE_WATCHDOG),
        .DEFAULT_LIMIT                                      (DEFAULT_LIMIT),
        .PULSE_HDSHAKE                                      (PULSE_HDSHAKE)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_arm                                              (in_arm),
        .s_disarm                                           (in_disarm),
        .s_limit                                            (in_limit),
        .m_pulse_ack                                        (out_pulse_ack),
        .m_pulse                                            (out_pulse),
        .m_assert                                           (out_assert),
        .timer_cycle                                        (var_timer_cycle)
    );
endmodule

module generic_counter_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    localparam CLK_PERIOD                                   = 10;

    reg                                                     clk;
    reg                                                     aresetn;
    reg                                                     in_incre;
    reg                                                     in_reset;
    wire [`INTEGER_WIDTH * 8 - 1 : 0]                       var_counter_val;

    integer                                                 schedule_j;

// BEGIN

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    initial
    begin
        schedule_j                                          = 0;

        in_incre                                            = 1'b0;
        in_reset                                            = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            4:  in_incre                                    <= 1'b1;
            6:  in_incre                                    <= 1'b0;
            9:  in_incre                                    <= 1'b1;
            21: in_incre                                    <= 1'b0;
            23: in_reset                                    <= 1'b1;
            24: in_reset                                    <= 1'b0;
            31: in_incre                                    <= 1'b1;
            32: in_incre                                    <= 1'b0;
            33: in_incre                                    <= 1'b1;
            36: in_incre                                    <= 1'b0;
            40: $stop;
        endcase
    end

    generic_counter UUT(
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_incre                                            (in_incre),
        .s_reset                                            (in_reset),
        .counter_val                                        (var_counter_val)
    );
endmodule

module generic_SR_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    localparam CLK_PERIOD                                   = 10;

    reg                                                     clk;
    reg                                                     aresetn;
    reg                                                     in_set;
    reg                                                     in_reset;
    wire                                                    out_assert;

    integer                                                 schedule_j;

// BEGIN

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    initial
    begin
        schedule_j                                          = 0;

        in_set                                              = 1'b0;
        in_reset                                            = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            4:  in_set                                      <= 1'b1;
            5:  in_set                                      <= 1'b0;
            6:  in_set                                      <= 1'b1;
            7:  in_set                                      <= 1'b0;
            8:  in_reset                                    <= 1'b1;
            9:  in_reset                                    <= 1'b0;
            10: in_reset                                    <= 1'b1;
            11: in_reset                                    <= 1'b0;
            12: in_set                                      <= 1'b1;
            15: $stop;
        endcase
    end

    generic_SR UUT(
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_set                                              (in_set),
        .s_reset                                            (in_reset),
        .m_assert                                           (out_assert)
    );
endmodule

module generic_T_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    localparam CLK_PERIOD                                   = 10;

    reg                                                     clk;
    reg                                                     aresetn;
    reg                                                     in_set;
    wire                                                    out_assert;

    integer                                                 schedule_j;

// BEGIN

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    initial
    begin
        schedule_j                                          = 0;

        in_set                                              = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            4:  in_set                                      <= 1'b1;
            5:  in_set                                      <= 1'b0;
            6:  in_set                                      <= 1'b1;
            7:  in_set                                      <= 1'b0;
            8:  in_set                                      <= 1'b1;
            12: $stop;
        endcase
    end

    generic_T UUT(
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_set                                              (in_set),
        .m_assert                                           (out_assert)
    );
endmodule