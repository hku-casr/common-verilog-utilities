`timescale 1ns/1ps

/**
 *
 * generic_axis_fifo_tb.v
 *
 * Short Description: 
 *
 * Long Description: 
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

/**
 *
 * GENERIC_AXIS_FIFO_TB
 *
 * GENERIC PARAMETERS
 * --------------------
 * PREMATURE_DROP_EN   BOOLEAN VARIABLE TO DETERMINE THE AXI STREAM PRETENDING
 *                      SIDEBAND TDROP IS OBSERVED FOR DROPPING AN INCOMPLETED
 *                      AXI STREAM TRANSACTION STAGED INTO THE FIFO AND CONDUCT
 *                      RELEVANT ORCHESTRATED PROGRAM, PERFORM NORMAL PROGRAM
 *                      OTHERWISE
 *
 */
module generic_axis_fifo_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    parameter CLK_PERIOD                                    = 10;
    parameter MSB_ALIGN                                     = 1;

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter AXIS_TSBAND_ENABLE                            = 1;
    parameter AXIS_TWSBAND_ENABLE                           = 1;
    parameter BYTE_COUNT_ENABLE                             = 0;
    parameter BCCHNL_INDEPENDENT                            = 0;
    parameter AXIS_TDATA_WIDTH                              = 8;
    parameter AXIS_TSBAND_WIDTH                             = 32;
    parameter AXIS_TWSBAND_WIDTH                            = 32;
    parameter AXIS_XILINX_MODE                              = 0;
    parameter AXIS_MSB_ALIGN                                = 0;
    parameter DATA_BUFFER_DEPTH                             = 8;
    parameter VALID_TRANS_COUNT                             = 1;
    parameter PREMATURE_DROP_EN                             = 0;

    function integer GetWidth;
        input integer value;

        begin
            for(GetWidth = 0; value > 0; GetWidth = GetWidth + 1)
                value = value >> 1;
        end
    endfunction

    reg                                                     clk;
    reg                                                     aresetn;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     in_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     in_axis_tkeep;
    wire                                                    in_axis_tlast;
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     in_axis_tsband;
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     in_axis_twsband;
    reg                                                     in_axis_tdrop;
    wire                                                    in_axis_tready;
    wire                                                    in_axis_tvalid;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     out_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     out_axis_tkeep;
    wire                                                    out_axis_tlast;
    wire [GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH) - 1 : 0]
                                                            out_axis_tlen;
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     out_axis_tsband;
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     out_axis_twsband;
    reg                                                     out_axis_tready;
    wire                                                    out_axis_tvalid;
    wire                                                    out_axis_tlen_tready;
    wire                                                    out_axis_tlen_tvalid;
    wire [`INTEGER_WIDTH   * 8 - 1 : 0]                     axis_data_count;

    integer                                                 schedule_j;

// BEGIN

`include "axis_data_gen.vh"

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    // CYCLE-BASED STATIC SCHEDULE
    // ----------------------------------------
    //
    initial
    begin
        schedule_j                                          = 0;

        in_axis_tdrop                                       = 1'b0;

        out_axis_tready                                     = 1'b0;
    end

    generate
        if(PREMATURE_DROP_EN > 0)
            always @(posedge clk)
            begin
                schedule_j                                  <= schedule_j + 1;

                case(schedule_j)
                    3:  aresetn                             <= 1'b1;
                    17: in_axis_tdrop                       <= 1'b1;
                    18: in_axis_tdrop                       <= 1'b0;
                    21: in_axis_tdrop                       <= 1'b1;
                    22: in_axis_tdrop                       <= 1'b0;
                    27: in_axis_tdrop                       <= 1'b1;
                    28: in_axis_tdrop                       <= 1'b0;
                    31: in_axis_tdrop                       <= 1'b1;
                    32: in_axis_tdrop                       <= 1'b0;
                    36: out_axis_tready                     <= 1'b1;
                endcase
            end
        else
`define BEAT 1
           always @(posedge clk)
           begin
                schedule_j                                  <= schedule_j + 1;

               case(schedule_j)
                    3:  aresetn                             <= 1'b1;
                    5:  out_axis_tready                     <= 1'b0;
                    18: out_axis_tready                     <= 1'b1;
                    (20+`BEAT): out_axis_tready             <= 1'b0;
                    (21+`BEAT): out_axis_tready             <= 1'b1;
                    27: out_axis_tready                     <= 1'b0;
                    28: out_axis_tready                     <= 1'b1;
                    31: out_axis_tready                     <= 1'b0;
                    32: out_axis_tready                     <= 1'b1;
               endcase
           end

        if(BYTE_COUNT_ENABLE > 0 && BCCHNL_INDEPENDENT > 0)
            assign out_axis_tlen_tready                     = 1'b1;
    endgenerate

    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // TODO: DESCRIBE TRANSACTIONS
    // 1. TRANSACTION 1, READY = ...
    //
    generate
        if(PREMATURE_DROP_EN > 0)
            always @(posedge clk)
            begin
                if(aresetn == 1'b0)
                    `bus_null(0)
                else begin
                    `bus_adv(0)

                    case(`bus_step(0))
                        0:  `bus_out(0, 24, 6 * AXIS_TDATA_WIDTH, 0)
                        1:  `bus_out(0, 66, 5 * AXIS_TDATA_WIDTH, 0)
                        2:  `bus_out(0, 35, 7 * AXIS_TDATA_WIDTH, 0)
                        3:  `bus_out(0, 70, 4 * AXIS_TDATA_WIDTH, 0)
                        4:  `bus_out(0, 99, 5 * AXIS_TDATA_WIDTH, 0)
                        5:  `bus_out(0, 12, 5 * AXIS_TDATA_WIDTH, 0)
                        6:  `bus_out(0, 83, 6 * AXIS_TDATA_WIDTH, 0)
                        8:  `bus_out(0, 57, 7 * AXIS_TDATA_WIDTH, 0)
                        9:  `bus_out(0, 64, 6 * AXIS_TDATA_WIDTH, 0)
                        10: `bus_out(0, 39, 6 * AXIS_TDATA_WIDTH, 0)
                        11: `bus_out(0, 47, 5 * AXIS_TDATA_WIDTH, 0)
                        12: `bus_out(0, 86, 4 * AXIS_TDATA_WIDTH, 0)
                        default: `bus_null(0)
                    endcase
                end
            end
        else
            always @(posedge clk)
            begin
                if(aresetn == 1'b0)
                    `bus_null(0)
                else begin
                    `bus_adv(0)

                    case(`bus_step(0))
                        0:  `bus_out(0, 24, 4 * AXIS_TDATA_WIDTH - 3, 1)
                        1:  `bus_out(0, 66, 4 * AXIS_TDATA_WIDTH - 4, 2)
                        2:  `bus_out(0, 35, 4 * AXIS_TDATA_WIDTH - 4, 0)
                        3:  `bus_out(0, 70, 4 * AXIS_TDATA_WIDTH, 0)
                        4:  `bus_out(0, 99, 4 * AXIS_TDATA_WIDTH, 0)
                        5:  `bus_out(0, 12, 4 * AXIS_TDATA_WIDTH, 0)
                        6:  `bus_out(0, 83, 4 * AXIS_TDATA_WIDTH, 0)
                        8:  `bus_out(0, 57, 4 * AXIS_TDATA_WIDTH, 0)
                        9:  `bus_out(0, 64, 4 * AXIS_TDATA_WIDTH, 0)
                        10: `bus_out(0, 39, 4 * AXIS_TDATA_WIDTH, 0)
                        11: `bus_out(0, 47, 4 * AXIS_TDATA_WIDTH, 0)
                        12: `bus_out(0, 86, 4 * AXIS_TDATA_WIDTH, 0)
                        default: `bus_null(0)
                    endcase
                end
            end
    endgenerate

    assign in_axis_tsband                                   = schedule_j;
    assign in_axis_twsband                                  = schedule_j;

    generic_axis_fifo #(
        .AXIS_TSBAND_ENABLE                                 (AXIS_TSBAND_ENABLE),
        .AXIS_TWSBAND_ENABLE                                (AXIS_TWSBAND_ENABLE),
        .BYTE_COUNT_ENABLE                                  (BYTE_COUNT_ENABLE),
        .BCCHNL_INDEPENDENT                                 (BCCHNL_INDEPENDENT),
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH),
        .AXIS_TSBAND_WIDTH                                  (AXIS_TSBAND_WIDTH),
        .AXIS_TWSBAND_WIDTH                                 (AXIS_TWSBAND_WIDTH),
        .AXIS_XILINX_MODE                                   (AXIS_XILINX_MODE),
        .AXIS_MSB_ALIGN                                     (AXIS_MSB_ALIGN),
        .DATA_BUFFER_DEPTH                                  (DATA_BUFFER_DEPTH),
        .VALID_TRANS_COUNT                                  (VALID_TRANS_COUNT),
        .PREMATURE_DROP_EN                                  (PREMATURE_DROP_EN)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_axis_tdata                                       (in_axis_tdata),
        .s_axis_tkeep                                       (in_axis_tkeep),
        .s_axis_tlast                                       (in_axis_tlast),
        .s_axis_tsband                                      (in_axis_tsband),
        .s_axis_twsband                                     (in_axis_twsband),
        .s_axis_tdrop                                       (in_axis_tdrop),
        .s_axis_tready                                      (in_axis_tready),
        .s_axis_tvalid                                      (in_axis_tvalid),
        .m_axis_tdata                                       (out_axis_tdata),
        .m_axis_tkeep                                       (out_axis_tkeep),
        .m_axis_tlast                                       (out_axis_tlast),
        .m_axis_tlen                                        (out_axis_tlen),
        .m_axis_tsband                                      (out_axis_tsband),
        .m_axis_twsband                                     (out_axis_twsband),
        .m_axis_tready                                      (out_axis_tready),
        .m_axis_tvalid                                      (out_axis_tvalid),
        .m_axis_tlen_tready                                 (out_axis_tlen_tready),
        .m_axis_tlen_tvalid                                 (out_axis_tlen_tvalid),
        .axis_data_count                                    (axis_data_count)
    );

    axis_data_gen #(
        .MSB_ALIGN                                          (MSB_ALIGN),
        .CLK_ALIGN                                          (1),
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH)
    ) SIG_GEN (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .cmd_bus                                            (`cmd_bus(0)),
        .axis_tdata                                         (in_axis_tdata),
        .axis_tkeep                                         (in_axis_tkeep),
        .axis_tlast                                         (in_axis_tlast),
        .axis_tready                                        (in_axis_tready),
        .axis_tvalid                                        (in_axis_tvalid)
    );
endmodule