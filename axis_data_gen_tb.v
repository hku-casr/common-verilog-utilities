`timescale 1ns/1ps

/**
 *
 * axis_data_gen_tb.v
 *
 * Short Description: [SIMULATION Only] A testbench that demonstrates the inter-
 *  facing of the designed AXI Stream data generator core and the scheduling of
 *  data generation transactions for verification purposes.
 *
 * Long Description: [SIMULATION Only] A testbench that demonstrates the inter-
 *  facing of the designed AXI Stream data generator core and the scheduling of
 *  data generation transactions for verification purposes.
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

module axis_data_gen_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    parameter CLK_PERIOD                                    = 10;
    parameter MSB_ALIGN                                     = 1;

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter AXIS_TDATA_WIDTH                              = 8;
    parameter AXIS_TUSER_WIDTH                              = 4;

    reg                                                     clk;
    reg                                                     aresetn;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     in_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     in_axis_tkeep;
    wire                                                    in_axis_tlast;
    wire [AXIS_TUSER_WIDTH * AXIS_TDATA_WIDTH - 1 : 0]      in_axis_tuser;
    reg                                                     in_axis_tready;
    wire                                                    in_axis_tvalid;

    // DECLARING VARIABLE SUPPORTING THE COUNTING OF CYCLES FOR EASIER STATIC
    //  SCHEDULE ORCHESTRATION.
    //
    integer                                                 schedule_j;

    // DECLARING VARIABLE SUPPORTING HANDY RE-USE OF THE ARGUMENTS PROVIDED TO
    //  A BUS_OUT STEP.
    //
    integer                                                 param_start_value[64 - 1 : 0];
    integer                                                 param_byte_count[64 - 1 : 0];
    integer                                                 param_align_offset[64 - 1 : 0];

// BEGIN

`include "axis_data_gen.vh"

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    // CYCLE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // THE FOLLOWING BLOCKS INITIALISE AND PROVIDE A STATIC SCHEDULE OF SIGNAL
    //  CHANGES ALIGNED TO THE TESTBENCH CLOCK
    //
    initial
    begin
        schedule_j                                          = 0;

        in_axis_tready                                      = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            5:  in_axis_tready                              <= 1'b1;
            13: in_axis_tready                              <= 1'b0;
            14: in_axis_tready                              <= 1'b1;
            17: in_axis_tready                              <= 1'b0;
            18: in_axis_tready                              <= 1'b1;
        endcase
    end

    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // THE FOLLOWING BLOCKS INITIALISE AND PROVIDE AN ORDERED SCHEDULE OF AXI
    //  STREAM TRANSACTIONS ALIGNED TO THE TESTBENCH CLOCK. AN AXI STREAM TRAN-
    //  SACTION WILL ONLY START WHEN THE PRECEEDING TRANSACTION IS ACCEPTED.
    //
    //  THE BLOCK RELIES ON THE GENERATOR BLOCK "axis_data_gen" INCLUDED IN THE
    //  TESTBENCH AND A TESTBENCH CAN HAVE MULTIPLE GENERATOR BLOCK AND THEIR
    //  CORRESPONDING SCHEDULING. IT IS IMPORTANT TO MAKE SURE THE INDEXES OF
    //  INDIVIDUAL ELEMENTS IN THE SCHEDULING BLOCK, I.E., A <generator ID>
    //  AGREES TO THE GENERATOR BLOCK <generator ID>.
    //
    //  THE SCHEDULE BELOW USED A MIXED APPROACHE THAT PRESCRIBE A TRANSACTION
    //  THROUGH "bus_out" BY CONSTANT LITERALS IN ARGUMENT, AND BY SUPPORTING
    //  VARIABLE THAT CAN BE HANDILY RE-USED IN A SIDE CHANNEL, E.G., IN AN
    //  OCCASION THAT THE BYTE COUNT HAS TO BE PROVIDED IN THE AXI STREAM SIDE
    //  BUS, THE param_byte_count CAN BE ASSIGNED TO THE SIDE BUS.
    //
    initial
    begin
        param_start_value[4]                                = 55;
        param_byte_count[4]                                 = 2 * AXIS_TDATA_WIDTH;
        param_align_offset[4]                               = 0;

        param_start_value[5]                                = 70;
        param_byte_count[5]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[5]                               = 7;

        param_start_value[6]                                = 26;
        param_byte_count[6]                                 = 2 * AXIS_TDATA_WIDTH;
        param_align_offset[6]                               = 0;

        param_start_value[7]                                = 89;
        param_byte_count[7]                                 = 3 * AXIS_TDATA_WIDTH;
        param_align_offset[7]                               = 4;
    end

    always @(posedge clk)
    begin
        if(aresetn == 1'b0)
            `bus_null(0)
        else begin
            // "bus_adv(<generator ID>)" IS A MANDATORY MACRO CALL TO ADVANCE
            //  A SCHEDULE
            //
            `bus_adv(0)

            // "bus_step(<generator ID>)" IS A MANDATORY MACRO CALL TO REFER TO
            //  THE CURRENT STEP COUNT IN THE SCHEDULE OF THE PARIED GENERATOR
            //
            case(`bus_step(0))
                0: `bus_out(0, 64, 2 * AXIS_TDATA_WIDTH, 3)
                1: `bus_out(0, 30, 2 * AXIS_TDATA_WIDTH, 0)
                2: `bus_out(0, 85, 4 * AXIS_TDATA_WIDTH, 5)
                3: `bus_out(0, 12, 3 * AXIS_TDATA_WIDTH, 0)

                // "trans_step(<generator ID>)" IS AN OPTIONAL MACRO CALL THAT
                //  RETRIEVES THE COUNT OF SUCCEEDED AXI-STREAM TRANSACTIONS.
                //  THE COUNT ONLY COVERS REAL AXI-STREAM TRANSACTIONS, I.E.
                //  NON-BUS_NULL() CALLS
                //
                4: `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                5: `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                6: `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                8: `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                9: `bus_out(0, 52, 2 * AXIS_TDATA_WIDTH, 0)
                10: `bus_out(0, 0, 4 * AXIS_TDATA_WIDTH, 2)
                11: `bus_out(0, 37, 4 * AXIS_TDATA_WIDTH, 0)
                12: `bus_out(0, 48, 3 * AXIS_TDATA_WIDTH, 1)
                default: `bus_null(0)
            endcase
        end
    end

    // "trans_astep(<generator ID>)" PROVIDES ASYNCHRONOUS ACCESS TO SUCCEEDED
    //  TRANSACTION COUNTER. IT SHOULD BE NOTED THAT, SUCCEEDED TRANSACTION
    //  REFERS TO AXI STREAM TRANSACTION TRIGGERING COMMAND HANDSHAKE AND THE
    //  COUNTER ADVANCES TO THE NEXT VALUE AT THE CLOCK EDGE WHICH THE AXI
    //  STREAM STARTS AND THE ASSOCIATED COMMAND BEEN ACCEPTED AND LEAPS. THUS,
    //  THE CURRENT AXI STREAM TRANSACTION MAPS TO THE PREVIOUS COMMAND HAND-
    //  SHAKE IN TIME AND HENCE "trans_astep(<generator ID>) - 1" IS CODED.
    //
    //  THE NUMBER OF BYTES PARAMETER IS REUSED HERE AS ILLUSTRATION FOR THE
    //  TRANSACTIONS THAT LEVERAGED THE SUPPORTING VARIABLES FOR REUSE.
    //
    //  "bus_astep(<generator ID>)" IS ALSO PROVIDED FOR ASNCHRONOUS ACCESS TO
    //   THE SCHEDULE ITERATOR.
    //
    assign in_axis_tuser                                    = `trans_astep(0) - 1 >= 4 && `trans_astep(0) - 1 < 8
                                                                ? param_byte_count[`trans_astep(0) - 1] : 0;

    // AXI STREAM DATA GENERATOR
    // ----------------------------------------
    // THE GENERATOR CONSTRUCTS AN AXI STREAM TRANSACTION OF DESIGNATED NUMBER
    //  OF BYTES IN THE TRANSACTION AND STARTS THE VALID STREAM BYTES WITH THE
    //  PROVIDED cmd_start_value AND PURPOSEFULLY MISALIGNS THE STARTING BYTE
    //  AGAINST THE STARTING TRANSACTION WORD MOST-/LEAST-SIGNIFICANT BYTE BY
    //  ACCEPTING cmd_align_offset.
    //
    //  BY CONFIGURING MSB_ALIGN, CLK_ALIGN, THE DESIGN CAN ALIGN THE MOST-/
    //  LEAST-SIGNIFICANT BYTE TO THE LEFT, I.E., HIGHER BIT NUMBER AND ALIGN
    //  OPERATIONS AT POSITIVE/NEGATIVE EDGE OR BOTH EDGES.
    //
    //  FOR MAKING THE DATA STREAM MORE GENERIC, AXIS_TDATA_WIDTH IS USED TO
    //  INSTRUCT THE CORE THE BYTE WIDTH OF A TRANSACTION WORD, I.E., NUMBER
    //  OF BYTES HANDSHAKING IN-BETWEEN TWO CONSECUTIVE TRIGGERING EDGES.
    //
    //  IT SHOULD BE NOTED THAT, THE DESIGN MIGHT BE USED FOR NON-AXI STREAM
    //  DATA GENERATION AND THE BUS WIDTH CAN BE NON-ALIGNING TO BYTE WIDTH IN
    //  THE SENSE THAT AN EXTERNAL SIGNAL WITH WIDTH LESS THAN THE WIDTH OF THE
    //  AXI STREAM BYTE WIDTH CAN TAKE THE OUTPUT FROM THE CORE INTERFACE. THE
    //  GENERATOR SHOULD FITS ANY BUS/TRAFFIC THAT IS HANDSHAKING IN VALID/
    //  READY OR VALID/ACK.
    //
    //  A TESTBENCH CAN HAVE MULTIPLE GENERATOR BLOCK AND THEIR CORRESPONDING
    //  SCHEDULING, WITH A MAXIMUM HARDCODED AS 16 CURRENTLY IN THE ASSOCIATED
    //  VERILOG HEADER FILE THAT CAN BE CHANGED. IT IS IMPORTANT TO MAKE SURE
    //  THE INDEXES OF THE SIGNALS CONNECTING THE cmd_* INTERFACE REFERS TO THE
    //  CORRECT <generator ID> WITH THE INTENDED GENERATOR SCHEDULE. THE INTER-
    //  FACING SIGNALS NOW CONNECTED TO THE cmd_* PORTS ARE DECLARED IN THE
    //  ASSOCIATED VERILOG HEADER FILE AND IS REQUIRED TO BE USED AS-IS EXCEPT
    //  FOR THE INDEXES THAT SPECIFY THE RESPECTIVE <generator ID>.
    //
    axis_data_gen #(
        .MSB_ALIGN                                          (MSB_ALIGN),
        .CLK_ALIGN                                          (1),
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH)
    ) SIG_GEN (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .cmd_bus                                            (`cmd_bus(0)),
        .axis_tdata                                         (in_axis_tdata),
        .axis_tkeep                                         (in_axis_tkeep),
        .axis_tlast                                         (in_axis_tlast),
        .axis_tready                                        (in_axis_tready),
        .axis_tvalid                                        (in_axis_tvalid)
    );
endmodule