/**
 *
 * axis_data_gen.vh
 *
 * Short Description: [SIMULATION Only] The utility header file provide macro
 *  functions and declaration of connection wires to operate the AXI-Stream data
 *  generation module implemented in the associated utility source file.
 *
 * Long Description: [SIMULATION Only] The utility header file provide macro
 *  functions and declaration of connection wires to operate the AXI-Stream data
 *  generation module implemented in the associated utility source file.
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`define MAX_GENTOR 16
`define INTEGER_WIDTH 4

integer                                                     schedule_i[`MAX_GENTOR - 1 : 0];
integer                                                     transact_count[`MAX_GENTOR - 1 : 0];
wire [3 * `INTEGER_WIDTH * 8 + 2 - 1 : 0]                   axis_gen_cmd_bus[`MAX_GENTOR - 1 : 0];
reg  [`INTEGER_WIDTH * 8 - 1 : 0]                           counter_pass[`MAX_GENTOR - 1 : 0];
reg  [`INTEGER_WIDTH * 8 - 1 : 0]                           byte_count_pass[`MAX_GENTOR - 1 : 0];
reg  [`INTEGER_WIDTH * 8 - 1 : 0]                           align_offset_pass[`MAX_GENTOR - 1 : 0];
reg  [`MAX_GENTOR - 1 : 0]                                  gentor_enable;
wire [`MAX_GENTOR - 1 : 0]                                  gentor_ack;

/**
 *
 * BUS_ADV
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 *
 */
`define bus_adv(gentor_id)                                                      \
    begin                                                                       \
        if(gentor_ack[gentor_id] == 1'b1)                                       \
            schedule_i[gentor_id]                                               \
                                                  <= schedule_i[gentor_id] + 1; \
    end

/**
 *
 * BUS_STEP
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 *
 */
`define bus_step(gentor_id) (gentor_ack[gentor_id] == 1'b1 ? schedule_i[gentor_id] + 1 : schedule_i[gentor_id])

/**
 *
 * BUS_ASTEP
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 *
 */
`define bus_astep(gentor_id) (schedule_i[gentor_id])

/**
 *
 * BUS_OUT
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 * counter             IN    THE DATA GENERATED IN EACH BYTE OF THE STREAM WILL
 *                            BE INCREMENTED UPON THIS GIVEN BASE VALUE.
 * byte_count          IN    THE NUMBER OF VALID BYTES TO BE OUTPUT TO THE BUS
 *                            STREAM
 * align_offset        IN    THE OFFSET INDICATING PRECEDING INVALID BYTES IN A
 *                            DATA STREAM. IT IS ACCEPTABLED FOR AN OFFSET THAT
 *                            IS LARGER THAN THE NUMBER OF BYTES IN A SINGLE
 *                            WORD.
 *
 */
`define bus_out(gentor_id, counter, byte_count, align_offset)                   \
    begin                                                                       \
        gentor_enable[gentor_id]                            <= 1'b1;            \
                                                                                \
        counter_pass[gentor_id]                             <= counter;         \
        byte_count_pass[gentor_id]                          <= byte_count;      \
        align_offset_pass[gentor_id]                        <= align_offset;    \
                                                                                \
        if(gentor_enable[gentor_id] == 1'b1 && gentor_ack[gentor_id] == 1'b1)   \
            transact_count[gentor_id]                                           \
                                              <= transact_count[gentor_id] + 1; \
    end

/**
 *
 * BUS_NULL
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 *
 */
`define bus_null(gentor_id)                                                     \
    begin                                                                       \
        gentor_enable[gentor_id]                            <= 1'b0;            \
                                                                                \
        counter_pass[gentor_id]                             <= {32{1'b0}};      \
        byte_count_pass[gentor_id]                          <= {32{1'b0}};      \
        align_offset_pass[gentor_id]                        <= {32{1'b0}};      \
                                                                                \
        if(gentor_enable[gentor_id] == 1'b1 && gentor_ack[gentor_id] == 1'b1)   \
            transact_count[gentor_id]                                           \
                                              <= transact_count[gentor_id] + 1; \
    end

/**
 *
 * CMD_BUS
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 *
 */
`define cmd_bus(gentor_id) axis_gen_cmd_bus[gentor_id]

/**
 *
 * TRANS_STEP
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 *
 */
`define trans_step(gentor_id) (gentor_ack[gentor_id] == 1'b1 && gentor_enable[gentor_id] == 1'b1 ? transact_count[gentor_id] + 1 : transact_count[gentor_id])

/**
 *
 * TRANS_ASTEP
 *
 * INTERFACE PARAMETERS
 * --------------------
 * gentor_id           IN    DATA GENERATROR INDEX (CURRENT MAXIMUM INDEX: 15)
 *
 */
`define trans_astep(gentor_id) (transact_count[gentor_id])

generate
    genvar gentor_init_i;

    for(gentor_init_i = 0; gentor_init_i < `MAX_GENTOR; gentor_init_i = gentor_init_i + 1) begin
        initial begin
            schedule_i[gentor_init_i]                       = 0;
            transact_count[gentor_init_i]                   = 0;

            gentor_enable[gentor_init_i]                    = 0;

            counter_pass[gentor_init_i]                     = 0;
            byte_count_pass[gentor_init_i]                  = 0;
            align_offset_pass[gentor_init_i]                = 0;
        end

        assign axis_gen_cmd_bus[gentor_init_i][0]           = gentor_enable[gentor_init_i];
        assign gentor_ack[gentor_init_i]                    = axis_gen_cmd_bus[gentor_init_i][1];
        assign axis_gen_cmd_bus[gentor_init_i][2 +: `INTEGER_WIDTH * 8]
                                                            = counter_pass[gentor_init_i];
        assign axis_gen_cmd_bus[gentor_init_i][`INTEGER_WIDTH * 8 + 2 +: `INTEGER_WIDTH * 8]
                                                            = byte_count_pass[gentor_init_i];
        assign axis_gen_cmd_bus[gentor_init_i][2 * `INTEGER_WIDTH * 8 + 2 +: `INTEGER_WIDTH * 8]
                                                            = align_offset_pass[gentor_init_i];
    end
endgenerate