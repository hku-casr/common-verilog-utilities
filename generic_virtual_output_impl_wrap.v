`timescale 1ns/1ps

/**
 *
 * generic_virtual_output_impl_wrap.v
 *
 * Short Description: 
 *
 * Long Description: 
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

module generic_virtual_output_impl_wrap(
        input  wire                                         clk
    );

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter DATA_WIDTH                                    = 8;
    parameter DATA_DEPTH                                    = 256;
    parameter PROTO_HDSHAKE                                 = 1;
    parameter PROTO_HDSHAKE_INVAL                           = 0;
    parameter PROG_OUTPUT_FILE                              = "prog_file-counter.hex";

    wire                                                    aresetn;
    wire [DATA_WIDTH - 1 : 0]                               m_data;
    wire                                                    m_ready;
    wire                                                    m_valid;

// BEGIN

    generic_virtual_output #(
        .DATA_WIDTH                                         (DATA_WIDTH),
        .DATA_DEPTH                                         (DATA_DEPTH),
        .PROTO_HDSHAKE                                      (PROTO_HDSHAKE),
        .PROTO_HDSHAKE_INVAL                                (PROTO_HDSHAKE_INVAL),
        .PROG_OUTPUT_FILE                                   (PROG_OUTPUT_FILE)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .m_data                                             (m_data),
        .m_ready                                            (m_ready),
        .m_valid                                            (m_valid)
    );

    vio_generic_virtual_output line_keeper(
        .clk                                                (clk),
        .probe_out0                                         (aresetn),
        .probe_in0                                          (m_data),
        .probe_out1                                         (m_ready),
        .probe_in1                                          (m_valid)
    );

    ila_generic_virtual_output line_watcher(
        .clk                                                (clk),
        .probe0                                             (aresetn),
        .probe1                                             (m_data),
        .probe2                                             (m_ready),
        .probe3                                             (m_valid)
    );
endmodule