`timescale 1ns/1ps

/**
 *
 * generic_bram.v
 *
 * Short Description: This module implemented a index-addressed memory structure
 *  with optional valid bit for identifying cold memory and is targeted for on-
 *  board block RAMs.
 *
 * Long Description: This module implemented a index-addressed memory structure
 *  with optional valid bit for identifying cold memory and is targeted for on-
 *  board block RAMs.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

/**
 *
 * GENERIC_BRAM
 *
 * GENERIC PARAMETERS
 * --------------------
 * USE_VALID_BIT       BOOLEAN VARIABLE TO DETERMINE IF VALID BIT IS USED IN
 *                      THE BACK-END MEMORY REFLECTING ANY UNIQUE ADDRESS IS
 *                      PROPERLY INITIALISED OR RESIDING VALID DATA
 * MEM_ADDR_WIDTH      THE BIT WIDTH OF THE ADDRESS FOR ACCESSING MEMORY CON-
 *                      TENT FROM A BACK-END MEMORY
 * MEM_DATA_WIDTH      THE BIT WIDTH OF THE COMPLETE DATA CONTENT IN THE BACK-
 *                      END MEMORY REGARDLESS OF THEIR RELEVANCE TO THE SEARCH
 *                      OR INTENDED CONTENT
 * MEM_INITIALISE      BOOLEAN VARIABLE TO DICTATE THE MEMORY TO BE INITIALISED
 *                      FROM THE MEMORY CONFIGURATION FILE PRESENTED
 * MEM_CONF_FILE       MEMORY CONFIGURATION FILE, A BLOCK RAM .HEX MEMORY DUMP
 *                      FILE THIS BLOCK RAM IS INTENDED TO INITIALISED TO
 *
 * INTERFACE PINS
 * --------------------
 * clk*                IN    DRIVING CLOCK FOR THE MEMORY INTERFACE OF THE RES-
 *                            PECTIVE PORT
 * port*_addr          IN    THE ADDRESS OF THE ACCESSING MEMORY LINE
 * port*_en            IN    BOOLEAN SIGNAL MARKING A DRIVING-IN TRANSACTION IN
 *                            THE BUS AS A VALID TRANSACTION
 * port*_wr_en         IN    BOOLEAN SIGNAL MARKING A DRIVING-IN TRANSACTION IN
 *                            THE BUS AS A WRITE TRANSACTION
 * port*_data_in       IN    DATA INTENDED TO RESIDE AT THE MEMORY LOCATION
 *                            PROMPTED BY port*_addr
 * port*_data_out      OUT   DATA FETCHED TO FROM THE MEMORY LOCATION PROMPTED 
 *                            BY port*_addr
 *
 */
module generic_bram #(
        parameter USE_VALID_BIT                             = 1,
        parameter MEM_ADDR_WIDTH                            = 2,
        parameter MEM_DATA_WIDTH                            = 32,
        parameter MEM_INITIALISE                            = 1,
        parameter MEM_CONF_FILE                             = "mem_conf.hex"
    )
    (
        input  wire                                         clka,
        input  wire [MEM_ADDR_WIDTH - 1 : 0]                porta_addr,
        input  wire                                         porta_en,
        input  wire                                         porta_wr_en,
        input  wire [MEM_DATA_WIDTH + (USE_VALID_BIT > 0 ? 1 : 0) - 1 : 0]
                                                            porta_data_in,
        output wire [MEM_DATA_WIDTH + (USE_VALID_BIT > 0 ? 1 : 0) - 1 : 0]
                                                            porta_data_out,

        input  wire                                         clkb,
        input  wire [MEM_ADDR_WIDTH - 1 : 0]                portb_addr,
        input  wire                                         portb_en,
        input  wire                                         portb_wr_en,
        input  wire [MEM_DATA_WIDTH + (USE_VALID_BIT > 0 ? 1 : 0) - 1 : 0]
                                                            portb_data_in,
        output wire [MEM_DATA_WIDTH + (USE_VALID_BIT > 0 ? 1 : 0) - 1 : 0]
                                                            portb_data_out
    );

/**
 *
 *      A+1      A                  0
 * INDEX | VALID |      CONTENT
 *  ...  |  ...  |        ...
 *  205  |   1   | 00:01:02:03:04:05
 *  211  |   1   | 00:01:02:03:05:01
 *  255  |   0   | FF:FF:FF:FF:FF:FF
 *
 * NOTE: A IS MEM_DATA_WIDTH. THE VALID BIT ONLY PRESENTS IF USE_VALID_BIT IS
 *  PROVIDED AS NON-ZERO.
 *
 */

    (* RAM_STYLE = "BLOCK" *)
    reg  [MEM_DATA_WIDTH + (USE_VALID_BIT > 0 ? 1 : 0) - 1 : 0]
                                                            mem_array[2 ** MEM_ADDR_WIDTH - 1 : 0];

    reg  [MEM_ADDR_WIDTH - 1 : 0]                           stage_porta_addr;
    reg  [MEM_ADDR_WIDTH - 1 : 0]                           stage_portb_addr;

// BEGIN

    generate
        genvar init_i;

        if(MEM_INITIALISE != 0)
            initial $readmemh(MEM_CONF_FILE, mem_array);
        else
            for(init_i = 0; init_i < 2 ** MEM_ADDR_WIDTH; init_i = init_i + 1)
                initial mem_array[init_i]                   = {MEM_DATA_WIDTH{1'b0}};
    endgenerate

    always @(posedge clka) begin
        if(porta_en) begin
            stage_porta_addr                                <= porta_addr;

            if(porta_wr_en)
                mem_array[porta_addr]                       <= porta_data_in;
        end
    end

    always @(posedge clkb) begin
        if(portb_en) begin
            stage_portb_addr                                <= portb_addr;

            if(portb_wr_en)
                mem_array[portb_addr]                       <= portb_data_in;
        end
    end

    assign porta_data_out                                   = mem_array[stage_porta_addr];
    assign portb_data_out                                   = mem_array[stage_portb_addr];
endmodule