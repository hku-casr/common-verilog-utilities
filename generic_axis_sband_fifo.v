`timescale 1ns/1ps

/**
 *
 * generic_axis_sband_fifo.v
 *
 * Short Description: The module implemented a First-in-first-out buffering
 *  module to bypass or relay transaction-aligned sideband channels to their
 *  corresponding AXI Stream transactions in AXI Stream protocol.
 *
 * Long Description: The module implemented a First-in-first-out buffering
 *  module to bypass or relay transaction-aligned sideband channels to their
 *  corresponding AXI Stream transactions in AXI Stream protocol. The module is
 *  designed for a full system pipeline which individual submodules along the
 *  pipeline might not handle all non-mandated sidebands channels input from any
 *  pipeline submodules ahead but such sidebands are required as output of the
 *  pipeline or required as input to subsequent submodules.
 *
 *  The module targets sideband channel of content that is transaction-aligned,
 *  e.g., axis_tdest destination sideband channel that hints the destination
 *  element of a routing transaction as a whole, as oppose to word-aligned or
 *  byte-aligned sideband channels, e.g. axis_tuser that passes data that varies
 *  according to individual bytes. The module eavesdrops the input and output
 *  AXI Stream channels of the module it runs parallelly with for AXI Stream
 *  stateful information and bypasses sideband through this module, aligned to
 *  its original AXI-S transaction that passes through the module not handling
 *  the sideband.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

/**
 *
 * GENERIC_AXIS_SBAND_FIFO
 *
 * GENERIC PARAMETERS
 * --------------------
 * AXIS_TSBAND_WIDTH   THE BIT WIDTH OF THE SIDEBAND CHANNEL THE MODULE INTENDS
 *                      TO RELAY
 * DATA_BUFFER_DEPTH   THE NUMBER OF TRANSACTION WORDS THAT CAN BE BUFFERED BY
 *                      THE FIFO
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_axis_tsideband    IN    INCOMING AXI-STREAM SIDEBAND CHANNEL SYNCHRONOUS TO
 *                            THE TAPPED INCOMING AXI-STREAM INTERFACE
 * m_axis_tsideband    OUT   OUT-GOING AXI-STREAM SIDEBAND CHANNEL SYNCHRONOUS
 *                            TO THE TAPPED OUT-GOING AXI-STREAM INTERFACE
 * tap_s_axis_*        IN    TAPPING INCOMING AXI-STREAM INTERFACE
 * tap_m_axis_*        IN    TAPPING OUT-GOING AXI-STREAM INTERFACE
 * axis_trans_count    OUT THE NUMBER OF TRANSACTIONS STORED IN THE AXI STREAM
 *                            FIFO
 *
 */
module generic_axis_sband_fifo #(
        parameter AXIS_TSBAND_WIDTH                         = 1,
        parameter DATA_BUFFER_DEPTH                         = 10
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire [AXIS_TSBAND_WIDTH  - 1 : 0]            s_axis_tsideband,
        output wire [AXIS_TSBAND_WIDTH  - 1 : 0]            m_axis_tsideband,
        input  wire                                         tap_s_axis_tlast,
        input  wire                                         tap_s_axis_tready,
        input  wire                                         tap_s_axis_tvalid,
        input  wire                                         tap_m_axis_tlast,
        input  wire                                         tap_m_axis_tready,
        input  wire                                         tap_m_axis_tvalid,
        output wire [`INTEGER_WIDTH * 8 - 1 : 0]            axis_trans_count
    );

    // DATA_OVERF_DEPTH MUST BE GREATER THAN 2 OR THE STOP CONDITION OF THE
    //  STATE MACHINE WILL ALWAYS BE MET, I.E., HITTING OVERFLOW CONTINUE PATH
    //  fifo_axis_toverf == 0 && fifo_read_overf == DATA_OVERF_DEPTH - 1
    //
    localparam DATA_OVERF_DEPTH                             = 3;

    function integer GetWidth;
        input integer value;

        begin
            for(GetWidth = 0; value > 0; GetWidth = GetWidth + 1)
                value = value >> 1;
        end
    endfunction

    reg  [2 : 0]                                            IN_STATE;
    localparam IN_STATE_IDLE                                = 3'b000;
    localparam IN_STATE_DECODE                              = 3'b001;
    localparam IN_STATE_PASS                                = 3'b010;
    localparam IN_STATE_WAIT                                = 3'b011;
    localparam IN_STATE_ONESHOT                             = 3'b100;
    localparam IN_STATE_LAST                                = 3'b101;
    localparam IN_STATE_RESET                               = 3'b111;

    reg  [AXIS_TSBAND_WIDTH           - 1 : 0]              stage_in_axis_tsband;

    // FIFO IN CIRCULAR MEMORY IMPLEMENTATION
    //
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_write_ptr;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          stage_fifo_write_ptr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_write_overf;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_write_tptr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_write_toverf;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_read_ptr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_read_overf;

    // INTERNAL TREADY SIGNAL FOR INPUT FLOW CONTROL, INTERMEDIATE FIFO FLOW
    //  CONTROL AND OUTPUT FLOW CONTROL
    //
    wire                                                    fifo_in_tready;
    wire                                                    fifo_full;
    wire                                                    fifo_out_tempty;
    wire                                                    fifo_out_tfull;
    wire                                                    fifo_out_tvalid;

    reg  [1 : 0]                                            FETCH_STATE;
    localparam FETCH_STATE_IDLE                             = 2'b00;
    localparam FETCH_STATE_PASSTHROUGH                      = 2'b01;
    localparam FETCH_STATE_RESET                            = 2'b11;

    wire [AXIS_TSBAND_WIDTH           - 1 : 0]              relay_fetch_axis_tsband;

    reg  [1 : 0]                                            STAB_STACK;
    reg  [AXIS_TSBAND_WIDTH           - 1 : 0]              pf_fetch_axis_tsband[1 : 0];

    wire                                                    relay_stab_fetch_valid;
    wire                                                    relay_stab_fetch_greedy_ready;
    wire                                                    relay_stab_fetch_ready;

    reg  [1 : 0]                                            OUT_STATE;
    localparam OUT_STATE_IDLE                               = 2'b00;
    localparam OUT_STATE_PASSTHROUGH                        = 2'b01;
    localparam OUT_STATE_RESET                              = 2'b11;

    wire                                                    relay_out_stab_valid;
    wire                                                    relay_out_stab_ready;

// BEGIN

    initial
    begin
        IN_STATE                                            = IN_STATE_RESET;

        fifo_write_ptr                                      = 0;
        fifo_write_overf                                    = 0;

        fifo_write_tptr                                     = 0;
        fifo_write_toverf                                   = 0;

        stage_fifo_write_ptr                                = 0;
        stage_in_axis_tsband                                = {AXIS_TSBAND_WIDTH{1'b0}};
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            IN_STATE                                        <= IN_STATE_RESET;
        else
            case(IN_STATE)
                IN_STATE_RESET:
                begin
                    IN_STATE                                <= IN_STATE_IDLE;

                    fifo_write_ptr                          <= 0;
                    fifo_write_overf                        <= 0;

                    fifo_write_tptr                         <= 0;
                    fifo_write_toverf                       <= 0;

                    stage_fifo_write_ptr                    <= 0;
                    stage_in_axis_tsband                    <= {AXIS_TSBAND_WIDTH{1'b0}};
                end
                IN_STATE_IDLE:
                    if(fifo_in_tready == 1'b1 && tap_s_axis_tvalid == 1'b1) begin
                        if(tap_s_axis_tlast == 1'b1)
                            IN_STATE                        <= IN_STATE_ONESHOT;
                        else
                            IN_STATE                        <= IN_STATE_DECODE;

                        stage_fifo_write_ptr                <= fifo_write_ptr;
                        stage_in_axis_tsband                <= s_axis_tsideband;

                        fifo_write_ptr                      <= (fifo_write_ptr + 1) % DATA_BUFFER_DEPTH;

                        if((fifo_write_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                            fifo_write_overf                <= (fifo_write_overf + 1) % DATA_OVERF_DEPTH;
                    end
                IN_STATE_DECODE:
                    if(tap_s_axis_tready == 1'b1) begin
                        if(tap_s_axis_tvalid == 1'b1 && tap_s_axis_tlast == 1'b1)
                            IN_STATE                        <= IN_STATE_LAST;
                        else
                            IN_STATE                        <= IN_STATE_PASS;

                        fifo_write_tptr                     <= fifo_write_ptr;
                        fifo_write_toverf                   <= fifo_write_overf;
                    end
                    else
                        if(tap_s_axis_tvalid == 1'b1) begin
                            if(tap_s_axis_tlast == 1'b1)
                                IN_STATE                    <= IN_STATE_ONESHOT;
                            else
                                IN_STATE                    <= IN_STATE_DECODE;

                            stage_in_axis_tsband            <= s_axis_tsideband;
                        end
                        else
                            IN_STATE                        <= IN_STATE_IDLE;
                IN_STATE_PASS:
                    if(tap_s_axis_tready == 1'b1)
                        if(tap_s_axis_tvalid == 1'b1 && tap_s_axis_tlast == 1'b1)
                            IN_STATE                        <= IN_STATE_LAST;
                        else
                            IN_STATE                        <= IN_STATE_PASS;
                IN_STATE_ONESHOT:
                    if(tap_s_axis_tready == 1'b1) begin
                        if(fifo_in_tready == 1'b1 && tap_s_axis_tvalid == 1'b1) begin
                            if(tap_s_axis_tlast == 1'b1)
                                IN_STATE                    <= IN_STATE_ONESHOT;
                            else
                                IN_STATE                    <= IN_STATE_DECODE;

                            stage_fifo_write_ptr            <= fifo_write_ptr;
                            stage_in_axis_tsband            <= s_axis_tsideband;

                            fifo_write_ptr                  <= (fifo_write_ptr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_write_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_write_overf            <= (fifo_write_overf + 1) % DATA_OVERF_DEPTH;
                        end
                        else
                            IN_STATE                        <= IN_STATE_IDLE;

                        fifo_write_tptr                     <= fifo_write_ptr;
                        fifo_write_toverf                   <= fifo_write_overf;
                    end
                    else
                        if(tap_s_axis_tvalid == 1'b1) begin
                            if(tap_s_axis_tlast == 1'b1)
                                IN_STATE                    <= IN_STATE_ONESHOT;
                            else
                                IN_STATE                    <= IN_STATE_DECODE;

                            stage_in_axis_tsband            <= s_axis_tsideband;
                        end
                        else
                            IN_STATE                        <= IN_STATE_IDLE;
                IN_STATE_LAST:
                    if(tap_s_axis_tready == 1'b1)
                        if(fifo_in_tready == 1'b1 && tap_s_axis_tvalid == 1'b1) begin
                            if(tap_s_axis_tlast == 1'b1)
                                IN_STATE                    <= IN_STATE_ONESHOT;
                            else
                                IN_STATE                    <= IN_STATE_DECODE;

                            stage_fifo_write_ptr            <= fifo_write_ptr;
                            stage_in_axis_tsband            <= s_axis_tsideband;

                            fifo_write_ptr                  <= (fifo_write_ptr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_write_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_write_overf            <= (fifo_write_overf + 1) % DATA_OVERF_DEPTH;
                        end
                        else
                            IN_STATE                        <= IN_STATE_IDLE;
            endcase
    end

    assign fifo_in_tready                                   = (IN_STATE != IN_STATE_RESET && OUT_STATE != OUT_STATE_RESET)
                                                                & !((fifo_write_ptr == fifo_read_ptr)
                                                                && (fifo_write_overf != fifo_read_overf));
    assign fifo_full                                        = (fifo_write_ptr == fifo_read_ptr)
                                                                && (fifo_write_overf != fifo_read_overf);

    generic_bram #(
        .USE_VALID_BIT                                      (0),
        .MEM_ADDR_WIDTH                                     (GetWidth(DATA_BUFFER_DEPTH - 1)),
        .MEM_DATA_WIDTH                                     (AXIS_TSBAND_WIDTH),
        .MEM_INITIALISE                                     (0)
    ) axis_tsband_fifo (
        .clka                                               (clk),
        .porta_addr                                         (stage_fifo_write_ptr),
        .porta_en                                           (IN_STATE == IN_STATE_DECODE || IN_STATE == IN_STATE_ONESHOT),
        .porta_wr_en                                        (1'b1),
        .porta_data_in                                      (stage_in_axis_tsband),
        .porta_data_out                                     (),

        .clkb                                               (clk),
        .portb_addr                                         (fifo_read_ptr),
        .portb_en                                           (1'b1),
        .portb_wr_en                                        (1'b0),
        .portb_data_in                                      (),
        .portb_data_out                                     (relay_fetch_axis_tsband)
    );

    assign fifo_out_tempty                                  = (fifo_write_tptr == fifo_read_ptr) && (fifo_write_toverf == fifo_read_overf);
    assign fifo_out_tfull                                   = (fifo_write_tptr == fifo_read_ptr) && (fifo_write_toverf != fifo_read_overf);
    assign fifo_out_tvalid                                  = !fifo_out_tempty;

    initial
    begin
        FETCH_STATE                                         = FETCH_STATE_RESET;

        fifo_read_ptr                                       = 0;
        fifo_read_overf                                     = 0;
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            FETCH_STATE                                     <= FETCH_STATE_RESET;
        else
            case(FETCH_STATE)
                FETCH_STATE_RESET:
                begin
                    FETCH_STATE                             <= FETCH_STATE_IDLE;

                    fifo_read_ptr                           <= 0;
                    fifo_read_overf                         <= 0;
                end
                FETCH_STATE_IDLE:
                    if(relay_stab_fetch_greedy_ready == 1'b1)
                        if(fifo_out_tvalid == 1'b1) begin
                            FETCH_STATE                     <= FETCH_STATE_PASSTHROUGH;

                            fifo_read_ptr                   <= (fifo_read_ptr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_read_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_read_overf             <= (fifo_read_overf + 1) % DATA_OVERF_DEPTH;
                        end
                FETCH_STATE_PASSTHROUGH:
                    if(relay_stab_fetch_ready == 1'b1)
                        if(fifo_out_tvalid == 1'b1) begin
                            fifo_read_ptr                   <= (fifo_read_ptr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_read_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_read_overf             <= (fifo_read_overf + 1) % DATA_OVERF_DEPTH;
                        end
                        else
                            FETCH_STATE                     <= FETCH_STATE_IDLE;
            endcase
    end

    initial
    begin
        STAB_STACK                                          = 0;

        pf_fetch_axis_tsband[0]                             = {AXIS_TSBAND_WIDTH{1'b0}};
        pf_fetch_axis_tsband[1]                             = {AXIS_TSBAND_WIDTH{1'b0}};
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0) begin
            STAB_STACK                                      <= 0;

            pf_fetch_axis_tsband[0]                         <= {AXIS_TSBAND_WIDTH{1'b0}};
            pf_fetch_axis_tsband[1]                         <= {AXIS_TSBAND_WIDTH{1'b0}};
        end
        else
            case(STAB_STACK)
                0:  if(relay_stab_fetch_valid == 1'b1) begin
                        STAB_STACK                          <= STAB_STACK + 1;

                        pf_fetch_axis_tsband[0]             <= relay_fetch_axis_tsband;
                    end
                1:  if(relay_out_stab_ready == 1'b1)
                        if(relay_stab_fetch_valid == 1'b1) begin
                            STAB_STACK                      <= STAB_STACK + 1 - 1;

                            pf_fetch_axis_tsband[0]         <= relay_fetch_axis_tsband;
                        end
                        else
                            STAB_STACK                      <= STAB_STACK - 1;
                    else
                        if(relay_stab_fetch_valid == 1'b1) begin
                            STAB_STACK                      <= STAB_STACK + 1;

                            pf_fetch_axis_tsband[1]         <= relay_fetch_axis_tsband;
                        end
                2:  if(relay_out_stab_ready == 1'b1) begin
                        STAB_STACK                          <= STAB_STACK - 1;

                        pf_fetch_axis_tsband[0]             <= pf_fetch_axis_tsband[1];
                    end
            endcase
    end

    assign relay_stab_fetch_valid                           = FETCH_STATE == FETCH_STATE_PASSTHROUGH;
    assign relay_stab_fetch_greedy_ready                    = STAB_STACK < 2 || STAB_STACK == 2  && tap_m_axis_tready == 1'b1
                                                                && tap_m_axis_tvalid == 1'b1 && tap_m_axis_tlast == 1'b1;
    assign relay_stab_fetch_ready                           = STAB_STACK == 0 || STAB_STACK > 0 && tap_m_axis_tready == 1'b1
                                                                && tap_m_axis_tvalid == 1'b1 && tap_m_axis_tlast == 1'b1;

    initial
        OUT_STATE                                           = OUT_STATE_RESET;

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            OUT_STATE                                       <= OUT_STATE_RESET;
        else
            case(OUT_STATE)
                OUT_STATE_RESET:
                    OUT_STATE                               <= OUT_STATE_IDLE;
                OUT_STATE_IDLE:
                    if(relay_out_stab_valid == 1'b1)
                        OUT_STATE                           <= OUT_STATE_PASSTHROUGH;
                OUT_STATE_PASSTHROUGH:
                    if(tap_m_axis_tready == 1'b1)
                        if(tap_m_axis_tvalid == 1'b1 && tap_m_axis_tlast == 1'b1)
                            if(relay_out_stab_valid == 1'b1)
                                OUT_STATE                   <= OUT_STATE_PASSTHROUGH;
                            else
                                OUT_STATE                   <= OUT_STATE_IDLE;
            endcase
    end

    assign relay_out_stab_valid                             = STAB_STACK > 0;
    assign relay_out_stab_ready                             = OUT_STATE == OUT_STATE_PASSTHROUGH && tap_m_axis_tready == 1'b1
                                                                && tap_m_axis_tvalid == 1'b1 && tap_m_axis_tlast == 1'b1;

    assign m_axis_tsideband                                 = OUT_STATE == OUT_STATE_PASSTHROUGH
                                                                ? pf_fetch_axis_tsband[0] : {AXIS_TSBAND_WIDTH{1'b0}};
    assign axis_trans_count                                 = fifo_write_tptr - fifo_read_ptr
                                                                + (fifo_write_toverf == fifo_read_overf ? 0 : DATA_BUFFER_DEPTH)
                                                                + STAB_STACK;
endmodule