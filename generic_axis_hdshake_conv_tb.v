`timescale 1ns/1ps

/**
 *
 * generic_axis_hdshake_conv_tb.v
 *
 * Short Description: 
 *
 * Long Description: 
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

module generic_axis_hdshake_conv_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    parameter CLK_PERIOD                                    = 10;
    parameter MSB_ALIGN                                     = 1;

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter AXIS_TDATA_WIDTH                              = 8;
    parameter DATA_BUFFER_DEPTH                             = 8;
    parameter PREMATURE_DROP_EN                             = 1;

    reg                                                     clk;
    reg                                                     aresetn;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     in_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     in_axis_tkeep;
    wire                                                    in_axis_tlast;
    wire                                                    in_axis_tvalid;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     relay_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     relay_axis_tkeep;
    wire                                                    relay_axis_tlast;
    wire                                                    relay_axis_tdrop;
    wire                                                    relay_axis_tready;
    wire                                                    relay_axis_tvalid;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     out_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     out_axis_tkeep;
    wire                                                    out_axis_tlast;
    reg                                                     out_axis_tready;
    wire                                                    out_axis_tvalid;
    wire [`INTEGER_WIDTH   * 8 - 1 : 0]                     axis_data_count;

    integer                                                 schedule_j;

// BEGIN

`include "axis_data_gen.vh"

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    // CYCLE-BASED STATIC SCHEDULE
    // ----------------------------------------
    //
    initial
    begin
        schedule_j                                          = 0;

        out_axis_tready                                     = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            // 17: out_axis_tready                             <= 1'b1;
            // 18: out_axis_tready                             <= 1'b0;
            // 21: out_axis_tready                             <= 1'b1;
            // 22: out_axis_tready                             <= 1'b0;
            // 27: out_axis_tready                             <= 1'b1;
            // 28: out_axis_tready                             <= 1'b0;
            // 31: out_axis_tready                             <= 1'b1;
            // 32: out_axis_tready                             <= 1'b0;
        endcase
    end

    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // TODO: DESCRIBE TRANSACTIONS
    // 1. TRANSACTION 1, READY = ...
    //
    always @(posedge clk)
    begin
        if(aresetn == 1'b0)
            `bus_null(0)
        else begin
            `bus_adv(0)

            case(`bus_step(0))
                0:  `bus_out(0, 24, 6 * AXIS_TDATA_WIDTH, 0)
                1:  `bus_out(0, 66, 5 * AXIS_TDATA_WIDTH, 0)
                2:  `bus_out(0, 35, 7 * AXIS_TDATA_WIDTH, 0)
                3:  `bus_out(0, 70, 4 * AXIS_TDATA_WIDTH, 0)
                4:  `bus_out(0, 99, 5 * AXIS_TDATA_WIDTH, 0)
                5:  `bus_out(0, 12, 5 * AXIS_TDATA_WIDTH, 0)
                6:  `bus_out(0, 83, 6 * AXIS_TDATA_WIDTH, 0)
                8:  `bus_out(0, 57, 7 * AXIS_TDATA_WIDTH, 0)
                9:  `bus_out(0, 64, 6 * AXIS_TDATA_WIDTH, 0)
                10: `bus_out(0, 39, 6 * AXIS_TDATA_WIDTH, 0)
                11: `bus_out(0, 47, 5 * AXIS_TDATA_WIDTH, 0)
                12: `bus_out(0, 86, 4 * AXIS_TDATA_WIDTH, 0)
                default: `bus_null(0)
            endcase
        end
    end

    generic_axis_hdshake_conv #(
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_axis_tdata                                       (in_axis_tdata),
        .s_axis_tkeep                                       (in_axis_tkeep),
        .s_axis_tlast                                       (in_axis_tlast),
        .s_axis_tvalid                                      (in_axis_tvalid),
        .m_axis_tdata                                       (relay_axis_tdata),
        .m_axis_tkeep                                       (relay_axis_tkeep),
        .m_axis_tlast                                       (relay_axis_tlast),
        .m_axis_tdrop                                       (relay_axis_tdrop),
        .m_axis_tready                                      (relay_axis_tready),
        .m_axis_tvalid                                      (relay_axis_tvalid)
    );

    generic_axis_fifo #(
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH),
        .DATA_BUFFER_DEPTH                                  (DATA_BUFFER_DEPTH),
        .PREMATURE_DROP_EN                                  (PREMATURE_DROP_EN)
    ) UUT_ASSIST (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_axis_tdata                                       (relay_axis_tdata),
        .s_axis_tkeep                                       (relay_axis_tkeep),
        .s_axis_tlast                                       (relay_axis_tlast),
        .s_axis_tdrop                                       (relay_axis_tdrop),
        .s_axis_tready                                      (relay_axis_tready),
        .s_axis_tvalid                                      (relay_axis_tvalid),
        .m_axis_tdata                                       (out_axis_tdata),
        .m_axis_tkeep                                       (out_axis_tkeep),
        .m_axis_tlast                                       (out_axis_tlast),
        .m_axis_tready                                      (out_axis_tready),
        .m_axis_tvalid                                      (out_axis_tvalid),
        .axis_data_count                                    (axis_data_count)
    );

    axis_data_gen #(
        .MSB_ALIGN                                          (MSB_ALIGN),
        .CLK_ALIGN                                          (1),
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH)
    ) SIG_GEN (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .cmd_bus                                            (`cmd_bus(0)),
        .axis_tdata                                         (in_axis_tdata),
        .axis_tkeep                                         (in_axis_tkeep),
        .axis_tlast                                         (in_axis_tlast),
        .axis_tready                                        (1'b1),
        .axis_tvalid                                        (in_axis_tvalid)
    );
endmodule