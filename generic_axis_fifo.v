`timescale 1ns/1ps

/**
 *
 * generic_axis_fifo.v
 *
 * Short Description: The module implemented a First-in-first-out buffering
 *  module in AXI Stream protocol.
 *
 * Long Description: The module implemented a First-in-first-out buffering
 *  module in AXI Stream protocol.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

/**
 *
 * GENERIC_AXIS_FIFO
 *
 * GENERIC PARAMETERS
 * --------------------
 * AXIS_TSBAND_ENABLE  BOOLEAN VARIABLE TO ENABLE THE MODULE TO PROCESS CUSTOM
 *                      AXI-STREAM TRANSACTION-BASED SIDEBAND CHANNEL, TRANS-
 *                      ACTION-BASED SIDEBAND CHANNEL TRANSFERS VALUE THAT KEEPS
 *                      CONSTANT IN A TRANSACTION SPAN
 * AXIS_TWSBAND_ENABLE BOOLEAN VARIABLE TO ENABLE THE MODULE TO PROCESS CUSTOM
 *                      AXI-STREAM WORD-BASED SIDEBAND CHANNEL, WORD-BASED SIDE-
 *                      BAND CHANNEL RUNS VARYING DATA THAT ALIGNS TO INDIVIDUAL
 *                      AXI-STREAM WORDS ACROSS A TRANSACTION SPAN
 * BYTE_COUNT_ENABLE   BOOLEAN VARIABLE TO TOGGLE ON THE CAPABILITY TO COUNT
 *                      VALID BYTES IN AN AXI-STREAM TRANSACTION AND PROVIDE
 *                      THE COUNT IN A MOCKERY AXI-STREAM SIDEBAND CHANNEL THAT
 *                      ALIGNS TO THE AXI-STREAM TRANSACTION, AS M_AXIS_TLEN
 * BCCHNL_INDEPENDENT  BOOLEAN VARIABLE TO OPERATE THE MODULE WITH AXI-STREAM
 *                      TRANSACTION BYTE COUNTING CHANNEL, NAMELY, M_AXIS_TLEN
 *                      INDEPENDENT FROM THE OUTGOING AXI-S TRANSACTION, I.E.,
 *                      OPERATE NOT AS A SIDEBAND CHANNEL ALIGNED TO THE STREAM
 *                      WITH SEPARATE HANDSHAKE MECHANISM SUPPORTED BY EXTRA
 *                      PINS, M_AXIS_TLEN_TVALID/TREADY
 * AXIS_TDATA_WIDTH    THE NUMBER OF BYTES IN THE AXI-STREAM DATA CHANNEL
 * AXIS_TSBAND_WIDTH   THE NUMBER OF BITS IN THE CORRESPONDING TRANSACTION-BASED
 *                      SIDEBAND CHANNEL
 * AXIS_TWSBAND_WIDTH  THE NUMBER OF BITS IN THE CORRESPONDING WORD-BASED SIDE-
 *                      BAND CHANNEL
 * AXIS_XILINX_MODE    BOOLEAN VARIABLE TO INSTRUCT THE MODULE TO OPERATE IN
 *                      XILINX AXI-STREAM COMPATIBILITY MODE. ORIGINAL AXIS
 *                      SPECIFICATION DICTATED THE USE OF AXIS TKEEP SIDEBAND
 *                      CHANNEL TO RESOLVE INDIVIDUAL BYTE IN TDATA CHANNEL
 *                      BEING VALID. XILINX HAS REDUCED SUPPORT IN THIS ASPECT
 *                      IN ONLY TO HONOUR SUCH RESOLUTION WHEN TLAST IS ASSERTED
 *                      HIGH ASSUMING CONTINUOUS STREAM. THIS MODE ASSUMES ALL
 *                      BYTES IN WORDS ARE VALID UNTIL TLAST IS ASSERTED HIGH
 *                      BUT VALIDITY INDICATION AS PROVIDED IN INCOMING TKEEP
 *                      SIDEBAND CHANNEL WILL NOT BE AMENDED BY THE MODULE
 * AXIS_MSB_ALIGN      BOOLEAN VARIABLE INDICATING WORDS IN AN AXI-STREAM TRANS-
 *                      ACTION IS ALIGNED TO THE SIDE OF THE MOST SIGNIFICANT
 *                      BYTE. A BYTE COUNT OPERATION IS CONDUCTED AS FROM MSB TO
 *                      LAST VALID LSB WHEN THIS PARAMETER AND AXIS_XILINX_MODE
 *                      IS BOTH ASSERTED
 * DATA_BUFFER_DEPTH   THE NUMBER OF TRANSACTION WORDS THAT CAN BE BUFFERED BY
 *                      THE FIFO
 * VALID_TRANS_COUNT   BOOLEAN VARIABLE TO FORCE THE COUNTER OF BUFFERED DATA TO
 *                      INCLUDE ONLY COMPLETED AXI-STREAM TRANSACTIONS, I.E.,
 *                      A TRANSACTION WITH THE LAST WORD (A WORD WITH SIDEBAND
 *                      TLAST ASSERTED) ADMITTED TO THE LOCAL FIFO BUFFER
 * PREMATURE_DROP_EN   BOOLEAN VARIABLE TO DETERMINE THE AXI STREAM PRETENDING
 *                      SIDEBAND TDROP IS OBSERVED FOR DROPPING AN INCOMPLETED
 *                      AXI STREAM TRANSACTION STAGED INTO THE FIFO
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_axis_*            VAR   INCOMING AXI-STREAM INTERFACE
 * s_axis_tsband       IN    INCOMING CUSTOM AXI-STREAM TRANSACTION-BASED SIDE-
 *                            BAND CHANNEL, TRANSACTION-BASED SIDEBAND CHANNEL
 *                            TRANSFERS VALUES THAT REMAIN CONSTANT IN THE WHOLE
 *                            TRANSACTION SPAN
 * s_axis_twsband      IN    INCOMING CUSTOM AXI-STREAM WORD-BASED SIDEBAND
 *                            CHANNEL, WORD-BASED SIDEBAND CHANNEL RUNS VARYING
 *                            DATA THAT ALIGNS TO AXI-STREAM WORDS THROUGHOUT
 *                            THE TRANSACTION SPAN
 * s_axis_tdrop        IN    A SIDEBAND CHANNEL MOCKING TO BE A STANDARD AXI-
 *                            STREAM INTERFACE THAT MANDATES THE MODULE TO DROP
 *                            INCOMPLETED AXI-STREAM TRANSACTION, IF EXISTS,
 *                            PRIOR TO THE CYCLE OF ASSERTION
 * m_axis_*            VAR   OUT-GOING AXI-STREAM INTERFACE
 * m_axis_tlen         OUT   OUT-GOING CUSTOM AXI-STREAM TRANSACTION-BASED BYTE
 *                            COUNT RESULT SIDEBAND CHANNEL
 * m_axis_tlen_*       VAR   ADDITIONAL PINS SUPPORTING TWO-WAY HANDSHAKE WHEN
 *                            THE TRANSACTION-BASED BYTE COUNT SIDEBAND CHANNEL
 *                            OPERATES INDEPENDENT FROM THE OUTGOING AXI-STREAM
 * m_axis_tsband       OUT   OUT-GOING CUSTOM AXI-STREAM TRANSACTION-BASED SIDE-
 *                             BAND CHANNEL
 * m_axis_twsband      OUT   OUT-GOING CUSTOM AXI-STREAM WORD-BASED SIDEBAND
 *                            CHANNEL
 * axis_data_count     OUT   THE NUMBER OF TRANSACTION WORDS RESIDING IN THE AXI
 *                            STREAM FIFO
 *
 */
module generic_axis_fifo #(
        parameter AXIS_TSBAND_ENABLE                        = 0,
        parameter AXIS_TWSBAND_ENABLE                       = 0,
        parameter BYTE_COUNT_ENABLE                         = 0,
        parameter BCCHNL_INDEPENDENT                        = 0,
        parameter AXIS_TDATA_WIDTH                          = 8,
        parameter AXIS_TSBAND_WIDTH                         = 64,
        parameter AXIS_TWSBAND_WIDTH                        = 64,
        parameter DATA_BUFFER_DEPTH                         = 1024,
        parameter AXIS_XILINX_MODE                          = 0,
        parameter AXIS_MSB_ALIGN                            = 1,
        parameter VALID_TRANS_COUNT                         = 0,
        parameter PREMATURE_DROP_EN                         = 0
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]          s_axis_tdata,
        input  wire [AXIS_TDATA_WIDTH     - 1 : 0]          s_axis_tkeep,
        input  wire                                         s_axis_tlast,
        input  wire [AXIS_TSBAND_WIDTH    - 1 : 0]          s_axis_tsband,
        input  wire [AXIS_TWSBAND_WIDTH   - 1 : 0]          s_axis_twsband,
        input  wire                                         s_axis_tdrop,
        output wire                                         s_axis_tready,
        input  wire                                         s_axis_tvalid,
        output wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]          m_axis_tdata,
        output wire [AXIS_TDATA_WIDTH     - 1 : 0]          m_axis_tkeep,
        output wire                                         m_axis_tlast,
        output wire [GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH) - 1 : 0]
                                                            m_axis_tlen,
        output wire [AXIS_TSBAND_WIDTH    - 1 : 0]          m_axis_tsband,
        output wire [AXIS_TWSBAND_WIDTH   - 1 : 0]          m_axis_twsband,
        input  wire                                         m_axis_tready,
        output wire                                         m_axis_tvalid,
        input  wire                                         m_axis_tlen_tready,
        output wire                                         m_axis_tlen_tvalid,
        output wire [`INTEGER_WIDTH   * 8 - 1 : 0]          axis_data_count
    );

    function integer GetWidth;
        input integer value;

        begin
            for(GetWidth = 0; value > 0; GetWidth = GetWidth + 1)
                value = value >> 1;
        end
    endfunction

    // DATA_OVERF_DEPTH MUST BE GREATER THAN 2 OR THE STOP CONDITION OF THE
    //  STATE MACHINE WILL ALWAYS BE MET, I.E., HITTING OVERFLOW CONTINUE PATH
    //  fifo_axis_toverf == 0 && fifo_read_overf == DATA_OVERF_DEPTH - 1
    //
    localparam DATA_OVERF_DEPTH                             = 3;

    reg  [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     stage_count_axis_tdata;
    reg  [AXIS_TDATA_WIDTH     - 1 : 0]                     stage_count_axis_tkeep;
    reg                                                     stage_count_axis_tlast;
    reg                                                     stage_count_axis_tvalid;
    reg  [AXIS_TSBAND_WIDTH    - 1 : 0]                     stage_count_axis_tsband;
    reg  [AXIS_TWSBAND_WIDTH   - 1 : 0]                     stage_count_axis_twsband;

    reg  [2 : 0]                                            IN_STATE;
    localparam IN_STATE_IDLE                                = 3'b000;
    localparam IN_STATE_PASSTHROUGH                         = 3'b001;
    localparam IN_STATE_ONESHOT                             = 3'b010;
    localparam IN_STATE_LAST                                = 3'b011;
    localparam IN_STATE_RESET                               = 3'b111;

    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     relay_in_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     relay_in_axis_tkeep;
    wire                                                    relay_in_axis_tlast;
    wire                                                    relay_in_axis_tdrop;
    wire                                                    relay_in_axis_tready;
    wire                                                    relay_in_axis_tvalid;
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     relay_in_axis_tsband;
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     relay_in_axis_twsband;
    wire [GetWidth(AXIS_TDATA_WIDTH) - 1 : 0]               relay_in_axis_twcount;

    reg  [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     stage_in_axis_tdata;
    reg  [AXIS_TDATA_WIDTH     - 1 : 0]                     stage_in_axis_tkeep;
    reg                                                     stage_in_axis_tlast;
    reg  [AXIS_TSBAND_WIDTH    - 1 : 0]                     stage_in_axis_tsband;
    reg  [AXIS_TWSBAND_WIDTH   - 1 : 0]                     stage_in_axis_twsband;
    reg  [GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH) - 1 : 0]
                                                            stage_in_axis_tcount;

    // INTERNAL READY SIGNAL FOR INPUT FLOW CONTROL AND OUTPUT FLOW CONTROL
    //  TO INTERNAL FIFO
    //
    wire                                                    fifo_in_ready;
    wire                                                    fifo_in_reset_ready;
    wire                                                    fifo_full;
    wire                                                    fifo_reset_full;
    wire                                                    fifo_empty;
    wire                                                    fifo_out_valid;

    // INTERNAL READY SIGNAL FOR INPUT FLOW CONTROL AND OUTPUT FLOW CONTROL
    //  TO TRANSACTION-BASED INTERNAL FIFO
    //
    wire                                                    fifo_in_tcready;
    wire                                                    fifo_tcfull;
    wire                                                    fifo_tcempty;
    wire                                                    fifo_out_tcvalid;
    wire                                                    fifo_in_tbcready;
    wire                                                    fifo_tbcfull;
    wire                                                    fifo_tbcempty;
    wire                                                    fifo_out_tbcvalid;

    // FIFO IN CIRCULAR MEMORY IMPLEMENTATION
    //
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_write_ptr;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          stage_fifo_write_ptr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_write_overf;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_write_tptr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_write_toverf;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_read_ptr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_read_overf;

    // TRANSACTION-BASED FIFO IN CIRCULAR MEMORY IMPLEMENTATION COLLOQUALLY
    //  KNOWN AS TRANSACTION COUNTER
    //
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_write_tctr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_write_tcoverf;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_read_tctr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_read_tcoverf;
    reg  [GetWidth(DATA_BUFFER_DEPTH - 1) - 1 : 0]          fifo_read_tbctr;
    reg  [GetWidth(DATA_OVERF_DEPTH - 1)  - 1 : 0]          fifo_read_tbcoverf;

    reg  [1 : 0]                                            FETCH_STATE;
    reg  [1 : 0]                                            TCFETCH_STATE;
    reg  [1 : 0]                                            TBCFETCH_STATE;
    localparam FETCH_STATE_IDLE                             = 2'b00;
    localparam FETCH_STATE_PASSTHROUGH                      = 2'b01;
    localparam FETCH_STATE_STALL                            = 2'b10;
    localparam FETCH_STATE_RESET                            = 2'b11;

    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     relay_fetch_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     relay_fetch_axis_tkeep;
    wire                                                    relay_fetch_axis_tlast;
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     relay_fetch_axis_tsband;
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     relay_fetch_axis_twsband;
    wire [GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH) - 1 : 0]
                                                            relay_fetch_axis_tcount;
    //
    // IT SHOULD BE NOTED THAT THE GETWIDTH CALCULATES FROM 0 TO DATA_BUFFER_DEPTH
    //  * AXIS_TDATA_WIDTH INCLUSIVELY, UNLIKE NORMAL GETWIDTH CALCULATIONS THAT
    //  INTENDS TO GET THE BITS FOR THE NUMBER OF COMBINATIONS IN A RANGE, I.E.,
    //  0 TO DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH - 1.

    reg  [1 : 0]                                            STAB_STACK;
    reg  [1 : 0]                                            TCSTAB_STACK;
    reg  [1 : 0]                                            TBCSTAB_STACK;
    reg  [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     pf_fetch_axis_tdata[1 : 0];
    reg  [AXIS_TDATA_WIDTH     - 1 : 0]                     pf_fetch_axis_tkeep[1 : 0];
    reg                                                     pf_fetch_axis_tlast[1 : 0];
    reg  [AXIS_TSBAND_WIDTH    - 1 : 0]                     pf_fetch_axis_tsband[1 : 0];
    reg  [AXIS_TWSBAND_WIDTH   - 1 : 0]                     pf_fetch_axis_twsband[1 : 0];
    reg  [GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH) - 1 : 0]
                                                            pf_fetch_axis_tcount[1 : 0];

    wire                                                    relay_stab_fetch_valid;
    wire                                                    relay_stab_fetch_greedy_ready;
    wire                                                    relay_stab_fetch_ready;

    wire                                                    relay_tcstab_tcfetch_valid;
    wire                                                    relay_tcstab_tcfetch_greedy_ready;
    wire                                                    relay_tcstab_tcfetch_ready;

    wire                                                    relay_tbcstab_tbcfetch_valid;
    wire                                                    relay_tbcstab_tbcfetch_greedy_ready;
    wire                                                    relay_tbcstab_tbcfetch_ready;

    wire                                                    relay_out_stab_valid;
    wire                                                    relay_out_stab_ready;

    wire                                                    relay_out_tcstab_valid;
    wire                                                    relay_out_tcstab_ready;

    wire                                                    relay_out_tbcstab_valid;
    wire                                                    relay_out_tbcstab_ready;

    wire                                                    relay_out_axis_tvalid;

// BEGIN

    generate
        genvar count_i;

        localparam SAMPLE_WIDTH                             = 4;

        if(BYTE_COUNT_ENABLE > 0) begin
            initial
            begin
                stage_count_axis_tdata                      = {AXIS_TDATA_WIDTH{8'h0}};
                stage_count_axis_tkeep                      = {AXIS_TDATA_WIDTH{1'b0}};
                stage_count_axis_tlast                      = 1'b0;
                stage_count_axis_tvalid                     = 1'b0;
            end

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0) begin
                    stage_count_axis_tdata                  <= {AXIS_TDATA_WIDTH{8'h0}};
                    stage_count_axis_tkeep                  <= {AXIS_TDATA_WIDTH{1'b0}};
                    stage_count_axis_tlast                  <= 1'b0;
                    stage_count_axis_tvalid                 <= 1'b0;
                end
                else
                    if(relay_in_axis_tready == 1'b1) begin
                        stage_count_axis_tdata              <= s_axis_tdata;
                        stage_count_axis_tkeep              <= s_axis_tkeep;
                        stage_count_axis_tlast              <= s_axis_tlast;
                        stage_count_axis_tvalid             <= s_axis_tvalid;
                    end
            end

            if(AXIS_TSBAND_ENABLE > 0) begin
                initial
                    stage_count_axis_tsband                 = {AXIS_TSBAND_WIDTH{1'b0}};

                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_count_axis_tsband             <= {AXIS_TSBAND_WIDTH{1'b0}};
                    else
                        if(relay_in_axis_tready == 1'b1)
                            stage_count_axis_tsband         <= s_axis_tsband;
            end

            if(AXIS_TWSBAND_ENABLE > 0) begin
                initial
                    stage_count_axis_twsband                = {AXIS_TWSBAND_WIDTH{1'b0}};

                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_count_axis_twsband            <= {AXIS_TWSBAND_WIDTH{1'b0}};
                    else
                        if(relay_in_axis_tready == 1'b1)
                            stage_count_axis_twsband        <= s_axis_twsband;
            end

            if(AXIS_XILINX_MODE > 0) begin
                wire [GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH) - 1 : 0]
                                                            in_axis_twcount[AXIS_TDATA_WIDTH / SAMPLE_WIDTH : 0];

                if(AXIS_MSB_ALIGN > 0) begin
                    for(count_i = 0; count_i < AXIS_TDATA_WIDTH / SAMPLE_WIDTH; count_i = count_i + 1)
                        assign in_axis_twcount[count_i]     = stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 0] == 1'b1
                                                                ? (AXIS_TDATA_WIDTH / SAMPLE_WIDTH - count_i - 1) * SAMPLE_WIDTH + 4
                                                                : stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 1] == 1'b1
                                                                ? (AXIS_TDATA_WIDTH / SAMPLE_WIDTH - count_i - 1) * SAMPLE_WIDTH + 3
                                                                : stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 2] == 1'b1
                                                                ? (AXIS_TDATA_WIDTH / SAMPLE_WIDTH - count_i - 1) * SAMPLE_WIDTH + 2
                                                                : stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 3] == 1'b1
                                                                ? (AXIS_TDATA_WIDTH / SAMPLE_WIDTH - count_i - 1) * SAMPLE_WIDTH + 1
                                                                : in_axis_twcount[count_i + 1];

                    assign in_axis_twcount[AXIS_TDATA_WIDTH / SAMPLE_WIDTH]
                                                            = 0;
                    assign relay_in_axis_twcount            = in_axis_twcount[0];
                end
                else begin
                    for(count_i = 0; count_i < AXIS_TDATA_WIDTH / SAMPLE_WIDTH; count_i = count_i + 1)
                        assign in_axis_twcount[count_i + 1] = stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 3] == 1'b1
                                                                ? count_i * SAMPLE_WIDTH + 4
                                                                : stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 2] == 1'b1
                                                                ? count_i * SAMPLE_WIDTH + 3
                                                                : stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 1] == 1'b1
                                                                ? count_i * SAMPLE_WIDTH + 2
                                                                : stage_count_axis_tkeep[count_i * SAMPLE_WIDTH + 0] == 1'b1
                                                                ? count_i * SAMPLE_WIDTH + 1
                                                                : in_axis_twcount[count_i];

                    assign in_axis_twcount[0]               = 0;
                    assign relay_in_axis_twcount            = in_axis_twcount[AXIS_TDATA_WIDTH / SAMPLE_WIDTH];
                end
            end
            else begin
                reg  [GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH) - 1 : 0]
                                                            in_axis_twcount[AXIS_TDATA_WIDTH / SAMPLE_WIDTH : 0];

                initial
                    in_axis_twcount[0]                      = 0;

                for(count_i = 0; count_i < AXIS_TDATA_WIDTH / SAMPLE_WIDTH; count_i = count_i + 1) begin
                    initial
                        in_axis_twcount[count_i + 1]        = 0;

                    always @(*)
                    begin
                        case(stage_count_axis_tkeep[count_i * SAMPLE_WIDTH +: SAMPLE_WIDTH])
                            4'b0000:
                                in_axis_twcount[count_i + 1]
                                                            = 0 + in_axis_twcount[count_i];
                            4'b0001, 4'b0010, 4'b0100, 4'b1000:
                                in_axis_twcount[count_i + 1]
                                                            = 1 + in_axis_twcount[count_i];
                            4'b0011, 4'b0101, 4'b0110, 4'b1001, 4'b1010, 4'b1100:
                                in_axis_twcount[count_i + 1]
                                                            = 2 + in_axis_twcount[count_i];
                            4'b0111, 4'b1011, 4'b1101, 4'b1110:
                                in_axis_twcount[count_i + 1]
                                                            = 3 + in_axis_twcount[count_i];
                            4'b1111:
                                in_axis_twcount[count_i + 1]
                                                            = 4 + in_axis_twcount[count_i];
                        endcase
                    end
                end

                assign relay_in_axis_twcount                = in_axis_twcount[AXIS_TDATA_WIDTH / SAMPLE_WIDTH];
            end

            assign relay_in_axis_tdata                      = stage_count_axis_tdata;
            assign relay_in_axis_tkeep                      = stage_count_axis_tkeep;
            assign relay_in_axis_tlast                      = stage_count_axis_tlast;
            assign relay_in_axis_tvalid                     = relay_in_axis_tdrop == 1'b0 ? stage_count_axis_tvalid : 1'b0;

            if(AXIS_TSBAND_ENABLE > 0)
                assign relay_in_axis_tsband                 = stage_count_axis_tsband;
            if(AXIS_TWSBAND_ENABLE > 0)
                assign relay_in_axis_twsband                = stage_count_axis_twsband;
        end
        else begin
            assign relay_in_axis_tdata                      = s_axis_tdata;
            assign relay_in_axis_tkeep                      = s_axis_tkeep;
            assign relay_in_axis_tlast                      = s_axis_tlast;
            assign relay_in_axis_tvalid                     = s_axis_tvalid;

            if(AXIS_TSBAND_ENABLE > 0)
                assign relay_in_axis_tsband                 = s_axis_tsband;
            if(AXIS_TWSBAND_ENABLE > 0)
                assign relay_in_axis_twsband                = s_axis_twsband;
        end
    endgenerate

    initial
    begin
        IN_STATE                                            = IN_STATE_RESET;

        fifo_write_ptr                                      = 0;
        fifo_write_overf                                    = 0;

        fifo_write_tptr                                     = 0;
        fifo_write_toverf                                   = 0;

        fifo_write_tctr                                     = 0;
        fifo_write_tcoverf                                  = 0;

        stage_in_axis_tdata                                 = {AXIS_TDATA_WIDTH{8'h0}};
        stage_in_axis_tkeep                                 = {AXIS_TDATA_WIDTH{1'b0}};
        stage_in_axis_tlast                                 = 1'b0;

        stage_fifo_write_ptr                                = 0;
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            IN_STATE                                        <= IN_STATE_RESET;
        else
            if(relay_in_axis_tdrop == 1'b1) begin
                if(relay_in_axis_tvalid == 1'b1) begin
                    if(fifo_in_reset_ready == 1'b1) begin
                        if(relay_in_axis_tlast == 1'b1)
                            IN_STATE                        <= IN_STATE_ONESHOT;
                        else
                            IN_STATE                        <= IN_STATE_PASSTHROUGH;

                        stage_in_axis_tdata                 <= relay_in_axis_tdata;
                        stage_in_axis_tkeep                 <= relay_in_axis_tkeep;
                        stage_in_axis_tlast                 <= relay_in_axis_tlast;

                        stage_fifo_write_ptr                <= fifo_write_tptr;

                        fifo_write_ptr                      <= (fifo_write_tptr + 1) % DATA_BUFFER_DEPTH;

                        if((fifo_write_tptr + 1) % DATA_BUFFER_DEPTH == 0)
                            fifo_write_overf                <= (fifo_write_toverf + 1) % DATA_OVERF_DEPTH;
                        else
                            fifo_write_overf                <= fifo_write_toverf;
                        end
                end
                else begin
                    IN_STATE                                <= IN_STATE_IDLE;

                    fifo_write_ptr                          <= fifo_write_tptr;
                    fifo_write_overf                        <= fifo_write_toverf;
                end
            end
            else
                case(IN_STATE)
                    IN_STATE_RESET:
                    begin
                        IN_STATE                            <= IN_STATE_IDLE;

                        fifo_write_ptr                      <= 0;
                        fifo_write_overf                    <= 0;

                        fifo_write_tptr                     <= 0;
                        fifo_write_toverf                   <= 0;

                        fifo_write_tctr                     <= 0;
                        fifo_write_tcoverf                  <= 0;

                        stage_in_axis_tdata                 <= {AXIS_TDATA_WIDTH{8'h0}};
                        stage_in_axis_tkeep                 <= {AXIS_TDATA_WIDTH{1'b0}};
                        stage_in_axis_tlast                 <= 1'b0;

                        stage_fifo_write_ptr                <= 0;
                    end
                    IN_STATE_IDLE:
                        if(fifo_in_ready == 1'b1)
                            if(relay_in_axis_tvalid == 1'b1) begin
                                if(relay_in_axis_tlast == 1'b1)
                                    IN_STATE                <= IN_STATE_ONESHOT;
                                else
                                    IN_STATE                <= IN_STATE_PASSTHROUGH;

                                stage_in_axis_tdata         <= relay_in_axis_tdata;
                                stage_in_axis_tkeep         <= relay_in_axis_tkeep;
                                stage_in_axis_tlast         <= relay_in_axis_tlast;

                                stage_fifo_write_ptr        <= fifo_write_ptr;

                                fifo_write_ptr              <= (fifo_write_ptr + 1) % DATA_BUFFER_DEPTH;

                                if((fifo_write_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                    fifo_write_overf        <= (fifo_write_overf + 1) % DATA_OVERF_DEPTH;
                            end
                    IN_STATE_PASSTHROUGH:
                        if(fifo_in_ready == 1'b1)
                            if(relay_in_axis_tvalid == 1'b1) begin
                                if(relay_in_axis_tlast == 1'b1)
                                    IN_STATE                <= IN_STATE_LAST;

                                stage_in_axis_tdata         <= relay_in_axis_tdata;
                                stage_in_axis_tkeep         <= relay_in_axis_tkeep;
                                stage_in_axis_tlast         <= relay_in_axis_tlast;

                                stage_fifo_write_ptr        <= fifo_write_ptr;

                                fifo_write_ptr              <= (fifo_write_ptr + 1) % DATA_BUFFER_DEPTH;

                                if((fifo_write_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                    fifo_write_overf        <= (fifo_write_overf + 1) % DATA_OVERF_DEPTH;
                            end
                    IN_STATE_ONESHOT, IN_STATE_LAST:
                    begin
                        if(fifo_in_ready == 1'b1)
                            if(relay_in_axis_tvalid == 1'b1) begin
                                if(relay_in_axis_tlast == 1'b1)
                                    IN_STATE                <= IN_STATE_ONESHOT;
                                else
                                    IN_STATE                <= IN_STATE_PASSTHROUGH;

                                stage_in_axis_tdata         <= relay_in_axis_tdata;
                                stage_in_axis_tkeep         <= relay_in_axis_tkeep;
                                stage_in_axis_tlast         <= relay_in_axis_tlast;

                                stage_fifo_write_ptr        <= fifo_write_ptr;

                                fifo_write_ptr              <= (fifo_write_ptr + 1) % DATA_BUFFER_DEPTH;

                                if((fifo_write_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                    fifo_write_overf        <= (fifo_write_overf + 1) % DATA_OVERF_DEPTH;
                            end
                            else
                                IN_STATE                    <= IN_STATE_IDLE;
                        else
                            IN_STATE                        <= IN_STATE_IDLE;

                        fifo_write_tptr                     <= fifo_write_ptr;
                        fifo_write_toverf                   <= fifo_write_overf;

                        fifo_write_tctr                     <= (fifo_write_tctr + 1) % DATA_BUFFER_DEPTH;

                        if((fifo_write_tctr + 1) % DATA_BUFFER_DEPTH == 0)
                            fifo_write_tcoverf              <= (fifo_write_tcoverf + 1) % DATA_OVERF_DEPTH;
                    end
                endcase
    end

    generate
        if(AXIS_TSBAND_ENABLE > 0) begin
            initial
                stage_in_axis_tsband                        = {AXIS_TSBAND_WIDTH{1'b0}};

            always @(posedge clk or negedge aresetn)
                if(aresetn == 1'b0)
                    stage_in_axis_tsband                    <= {AXIS_TSBAND_WIDTH{1'b0}};
                else
                    if(relay_in_axis_tdrop == 1'b1 && fifo_in_reset_ready == 1'b1
                        || (IN_STATE == IN_STATE_IDLE || IN_STATE == IN_STATE_ONESHOT
                        || IN_STATE == IN_STATE_LAST) && fifo_in_ready == 1'b1)
                        if(relay_in_axis_tvalid == 1'b1)
                            stage_in_axis_tsband            <= relay_in_axis_tsband;
        end

        if(AXIS_TWSBAND_ENABLE > 0) begin
            initial
                stage_in_axis_twsband                       = {AXIS_TWSBAND_WIDTH{1'b0}};

            always @(posedge clk or negedge aresetn)
                if(aresetn == 1'b0)
                    stage_in_axis_twsband                   <= {AXIS_TWSBAND_WIDTH{1'b0}};
                else
                    if(relay_in_axis_tdrop == 1'b1 && fifo_in_reset_ready == 1'b1
                        || IN_STATE != IN_STATE_RESET && fifo_in_ready == 1'b1)
                        if(relay_in_axis_tvalid == 1'b1)
                            stage_in_axis_twsband           <= relay_in_axis_twsband;
        end

        if(BYTE_COUNT_ENABLE > 0) begin
            initial
                stage_in_axis_tcount                        = {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};

            if(AXIS_XILINX_MODE > 0) begin
                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_in_axis_tcount                <= {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
                    else
                        if(relay_in_axis_tdrop == 1'b1 && fifo_in_reset_ready == 1'b1
                            || (IN_STATE == IN_STATE_IDLE || IN_STATE == IN_STATE_ONESHOT
                            || IN_STATE == IN_STATE_LAST) && fifo_in_ready == 1'b1)
                            if(relay_in_axis_tvalid == 1'b1)
                                if(relay_in_axis_tlast == 1'b1)
                                    stage_in_axis_tcount    <= relay_in_axis_twcount;
                                else
                                    stage_in_axis_tcount    <= AXIS_TDATA_WIDTH;
                            else
                                stage_in_axis_tcount        <= 0;
                        else if(IN_STATE != IN_STATE_RESET && fifo_in_ready == 1'b1) begin
                            if(relay_in_axis_tvalid == 1'b1)
                                if(relay_in_axis_tlast == 1'b1)
                                    stage_in_axis_tcount    <= stage_in_axis_tcount + relay_in_axis_twcount;
                                else
                                    stage_in_axis_tcount    <= stage_in_axis_tcount + AXIS_TDATA_WIDTH;
                        end
            end
            else
                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_in_axis_tcount                <= {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
                    else
                        if(relay_in_axis_tdrop == 1'b1 && fifo_in_reset_ready == 1'b1
                            || (IN_STATE == IN_STATE_IDLE || IN_STATE == IN_STATE_ONESHOT
                            || IN_STATE == IN_STATE_LAST) && fifo_in_ready == 1'b1)
                            if(relay_in_axis_tvalid == 1'b1)
                                stage_in_axis_tcount        <= relay_in_axis_twcount;
                            else
                                stage_in_axis_tcount        <= 0;
                        else if(IN_STATE != IN_STATE_RESET && fifo_in_ready == 1'b1) begin
                            if(relay_in_axis_tvalid == 1'b1)
                                stage_in_axis_tcount        <= stage_in_axis_tcount + relay_in_axis_twcount;
                        end
        end
    endgenerate

    assign fifo_in_ready                                    = (IN_STATE != IN_STATE_RESET && FETCH_STATE != FETCH_STATE_RESET
                                                                && TCFETCH_STATE != FETCH_STATE_RESET && TBCFETCH_STATE != FETCH_STATE_RESET
                                                                && fifo_full == 1'b0);
    assign fifo_in_reset_ready                              = (IN_STATE != IN_STATE_RESET && FETCH_STATE != FETCH_STATE_RESET
                                                                && TCFETCH_STATE != FETCH_STATE_RESET && TBCFETCH_STATE != FETCH_STATE_RESET
                                                                && fifo_reset_full == 1'b0);
    assign fifo_full                                        = ((fifo_write_ptr == fifo_read_ptr)
                                                                && (fifo_write_overf != fifo_read_overf));
    assign fifo_reset_full                                  = ((fifo_write_tptr == fifo_read_ptr)
                                                                && (fifo_write_toverf != fifo_read_overf));

    assign fifo_in_tcready                                  = (IN_STATE != IN_STATE_RESET && FETCH_STATE != FETCH_STATE_RESET
                                                                && TCFETCH_STATE != FETCH_STATE_RESET && TBCFETCH_STATE != FETCH_STATE_RESET
                                                                && fifo_tcfull == 1'b0);
    assign fifo_tcfull                                      = ((fifo_write_tctr == fifo_read_tctr)
                                                                && (fifo_write_tcoverf != fifo_read_tcoverf));

    assign fifo_in_tbcready                                 = (IN_STATE != IN_STATE_RESET && FETCH_STATE != FETCH_STATE_RESET
                                                                && TCFETCH_STATE != FETCH_STATE_RESET && TBCFETCH_STATE != FETCH_STATE_RESET
                                                                && fifo_tbcfull == 1'b0);
    assign fifo_tbcfull                                     = ((fifo_write_tctr == fifo_read_tbctr)
                                                                && (fifo_write_tcoverf != fifo_read_tbcoverf));

    generate
        if(PREMATURE_DROP_EN > 0) begin
            assign relay_in_axis_tdrop                      = s_axis_tdrop;

            assign relay_in_axis_tready                     = fifo_in_ready | s_axis_tdrop & fifo_in_reset_ready;
        end
        else begin
            assign relay_in_axis_tdrop                      = 1'b0;

            assign relay_in_axis_tready                     = fifo_in_ready;
        end
    endgenerate

    generic_bram #(
        .USE_VALID_BIT                                      (0),
        .MEM_ADDR_WIDTH                                     (GetWidth(DATA_BUFFER_DEPTH - 1)),
        .MEM_DATA_WIDTH                                     (AXIS_TDATA_WIDTH * 8),
        .MEM_INITIALISE                                     (0)
    ) axis_tdata_fifo (
        .clka                                               (clk),
        .porta_addr                                         (stage_fifo_write_ptr),
        .porta_en                                           (IN_STATE != IN_STATE_RESET && IN_STATE != IN_STATE_IDLE),
        .porta_wr_en                                        (1'b1),
        .porta_data_in                                      (stage_in_axis_tdata),
        .porta_data_out                                     (),

        .clkb                                               (clk),
        .portb_addr                                         (fifo_read_ptr),
        .portb_en                                           (1'b1),
        .portb_wr_en                                        (1'b0),
        .portb_data_in                                      (),
        .portb_data_out                                     (relay_fetch_axis_tdata)
    );

    generic_bram #(
        .USE_VALID_BIT                                      (0),
        .MEM_ADDR_WIDTH                                     (GetWidth(DATA_BUFFER_DEPTH - 1)),
        .MEM_DATA_WIDTH                                     (AXIS_TDATA_WIDTH),
        .MEM_INITIALISE                                     (0)
    ) axis_tkeep_fifo (
        .clka                                               (clk),
        .porta_addr                                         (stage_fifo_write_ptr),
        .porta_en                                           (IN_STATE != IN_STATE_RESET && IN_STATE != IN_STATE_IDLE),
        .porta_wr_en                                        (1'b1),
        .porta_data_in                                      (stage_in_axis_tkeep),
        .porta_data_out                                     (),

        .clkb                                               (clk),
        .portb_addr                                         (fifo_read_ptr),
        .portb_en                                           (1'b1),
        .portb_wr_en                                        (1'b0),
        .portb_data_in                                      (),
        .portb_data_out                                     (relay_fetch_axis_tkeep)
    );

    generic_bram #(
        .USE_VALID_BIT                                      (0),
        .MEM_ADDR_WIDTH                                     (GetWidth(DATA_BUFFER_DEPTH - 1)),
        .MEM_DATA_WIDTH                                     (1),
        .MEM_INITIALISE                                     (0)
    ) axis_tlast_fifo (
        .clka                                               (clk),
        .porta_addr                                         (stage_fifo_write_ptr),
        .porta_en                                           (IN_STATE != IN_STATE_RESET && IN_STATE != IN_STATE_IDLE),
        .porta_wr_en                                        (1'b1),
        .porta_data_in                                      (stage_in_axis_tlast),
        .porta_data_out                                     (),

        .clkb                                               (clk),
        .portb_addr                                         (fifo_read_ptr),
        .portb_en                                           (1'b1),
        .portb_wr_en                                        (1'b0),
        .portb_data_in                                      (),
        .portb_data_out                                     (relay_fetch_axis_tlast)
    );

    generate
        if(AXIS_TSBAND_ENABLE > 0) begin
            generic_bram #(
                .USE_VALID_BIT                              (0),
                .MEM_ADDR_WIDTH                             (GetWidth(DATA_BUFFER_DEPTH - 1)),
                .MEM_DATA_WIDTH                             (AXIS_TSBAND_WIDTH),
                .MEM_INITIALISE                             (0)
            ) axis_tsband_fifo (
                .clka                                       (clk),
                .porta_addr                                 (fifo_write_tctr),
                .porta_en                                   (IN_STATE != IN_STATE_RESET && IN_STATE != IN_STATE_IDLE),
                .porta_wr_en                                (1'b1),
                .porta_data_in                              (stage_in_axis_tsband),
                .porta_data_out                             (),

                .clkb                                       (clk),
                .portb_addr                                 (fifo_read_tctr),
                .portb_en                                   (1'b1),
                .portb_wr_en                                (1'b0),
                .portb_data_in                              (),
                .portb_data_out                             (relay_fetch_axis_tsband)
            );
        end

        if(AXIS_TWSBAND_ENABLE > 0) begin
            generic_bram #(
                .USE_VALID_BIT                              (0),
                .MEM_ADDR_WIDTH                             (GetWidth(DATA_BUFFER_DEPTH - 1)),
                .MEM_DATA_WIDTH                             (AXIS_TWSBAND_WIDTH),
                .MEM_INITIALISE                             (0)
            ) axis_twsband_fifo (
                .clka                                       (clk),
                .porta_addr                                 (stage_fifo_write_ptr),
                .porta_en                                   (IN_STATE != IN_STATE_RESET && IN_STATE != IN_STATE_IDLE),
                .porta_wr_en                                (1'b1),
                .porta_data_in                              (stage_in_axis_twsband),
                .porta_data_out                             (),

                .clkb                                       (clk),
                .portb_addr                                 (fifo_read_ptr),
                .portb_en                                   (1'b1),
                .portb_wr_en                                (1'b0),
                .portb_data_in                              (),
                .portb_data_out                             (relay_fetch_axis_twsband)
            );
        end

        if(BYTE_COUNT_ENABLE > 0) begin
            generic_bram #(
                .USE_VALID_BIT                              (0),
                .MEM_ADDR_WIDTH                             (GetWidth(DATA_BUFFER_DEPTH - 1)),
                .MEM_DATA_WIDTH                             (GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH)),
                .MEM_INITIALISE                             (0)
            ) axis_tcount_fifo (
                .clka                                       (clk),
                .porta_addr                                 (fifo_write_tctr),
                .porta_en                                   (IN_STATE != IN_STATE_RESET && IN_STATE != IN_STATE_IDLE),
                .porta_wr_en                                (1'b1),
                .porta_data_in                              (stage_in_axis_tcount),
                .porta_data_out                             (),

                .clkb                                       (clk),
                .portb_addr                                 (fifo_read_tbctr),
                .portb_en                                   (1'b1),
                .portb_wr_en                                (1'b0),
                .portb_data_in                              (),
                .portb_data_out                             (relay_fetch_axis_tcount)
            );
        end
    endgenerate

    assign fifo_empty                                       = (fifo_write_tptr == fifo_read_ptr)
                                                                && (fifo_write_toverf == fifo_read_overf);
    assign fifo_out_valid                                   = !fifo_empty;

    assign fifo_tcempty                                     = (fifo_write_tctr == fifo_read_tctr)
                                                                && (fifo_write_tcoverf == fifo_read_tcoverf);
    assign fifo_out_tcvalid                                 = !fifo_tcempty;

    assign fifo_tbcempty                                    = (fifo_write_tctr == fifo_read_tbctr)
                                                                && (fifo_write_tcoverf == fifo_read_tbcoverf);
    assign fifo_out_tbcvalid                                = !fifo_tbcempty;

    //
    // THROUGHPUT STABLISATION WITH PREFETCH REGISTERS
    // ==============================
    //
    // THE OUTPUT READY SIGNAL IS FIRST CONSIDERED AT TIME a, AS THE FIRST DATUM AS A RESULT OF
    //  MEMORY ADDRESS TRANSITION TWO CYCLES BEFORE IS DRAINED INTO THE OUTPUT BUS AND AWAITS
    //  ACKNOWLEDGEMENT. IT SHOULD BE AWARE THAT, AT THIS POINT OF TIME, TWO MEMORY ADDRESS
    //  TRANSITIONS AND ONE MEMORY RETURNED-DATA TRANSITION ALREADY TOOK PLACE. AND THE SECOND
    //  MEMORY RETURNED-DATA TRANSITION IS IMMINENT AS THE SECOND ADDRESS IS ALREADY IN THE ADDRESS
    //  BUS. THEREFORE, IF THE CURRENT DATUM ON THE OUTPUT BUS IS NOT ACKNOWLEDGED AND REQUIRED TO
    //  REMAIN IN THE OUTPUT BUS FOR MORE CYCLES, THE CURRENT DATUM ON THE MEMORY DATA BUS WILL
    //  HAVE TO BE PROPERLY BUFFERED OR LOST.
    //
    //  AT TIME b, THE READY SIGNAL DEASSERTED, MEANING THE OUTPUT DATUM HAS NOT BEEN ACKNOWLEDGED
    //  AS ACQUIRED BY THE RECEIVING END. THE DATUM IN THE MEMORY RETURNED-DATA BUS HAS TO BE
    //  CACHED AS THE ADDRESS TRANSITION FOR FETCHING THE NEXT DATUM HAS ALREADY BEEN MADE IN THE
    //  PREVIOUS CYCLE. IF THE DATUM IN THE MEMORY RETURNED-DATA BUS IS NOT PROPERLY STACKED, IT
    //  WILL BE LOST AND REPLACED BY THE NEXT DATUM IN THE NEXT CYCLE. ADDRESS TRANSITION SHOULD
    //  BE STOPPED ALONGSIDE AS THE PREFETCH REGISTERS HAS ONLY TWO LEVELS STORING TWO DATA AS A
    //  WHOLE. FURTHER ADVANCEMENT OF ADDRESS WILL RENDER THE PREFETCH REGISTERS SET OVERFLOW. IN
    //  ACTUAL IMPLEMENTATION, PREDICATE STACK_SIZE > 0 AND M_AXIS_TREADY == 1'B1 IS FOR PREFETCH
    //  REGISTERS SET UTILISATION CONTROL.
    //
    //  AT TIME c, THE READY SIGNAL ASSERTED TO ACKNOWLEDGE THE DANGLING OUTPUT. ADDRESS TRANSITION
    //  RESTARTED WITH THE ACTUAL DATA REVEALING AT THE MEMORY RETURNED-DATA BUS ONE CYCLE LATER
    //  AND AT THE ACTUAL OUTPUT BUS TWO CYCLES SUBSEQUENT TO THE TRANSITION.
    //
    //  IT SHOULD BE NOTED FOR THE DIFFERENCE IN CYCLES FROM MEMORY ADDRESS TRANSITION TO MEMORY
    //  DATA TRANSITION, I.E., DURATION A AND C ARE TWO CYCLES. AND HENCE, A BUFFER OF TWO CYCLES
    //  OF DATA ARE REQUIRED, prefetch_reg[0] AND prefetch_reg[1].
    //
    //  A SCENARIO TO HAVE ALL THE PREFETCH REGISTERS ALWAYS FILLED SHOULD BE FORBIDDEN. AS THE
    //  HIGHER ORDER PREFETCH REGISTERS IS TO OCCUPY MEMORY RETURNED DATA AS A RESULT OF PREDICTIVE
    //  ADDRESS STREAK. IF THE HIGHER ORDER PREFETCH REGISTERS ARE NOT FLUSHED WHEN READY SIGNAL
    //  REASSERT, NO EXTRA SPACE WILL BE AVAILABLE TO STORE STREAKS UPON ANOTHER INTERRUPTION.
    //
    //                                          a       b       c
    //                                          v       v       v
    //                          |<----- A ----->|               |<----- C ----->|
    //              clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    //   fetch_addr_seq |<   0  ><   1  ><   2  ><       3      ><   4  ><   5  ><   6  ><   7  ><   8  >
    //   fetch_data_seq |<       0      ><   1  ><   2  ><       3      ><   4  ><   5  ><   6  ><   7  >
    // -----------------|--------------------------------------------------------------------------------
    //       STAB_STACK |<       0      ><       1      ><   2  ><                   1                  >
    //  prefetch_reg[1] |<               X              ><                       2                      >
    //  prefetch_reg[0] |<       X      ><   0  ><       1      ><   2  ><   3  ><   4  ><   5  ><   6  >
    //          m_valid |________________/---------------------------------------------------------------
    //          m_ready |-------------------------------\_______/----------------------------------------
    //
    // EXAMPLES
    // ==============================
    //
    // IN THE FOLLOWING EXAMPLES, prefetch_reg IS EQUIVALENT TO prefetch_reg[1], AND THE IDENTICAL PLACEHOLD
    //   OF prefetch_reg[0] IS NOT SHOWN. PREDICT_STACK SHALL BE TREATED AS THE SAME AS STABLISER STACK SIZE,
    //   I.E. STAB_STACK. AND THE CHOICE OF NAME IN THE FOLLOWING EXAMPLES REFLECT VARIABLE NAMES IN THIS
    //   ACTUAL IMPLEMENTATION.
    //
    //  AN UN-INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><   8   ><   9   ><   10  ><   11  ><   12  ><   13  ><   14  >
    //    fetch_data_seq |<  ...  ><   6   ><   7   ><   8   ><   9   ><   10  ><   11  ><   12  ><   13  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<  ...  ><                                   1                                  >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<  ...  ..  ...  ><   6   ><   7   ><   8   ><   9   ><   10  ><   11  ><   12  >
    //      prefetch_reg |<                                       X                                       >
    //     m_axis_tready |/- ... --------------------------------------------------------------------------
    //
    //  A READY START CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><   8   ><   9   ><   10  ><   11  ><   12  ><   13  ><   14  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><   8   ><   9   ><   10  ><   11  ><   12  ><   13  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><                              1                              >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><   6   ><   7   ><   8   ><   9   ><   10  ><   11  ><   12  >
    //      prefetch_reg |<                                       X                                       >
    //     m_axis_tready |---------------------------------------------------------------------------------
    //
    //  A READY START MULTI-TRANSACTION CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><   8   ><        9       ><   10  ><   11  ><   12  ><   13  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><   8   ><        9       ><   10  ><   11  ><   12  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/--------------------------\________/-----------------------------------
    //     PREDICT_STACK |<        0       ><            1            ><   0   ><            1            >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><   6   ><   7   ><   8   ><   X   ><   9   ><   10  ><   11  >
    //      prefetch_reg |<                                       X                                       >
    //     m_axis_tvalid |__________________/--------------------------\________/--------------------------
    //     m_axis_tready |---------------------------------------------------------------------------------
    //
    //  A ONE-CYCLE INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><        8       ><        9       ><   10  ><   11  ><   12  >
    //    fetch_data_seq |<  ...  ><   6   ><   7   ><        8       ><   9   ><   10  ><   11  ><   12  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |/- ... --------------------------------------------------------------------------
    //     PREDICT_STACK |<       ...      ><   1   ><   2   ><                     1                     >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<  ...  ..  ...  ><        6       ><   7   ><   8   ><   9   ><   10  ><   11  >
    //      prefetch_reg |<            X            ><                          7                         >
    //     m_axis_tready |/- ... -----------\________/-----------------------------------------------------
    //
    //  A ONE-CYCLE WAIT START CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><        8       ><   9   ><   10  ><   11  ><   12  ><   13  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><        8       ><   9   ><   10  ><   11  ><   12  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><   2   ><                     1                     >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><        6       ><   7   ><   8   ><   9   ><   10  ><   11  >
    //      prefetch_reg |<            X            ><                          7                         >
    //     m_axis_tready |___________________________/-----------------------------------------------------
    //
    //  A ONE-CYCLE WAIT START + INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><        8       ><        9       ><   10  ><   11  ><   12  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><        8       ><        9       ><   10  ><   11  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><   2   ><   1   ><   2   ><            1            >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><        6       ><        7       ><   8   ><   9   ><   10  >
    //      prefetch_reg |<            X            ><        7       ><                 8                >
    //     m_axis_tready |___________________________/--------\________/-----------------------------------
    //
    //  A ONE-CYCLE WAIT START + INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><        8       ><   9   ><       10       ><   11  ><   12  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><        8       ><   9   ><       10       ><   11  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><   2   ><        1        ><   2   ><       1       >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><        6       ><   7   ><        8       ><   9   ><   10  >
    //      prefetch_reg |<            X            ><            7            ><            9            >
    //     m_axis_tready |___________________________/-----------------\________/--------------------------
    //
    //  A TWO-CYCLE INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><            8            ><   9   ><   10  ><   11  ><   12  >
    //    fetch_data_seq |<  ...  ><   6   ><   7   ><            8            ><   9   ><   10  ><   11  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<  ...  ><        1       ><        2       ><                 1                >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<  ...  ..  ...  ><            6            ><   7   ><   8   ><   9   ><   10  >
    //      prefetch_reg |<            X            ><                          7                         >
    //     m_axis_tready |/- ... -----------\_________________/--------------------------------------------
    //
    //  A TWO-CYCLE WAIT START CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><            8            ><   9   ><   10  ><   11  ><   12  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><            8            ><   9   ><   10  ><   11  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><        2       ><                 1                >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><            6            ><   7   ><   8   ><   9   ><   10  >
    //      prefetch_reg |<            X            ><                          7                         >
    //     m_axis_tready |____________________________________/--------------------------------------------
    //
    //  A TWO-CYCLE WAIT START + INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><            8            ><        9       ><   10  ><   11  ><   12  ><   13  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><            8            ><        9       ><   10  ><   11  ><   12  >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><        2       ><   1   ><   2   ><                 1                >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><            6            ><        7       ><   8   ><   9   ><   10  ><   11  >
    //      prefetch_reg |<            X            ><            7            ><                     8                     >
    //     m_axis_tready |____________________________________/--------\________/--------------------------------------------
    //
    //
    //  A TWO-CYCLE WAIT START + INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><            8            ><   9   ><       10       ><   11  ><   12  ><   13  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><            8            ><   9   ><       10       ><   11  ><   12  >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><        2       ><        1       ><   2   ><             1           >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><            6            ><   7   ><        8       ><   9   ><   10  ><   11  >
    //      prefetch_reg |<            X            ><                 7                ><                 9                >
    //     m_axis_tready |____________________________________/-----------------\________/-----------------------------------
    //
    //
    //  A THREE-CYCLE INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><                 8                ><   9   ><   10  ><   11  >
    //    fetch_data_seq |<  ...  ><   6   ><   7   ><                 8                ><   9   ><   10  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<       ...      ><   1   ><            2            ><            1            >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<  ...  ..  ...  ><                6                 ><   7   ><   8   ><   9   >
    //      prefetch_reg |<            X            ><                          7                         >
    //     m_axis_tready |/- ... -----------\__________________________/-----------------------------------
    //
    //  A THREE-CYCLE WAIT START CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><                 8                ><   9   ><   10  ><   11  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><                 8                ><   9   ><   10  >
    //  -----------------|---------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><            2            ><            1            >
    //  -----------------|---------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><                 6                ><   7   ><   8   ><   9   >
    //      prefetch_reg |<            X            ><                          7                         >
    //     m_axis_tready |_____________________________________________/-----------------------------------
    //
    //  A THREE-CYCLE WAIT START + INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><                 8                ><        9       ><   10  ><   11  ><   12  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><                 8                ><        9       ><   10  ><   11  >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><            2            ><   1   ><   2   ><             1           >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><                 6                ><        7       ><   8   ><   9   ><   10  >
    //      prefetch_reg |<            X            ><                 7                ><                 8                >
    //     m_axis_tready |_____________________________________________/--------\________/-----------------------------------
    //
    //  A THREE-CYCLE WAIT START + INTERRUPTED CHANNEL
    //  ------------------------------
    //              clk  |/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____/---\____
    //    fetch_addr_seq |<   6   ><   7   ><                 8                ><   9   ><       10       ><   11  ><   12  >
    //    fetch_data_seq |<   X   ><   6   ><   7   ><                 8                ><   9   ><       10       ><   11  >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //       fetch_valid |_________/-----------------------------------------------------------------------------------------
    //     PREDICT_STACK |<        0       ><   1   ><            2            ><        1       ><   2   ><        1       >
    //  -----------------|---------------------------------------------------------------------------------------------------
    //   output_chnl_seq |<        X       ><                 6                ><   7   ><        8       ><   9   ><   10  >
    //      prefetch_reg |<            X            ><                     7                     ><            9            >
    //     m_axis_tready |_____________________________________________/-----------------\________/--------------------------
    //
    // PREFETCH REGISTERS UTILISATION PREDICATES
    // ==============================
    //
    // FROM PREVIOUS DISCUSSION, IT IS KNOWN THAT AN ADDRESS ADVANCEMENT HAS NO EFFECT TO THE PREFETCH
    //  REGISTERS STACK IN THE IMMEDIATE NEXT CYCLE. CONSIDER THE FOLLOWING SCENARIO WHICH MULTIPLE
    //  SPORADIC BURST IS FETCHED FROM MEMORY.
    //
    //  AT TIME a, THE READ POINTER WILL ADVANCE, BUT IT HAS NO PRIOR KNOWLEDGE OF WHETHER THERE ARE
    //  VALID DATA TO FOLLOW. AS SUCH, THE ADVANCEMENT MUST APPREHEND FOR A STALL WITH VALID DATA IN
    //  THE SUBSEQUENT ADDRESS IN ORDER TO ENSURE THAT NO DATA LOSS SUFFERED FROM OVERFLOWING OF
    //  PREFETCH SET. AT TIME b, A NEW DATA BURST IS OBSERVED TO BE VALID IS RESIDING IN MEMORY. IN
    //  CONTRAST TO THE CONSECUTIVE STREAM THAT HAS TO APPREHEND STALLS WITH VALID DATA, THE INTER-
    //  BURST INTERVAL OF TWO CYCLES (1 CYCLE OF BURST SEPARATION AND 1 CYCLE FOR VALID OUTPUT SIGNAL
    //  OBSERVANCE) ALLOWS STACK SIZE TO STABLISE AS TWO CYCLES ARE REQUIRED FROM MEMORY ADDRESS
    //  TRANSITION TO RELEVANT DATA IS CORRECTLY PROPAGATE. HENCE, IF THE PREFETCH REGISTERS ARE NOT
    //  FULLY OCCUPIED, TRANSITION OF MEMORY ADDRESS SHOULD BE ALLOWED AS EMPTY SLOT IN PREFETCH
    //  REGISTERS FOR THE RESULTING DATUM IS GUARANTEED. AS SUCH, THE FETCHING STATE MACHINE HAS TO
    //  TREAT START OF BURST CASE DIFFERENT TO THE ORDINARY BURST CONTINUING CASE TO EQUIP THE START
    //  CASE WITH A GREEDY READY SIGNAL.
    //
    //  AT POINT c AND d, THE PREDICATE USED BEING THE BURST CONTINUATION PREDICATE THAT REQUIRES
    //  STALL APPREHENSION.
    //
    //                                          a               b       c       d
    //                                          v               v       v       v
    //              clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    // -----------------|--------------------------------------------------------------------------------
    //   fetch_addr_seq |<   0  ><   1  ><   2  ><       3      ><       4      ><   5  ><   6  ><   7  >
    //           *valid |------------------------\_______/-----------------------------------------------
    //            *last |________________/-------\_______________________________________________________
    // -----------------|--------------------------------------------------------------------------------
    //   fetch_data_seq |<       0      ><   1  ><   2  ><       3      ><       4      ><   5  ><   6  >
    //           *valid |________/-----------------------\_______/---------------------------------------
    //            *last |________________________/-------\_______________________________________________
    // -----------------|--------------------------------------------------------------------------------
    //       STAB_STACK |<       0      ><               1              ><   2  ><           1          >
    //  prefetch_reg[1] |<                       X                      ><               3              >
    //  prefetch_reg[0] |<       X      ><   0  ><   1  ><           2          ><   3  ><   4  ><   5  >
    //          m_valid |________________/---------------------------------------------------------------
    //          m_ready |---------------------------------------\_______________/------------------------
    //
    //                                          a               b       c       d
    //                                          v               v       v       v
    //              clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    // -----------------|--------------------------------------------------------------------------------
    //   fetch_addr_seq |<   0  ><   1  ><   2  ><       3      ><   4  ><   5  ><   6  ><   7  ><   8  >
    //           *valid |------------------------\_______/-----------------------------------------------
    //            *last |________________/-------\_______________________________________________________
    // -----------------|--------------------------------------------------------------------------------
    //   fetch_data_seq |<       0      ><   1  ><   2  ><       3      ><   4  ><   5  ><   6  ><   7  >
    //           *valid |________/-----------------------\_______/---------------------------------------
    //            *last |________________________/-------\_______________________________________________
    // -----------------|--------------------------------------------------------------------------------
    //       STAB_STACK |<       0      ><                               1                              >
    //  prefetch_reg[1] |<                                       X                                      >
    //  prefetch_reg[0] |<       X      ><   0  ><   1  ><       2      ><   3  ><   4  ><   5  ><   6  >
    //          m_valid |________________/---------------------------------------------------------------
    //          m_ready |---------------------------------------\_______/--------------------------------
    //
    //                                          a               b       c       d
    //                                          v               v       v       v
    //              clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
    // -----------------|--------------------------------------------------------------------------------
    //   fetch_addr_seq |<   0  ><   1  ><   2  ><       3      ><   4  ><   5  ><   6  ><   7  ><   8  >
    //           *valid |------------------------\_______/-----------------------------------------------
    //            *last |________________/-------\_______________________________________________________
    // -----------------|--------------------------------------------------------------------------------
    //   fetch_data_seq |<       0      ><   1  ><   2  ><       3      ><   4  ><   5  ><   6  ><   7  >
    //           *valid |________/-----------------------\_______/---------------------------------------
    //            *last |________________________/-------\_______________________________________________
    // -----------------|--------------------------------------------------------------------------------
    //       STAB_STACK |<       0      ><       1      ><   2  ><                   1                  >
    //  prefetch_reg[1] |<               X              ><                       2                      >
    //  prefetch_reg[0] |<       X      ><   0  ><       1      ><   2  ><   3  ><   4  ><   5  ><   6  >
    //          m_valid |________________/---------------------------------------------------------------
    //          m_ready |-------------------------------\_______/----------------------------------------
    //

    // WORD-BASED FIFO CONVEYER BELL STATE MACHINE
    //
    initial
    begin
        FETCH_STATE                                         = FETCH_STATE_RESET;

        fifo_read_ptr                                       = 0;
        fifo_read_overf                                     = 0;
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            FETCH_STATE                                     <= FETCH_STATE_RESET;
        else
            case(FETCH_STATE)
                FETCH_STATE_RESET:
                begin
                    FETCH_STATE                             <= FETCH_STATE_IDLE;

                    fifo_read_ptr                           <= 0;
                    fifo_read_overf                         <= 0;
                end
                FETCH_STATE_IDLE:
                    if(relay_stab_fetch_greedy_ready == 1'b1)
                        if(fifo_out_valid == 1'b1) begin
                            FETCH_STATE                     <= FETCH_STATE_PASSTHROUGH;

                            fifo_read_ptr                   <= (fifo_read_ptr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_read_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_read_overf             <= (fifo_read_overf + 1) % DATA_OVERF_DEPTH;
                        end
                FETCH_STATE_PASSTHROUGH, FETCH_STATE_STALL:
                    if(relay_stab_fetch_ready == 1'b1) begin
                        if(fifo_out_valid == 1'b1) begin
                            FETCH_STATE                     <= FETCH_STATE_PASSTHROUGH;

                            fifo_read_ptr                   <= (fifo_read_ptr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_read_ptr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_read_overf             <= (fifo_read_overf + 1) % DATA_OVERF_DEPTH;
                        end
                        else
                            FETCH_STATE                     <= FETCH_STATE_IDLE;
                    end
                    else
                        FETCH_STATE                         <= FETCH_STATE_STALL;
            endcase
    end

    initial
    begin
        STAB_STACK                                          = 0;

        pf_fetch_axis_tdata[0]                              = {AXIS_TDATA_WIDTH{8'h0}};
        pf_fetch_axis_tdata[1]                              = {AXIS_TDATA_WIDTH{8'h0}};

        pf_fetch_axis_tkeep[0]                              = {AXIS_TDATA_WIDTH{1'b0}};
        pf_fetch_axis_tkeep[1]                              = {AXIS_TDATA_WIDTH{1'b0}};

        pf_fetch_axis_tlast[0]                              = 1'b0;
        pf_fetch_axis_tlast[1]                              = 1'b0;
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0) begin
            STAB_STACK                                      <= 0;

            pf_fetch_axis_tdata[0]                          <= {AXIS_TDATA_WIDTH{8'h0}};
            pf_fetch_axis_tdata[1]                          <= {AXIS_TDATA_WIDTH{8'h0}};

            pf_fetch_axis_tkeep[0]                          <= {AXIS_TDATA_WIDTH{1'b0}};
            pf_fetch_axis_tkeep[1]                          <= {AXIS_TDATA_WIDTH{1'b0}};

            pf_fetch_axis_tlast[0]                          <= 1'b0;
            pf_fetch_axis_tlast[1]                          <= 1'b0;
        end
        else
            case(STAB_STACK)
                0:  if(relay_stab_fetch_valid == 1'b1) begin
                        STAB_STACK                          <= STAB_STACK + 1;

                        pf_fetch_axis_tdata[0]              <= relay_fetch_axis_tdata;
                        pf_fetch_axis_tkeep[0]              <= relay_fetch_axis_tkeep;
                        pf_fetch_axis_tlast[0]              <= relay_fetch_axis_tlast;
                    end
                1:  if(relay_out_stab_ready == 1'b1)
                        if(relay_stab_fetch_valid == 1'b1) begin
                            STAB_STACK                      <= STAB_STACK + 1 - 1;

                            pf_fetch_axis_tdata[0]          <= relay_fetch_axis_tdata;
                            pf_fetch_axis_tkeep[0]          <= relay_fetch_axis_tkeep;
                            pf_fetch_axis_tlast[0]          <= relay_fetch_axis_tlast;
                        end
                        else
                            STAB_STACK                      <= STAB_STACK - 1;
                    else
                        if(relay_stab_fetch_valid == 1'b1) begin
                            STAB_STACK                      <= STAB_STACK + 1;

                            pf_fetch_axis_tdata[1]          <= relay_fetch_axis_tdata;
                            pf_fetch_axis_tkeep[1]          <= relay_fetch_axis_tkeep;
                            pf_fetch_axis_tlast[1]          <= relay_fetch_axis_tlast;
                        end
                2:  if(relay_out_stab_ready == 1'b1) begin
                        STAB_STACK                          <= STAB_STACK - 1;

                        pf_fetch_axis_tdata[0]              <= pf_fetch_axis_tdata[1];
                        pf_fetch_axis_tkeep[0]              <= pf_fetch_axis_tkeep[1];
                        pf_fetch_axis_tlast[0]              <= pf_fetch_axis_tlast[1];
                    end
            endcase
    end

    assign relay_stab_fetch_valid                           = (FETCH_STATE == FETCH_STATE_PASSTHROUGH
                                                                || FETCH_STATE == FETCH_STATE_STALL);
    assign relay_stab_fetch_greedy_ready                    = (STAB_STACK < 2 || STAB_STACK == 2
                                                                && relay_out_stab_ready == 1'b1);
    assign relay_stab_fetch_ready                           = (STAB_STACK == 0 || STAB_STACK > 0
                                                                && relay_out_stab_ready == 1'b1);

    // TRANSACTION-BASED FIFO CONVEYER BELL STATE MACHINE
    //
    initial
    begin
        TCFETCH_STATE                                       = FETCH_STATE_RESET;

        fifo_read_tctr                                      = 0;
        fifo_read_tcoverf                                   = 0;
    end

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            TCFETCH_STATE                                   <= FETCH_STATE_RESET;
        else
            case(TCFETCH_STATE)
                FETCH_STATE_RESET:
                begin
                    TCFETCH_STATE                           <= FETCH_STATE_IDLE;

                    fifo_read_tctr                          <= 0;
                    fifo_read_tcoverf                       <= 0;
                end
                FETCH_STATE_IDLE:
                    if(relay_tcstab_tcfetch_greedy_ready == 1'b1)
                        if(fifo_out_tcvalid == 1'b1) begin
                            TCFETCH_STATE                   <= FETCH_STATE_PASSTHROUGH;

                            fifo_read_tctr                  <= (fifo_read_tctr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_read_tctr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_read_tcoverf           <= (fifo_read_tcoverf + 1) % DATA_OVERF_DEPTH;
                        end
                FETCH_STATE_PASSTHROUGH, FETCH_STATE_STALL:
                    if(relay_tcstab_tcfetch_ready == 1'b1) begin
                        if(fifo_out_tcvalid == 1'b1) begin
                            TCFETCH_STATE                   <= FETCH_STATE_PASSTHROUGH;

                            fifo_read_tctr                  <= (fifo_read_tctr + 1) % DATA_BUFFER_DEPTH;

                            if((fifo_read_tctr + 1) % DATA_BUFFER_DEPTH == 0)
                                fifo_read_tcoverf           <= (fifo_read_tcoverf + 1) % DATA_OVERF_DEPTH;
                        end
                        else
                            TCFETCH_STATE                   <= FETCH_STATE_IDLE;
                    end
                    else
                        TCFETCH_STATE                       <= FETCH_STATE_STALL;
            endcase
    end

    initial
        TCSTAB_STACK                                        = 0;

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            TCSTAB_STACK                                    <= 0;
        else
            case(TCSTAB_STACK)
                0:  if(relay_tcstab_tcfetch_valid == 1'b1)
                        TCSTAB_STACK                        <= TCSTAB_STACK + 1;
                1:  if(relay_out_tcstab_ready == 1'b1)
                        if(relay_tcstab_tcfetch_valid == 1'b1)
                            TCSTAB_STACK                    <= TCSTAB_STACK + 1 - 1;
                        else
                            TCSTAB_STACK                    <= TCSTAB_STACK - 1;
                    else
                        if(relay_tcstab_tcfetch_valid == 1'b1)
                            TCSTAB_STACK                    <= TCSTAB_STACK + 1;
                2:  if(relay_out_tcstab_ready == 1'b1)
                        TCSTAB_STACK                        <= TCSTAB_STACK - 1;
            endcase
    end

    assign relay_tcstab_tcfetch_valid                       = (TCFETCH_STATE == FETCH_STATE_PASSTHROUGH
                                                                || TCFETCH_STATE == FETCH_STATE_STALL);
    assign relay_tcstab_tcfetch_greedy_ready                = (TCSTAB_STACK < 2 || TCSTAB_STACK == 2
                                                                && relay_out_tcstab_ready == 1'b1);
    assign relay_tcstab_tcfetch_ready                       = (TCSTAB_STACK == 0 || TCSTAB_STACK > 0
                                                                && relay_out_tcstab_ready == 1'b1);

    generate
        if(AXIS_TSBAND_ENABLE > 0) begin
            initial
            begin
                pf_fetch_axis_tsband[0]                     = {AXIS_TSBAND_WIDTH{1'b0}};
                pf_fetch_axis_tsband[1]                     = {AXIS_TSBAND_WIDTH{1'b0}};
            end

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0) begin
                    pf_fetch_axis_tsband[0]                 <= {AXIS_TSBAND_WIDTH{1'b0}};
                    pf_fetch_axis_tsband[1]                 <= {AXIS_TSBAND_WIDTH{1'b0}};
                end
                else
                    case(TCSTAB_STACK)
                        0:  if(relay_tcstab_tcfetch_valid == 1'b1)
                                pf_fetch_axis_tsband[0]     <= relay_fetch_axis_tsband;
                        1:  if(relay_out_tcstab_ready == 1'b1) begin
                                if(relay_tcstab_tcfetch_valid == 1'b1)
                                    pf_fetch_axis_tsband[0] <= relay_fetch_axis_tsband;
                            end
                            else
                                if(relay_tcstab_tcfetch_valid == 1'b1)
                                    pf_fetch_axis_tsband[1] <= relay_fetch_axis_tsband;
                        2:  if(relay_out_tcstab_ready == 1'b1)
                                pf_fetch_axis_tsband[0]     <= pf_fetch_axis_tsband[1];
                    endcase
            end
        end

        if(AXIS_TWSBAND_ENABLE > 0) begin
            initial
            begin
                pf_fetch_axis_twsband[0]                    = {AXIS_TWSBAND_WIDTH{1'b0}};
                pf_fetch_axis_twsband[1]                    = {AXIS_TWSBAND_WIDTH{1'b0}};
            end

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0) begin
                    pf_fetch_axis_twsband[0]                <= {AXIS_TWSBAND_WIDTH{1'b0}};
                    pf_fetch_axis_twsband[1]                <= {AXIS_TWSBAND_WIDTH{1'b0}};
                end
                else
                    case(STAB_STACK)
                        0:  if(relay_stab_fetch_valid == 1'b1)
                                pf_fetch_axis_twsband[0]    <= relay_fetch_axis_twsband;
                        1:  if(relay_out_stab_ready == 1'b1) begin
                                if(relay_stab_fetch_valid == 1'b1)
                                    pf_fetch_axis_twsband[0]
                                                            <= relay_fetch_axis_twsband;
                            end
                            else
                                if(relay_stab_fetch_valid == 1'b1)
                                    pf_fetch_axis_twsband[1]
                                                            <= relay_fetch_axis_twsband;
                        2:  if(relay_out_stab_ready == 1'b1)
                                pf_fetch_axis_twsband[0]    <= pf_fetch_axis_twsband[1];
                    endcase
            end
        end

        if(BYTE_COUNT_ENABLE > 0) begin
            if(BCCHNL_INDEPENDENT > 0) begin
                // OUTGOING AXI-STREAM CHANNEL INDEPENDENT FIFO CONVEYER BELL STATE MACHINE
                //
                initial
                begin
                    TBCFETCH_STATE                          = FETCH_STATE_RESET;

                    fifo_read_tbctr                         = 0;
                    fifo_read_tbcoverf                      = 0;
                end

                always @(posedge clk or negedge aresetn)
                begin
                    if(aresetn == 1'b0)
                        TBCFETCH_STATE                      <= FETCH_STATE_RESET;
                    else
                        case(TBCFETCH_STATE)
                            FETCH_STATE_RESET:
                            begin
                                TBCFETCH_STATE              <= FETCH_STATE_IDLE;

                                fifo_read_tbctr             <= 0;
                                fifo_read_tbcoverf          <= 0;
                            end
                            FETCH_STATE_IDLE:
                                if(relay_tbcstab_tbcfetch_greedy_ready == 1'b1)
                                    if(fifo_out_tbcvalid == 1'b1) begin
                                        TBCFETCH_STATE      <= FETCH_STATE_PASSTHROUGH;

                                        fifo_read_tbctr     <= (fifo_read_tbctr + 1) % DATA_BUFFER_DEPTH;

                                        if((fifo_read_tbctr + 1) % DATA_BUFFER_DEPTH == 0)
                                            fifo_read_tbcoverf
                                                            <= (fifo_read_tbcoverf + 1) % DATA_OVERF_DEPTH;
                                    end
                            FETCH_STATE_PASSTHROUGH, FETCH_STATE_STALL:
                                if(relay_tbcstab_tbcfetch_ready == 1'b1) begin
                                    if(fifo_out_tbcvalid == 1'b1) begin
                                        TBCFETCH_STATE      <= FETCH_STATE_PASSTHROUGH;

                                        fifo_read_tbctr     <= (fifo_read_tbctr + 1) % DATA_BUFFER_DEPTH;

                                        if((fifo_read_tbctr + 1) % DATA_BUFFER_DEPTH == 0)
                                            fifo_read_tbcoverf
                                                            <= (fifo_read_tbcoverf + 1) % DATA_OVERF_DEPTH;
                                    end
                                    else
                                        TBCFETCH_STATE      <= FETCH_STATE_IDLE;
                                end
                                else
                                    TBCFETCH_STATE          <= FETCH_STATE_STALL;
                        endcase
                end

                initial
                    TBCSTAB_STACK                           = 0;

                always @(posedge clk or negedge aresetn)
                begin
                    if(aresetn == 1'b0)
                        TBCSTAB_STACK                       <= 0;
                    else
                        case(TBCSTAB_STACK)
                            0:  if(relay_tbcstab_tbcfetch_valid == 1'b1)
                                    TBCSTAB_STACK           <= TBCSTAB_STACK + 1;
                            1:  if(relay_out_tbcstab_ready == 1'b1)
                                    if(relay_tbcstab_tbcfetch_valid == 1'b1)
                                        TBCSTAB_STACK       <= TBCSTAB_STACK + 1 - 1;
                                    else
                                        TBCSTAB_STACK       <= TBCSTAB_STACK - 1;
                                else
                                    if(relay_tbcstab_tbcfetch_valid == 1'b1)
                                        TBCSTAB_STACK       <= TBCSTAB_STACK + 1;
                            2:  if(relay_out_tbcstab_ready == 1'b1)
                                    TBCSTAB_STACK           <= TBCSTAB_STACK - 1;
                        endcase
                end

                assign relay_tbcstab_tbcfetch_valid         = (TBCFETCH_STATE == FETCH_STATE_PASSTHROUGH
                                                                || TBCFETCH_STATE == FETCH_STATE_STALL);
                assign relay_tbcstab_tbcfetch_greedy_ready  = (TBCSTAB_STACK < 2 || TBCSTAB_STACK == 2
                                                                && relay_out_tbcstab_ready == 1'b1);
                assign relay_tbcstab_tbcfetch_ready         = (TBCSTAB_STACK == 0 || TBCSTAB_STACK > 0
                                                                && relay_out_tbcstab_ready == 1'b1);
            end
            else begin
                always @(*) TBCFETCH_STATE                  = TCFETCH_STATE;
                always @(*) TBCSTAB_STACK                   = TCSTAB_STACK;
                always @(*) fifo_read_tbctr                 = fifo_read_tctr;
                always @(*) fifo_read_tbcoverf              = fifo_read_tcoverf;

                assign relay_tbcstab_tbcfetch_valid         = relay_tcstab_tcfetch_valid;
            end

            initial
            begin
                pf_fetch_axis_tcount[0]                     = {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
                pf_fetch_axis_tcount[1]                     = {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
            end

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0) begin
                    pf_fetch_axis_tcount[0]                 <= {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
                    pf_fetch_axis_tcount[1]                 <= {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
                end
                else
                    case(TBCSTAB_STACK)
                        0:  if(relay_tbcstab_tbcfetch_valid == 1'b1)
                                pf_fetch_axis_tcount[0]     <= relay_fetch_axis_tcount;
                        1:  if(relay_out_tbcstab_ready == 1'b1) begin
                                if(relay_tbcstab_tbcfetch_valid == 1'b1)
                                    pf_fetch_axis_tcount[0] <= relay_fetch_axis_tcount;
                            end
                            else
                                if(relay_tbcstab_tbcfetch_valid == 1'b1)
                                    pf_fetch_axis_tcount[1] <= relay_fetch_axis_tcount;
                        2:  if(relay_out_tbcstab_ready == 1'b1)
                                pf_fetch_axis_tcount[0]     <= pf_fetch_axis_tcount[1];
                    endcase
            end
        end
    endgenerate

    assign relay_out_axis_tvalid                            = relay_out_stab_valid & relay_out_tcstab_valid;

    assign relay_out_stab_valid                             = STAB_STACK > 0;
    assign relay_out_stab_ready                             = relay_out_axis_tvalid == 1'b1 && m_axis_tready == 1'b1;

    assign relay_out_tcstab_valid                           = TCSTAB_STACK > 0;
    assign relay_out_tcstab_ready                           = relay_out_axis_tvalid == 1'b1 && pf_fetch_axis_tlast[0] == 1'b1
                                                                && m_axis_tready == 1'b1;

    generate
        if(BYTE_COUNT_ENABLE > 0)
            if(BCCHNL_INDEPENDENT > 0)
                assign relay_out_tbcstab_valid              = TBCSTAB_STACK > 0;
            else
                assign relay_out_tbcstab_ready              = relay_out_tcstab_ready;
    endgenerate

    assign m_axis_tdata                                     = relay_out_axis_tvalid == 1'b1
                                                                ? pf_fetch_axis_tdata[0] : {AXIS_TDATA_WIDTH{8'h0}};
    assign m_axis_tkeep                                     = relay_out_axis_tvalid == 1'b1
                                                                ? pf_fetch_axis_tkeep[0] : {AXIS_TDATA_WIDTH{1'b0}};
    assign m_axis_tlast                                     = relay_out_axis_tvalid == 1'b1
                                                                ? pf_fetch_axis_tlast[0] : 1'b0;
    assign m_axis_tvalid                                    = relay_out_axis_tvalid;
    assign s_axis_tready                                    = relay_in_axis_tready;

    generate
        if(AXIS_TSBAND_ENABLE > 0)
            assign m_axis_tsband                            = relay_out_axis_tvalid == 1'b1
                                                                ? pf_fetch_axis_tsband[0] : {AXIS_TSBAND_WIDTH{1'b0}};

        if(AXIS_TWSBAND_ENABLE > 0)
            assign m_axis_twsband                           = relay_out_axis_tvalid == 1'b1
                                                                ? pf_fetch_axis_twsband[0] : {AXIS_TWSBAND_WIDTH{1'b0}};

        if(BYTE_COUNT_ENABLE > 0)
            if(BCCHNL_INDEPENDENT > 0) begin
                assign m_axis_tlen                          = relay_out_tbcstab_valid == 1'b1 ? pf_fetch_axis_tcount[0]
                                                                : {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
                assign m_axis_tlen_tvalid                   = relay_out_tbcstab_valid;
                assign relay_out_tbcstab_ready              = m_axis_tlen_tready;
            end
            else
                assign m_axis_tlen                          = relay_out_axis_tvalid == 1'b1 ? pf_fetch_axis_tcount[0]
                                                                : {GetWidth(DATA_BUFFER_DEPTH * AXIS_TDATA_WIDTH){1'b0}};
    endgenerate

    generate
        if(VALID_TRANS_COUNT > 0) begin
            wire [`INTEGER_WIDTH * 8 - 1 : 0]               true_axis_data_count;

            //
            // IN-FIFO DATUM CALCULATION
            // ==============================
            //
            // TO REFLECT THE ACTUAL DATA RESIDING IN THE COMPLETE FIFO PIPELINE, THE DATA COUNT IS
            //  OBTAINED FROM 1) NUMBER OF WORD RESIDING IN INTERNAL BLOCK RAM, 2) WHETHER VALID WORDS
            //  ARE PRESENTED ON THE OUTPUT DATA BUS OF THE INTERNAL BLOCK RAM AND 3) THE NUMBER OF
            //  WORDS CACHED IN THE PREFETCH REGISTERS.
            //
            //  THE NUMBER OF WORD RESIDING IN INTERNAL BLOCK RAM CAN BE OBTAINED BY CALCULATING THE
            //  DIFFERENCE OF THE WRITE POINTER AND THE READ POINTER. AND IT SHOULD BE ESPECIALLY NOTED
            //  THAT BECAUSE OF THE CYCLE DELAY IN RETRIEVING A DATUM, I.E., THE CYCLE DELAY BETWEEN AN
            //  ADDRESS PRESENTED TO THE OUTPUT ADDRESS BUS AND THE ACTUAL DATA PRESENTED ON THE OUTPUT
            //  DATA BUS, WHETHER AN DATUM IS IN-TRANSIT HAS TO BE PUT INTO CONSIDERATION. THAT IS, WHEN
            //  AN ADDRESS IS PRESENTED TO THE OUTPUT ADDRESS BUS, THE READ POINTER IS ADVANCED AND THE
            //  DIFFERENCE BETWEEN WRITE AND READ POINTER WILL NOT INCLUDE THE WORD IN MEMORY RETRIEVAL.
            //
            //  HENCE, TO ILLUSTRATE BY THE TRANSACTION DIAGRAM BELOW, ONE MIGHT OBTAIN THE "sum", I.E.,
            //  THE ACTUAL NUMBER OF DATUM IN THE FIFO PIPELINE AS A + B + C,
            //
            //   A: (trans_top_addr - fetch_addr_seq), DENOTED AS (top - fetch)addr
            //       + (fetch_addr_seq - fetch_data_seq), DENOTED AS in_transit
            //   B: fetch_data_seq_valid == 1'b1 ? 1 : 0
            //   C: STAB_STACK.
            //
            //              clk |/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___/---\___
            // -----------------|----------------------------------------------------------------------------------------
            //   trans_top_addr |<  ..  ><                               5                              ><      ..      >
            // -----------------|----------------------------------------------------------------------------------------
            //(top - fetch)addr |<  ..  ><   5  ><   4  ><   3  ><   2  ><           1          ><   0  ><      ..      >
            //   fetch_addr_seq |<  ..  ><   0  ><   1  ><   2  ><   3  ><           4          ><   5  ><      ..      >
            //           *valid |________/-------------------------------------------------------------------------------
            //            *last |________________________/-------\_______________________________________________________
            // -----------------|----------------------------------------------------------------------------------------
            //       in_transit |<   0  ><                   1                  ><       0      ><   1  ><       0      >
            //   fetch_data_seq |<      ..      ><   0  ><   1  ><   2  ><   3  ><           4          ><   5  ><  ..  >
            //           *valid |________________/---------------------------------------------------------------\_______
            //            *last |________________________________/-------\_______________________________________________
            // -----------------|----------------------------------------------------------------------------------------
            //       STAB_STACK |<           0          ><           1          ><       2      ><           1          >
            //  prefetch_reg[1] |<                       X                      ><                   3                  >
            //  prefetch_reg[0] |<           X          ><   0  ><   1  ><           2          ><   3  ><   4  ><   5  >
            //          m_valid |________________________/---------------------------------------------------------------
            //          m_ready |-----------------------------------------------\_______________/------------------------
            // -----------------|----------------------------------------------------------------------------------------
            //              sum |<  ..  ><           6          ><   5  ><           4          ><   3  ><   2  ><   1  >
            //
            assign axis_data_count                          = ((fifo_write_toverf == fifo_read_overf
                                                                ? fifo_write_tptr - fifo_read_ptr
                                                                : fifo_write_tptr - fifo_read_ptr + DATA_BUFFER_DEPTH)
                                                                + (FETCH_STATE == FETCH_STATE_PASSTHROUGH ? 1 : 0)
                                                                + STAB_STACK) & {GetWidth(DATA_BUFFER_DEPTH){1'b1}};

            if(BYTE_COUNT_ENABLE > 0)
                assign true_axis_data_count                 = ((fifo_write_overf == fifo_read_overf
                                                                ? fifo_write_ptr - fifo_read_ptr
                                                                : fifo_write_ptr - fifo_read_ptr + DATA_BUFFER_DEPTH)
                                                                + (FETCH_STATE == FETCH_STATE_PASSTHROUGH ? 1 : 0)
                                                                + STAB_STACK + (stage_count_axis_tvalid == 1'b1 ? 1 : 0))
                                                                & {GetWidth(DATA_BUFFER_DEPTH){1'b1}};
            else
                assign true_axis_data_count                 = ((fifo_write_overf == fifo_read_overf
                                                                ? fifo_write_ptr - fifo_read_ptr
                                                                : fifo_write_ptr - fifo_read_ptr + DATA_BUFFER_DEPTH)
                                                                + (FETCH_STATE == FETCH_STATE_PASSTHROUGH ? 1 : 0)
                                                                + STAB_STACK) & {GetWidth(DATA_BUFFER_DEPTH){1'b1}};
        end
        else
            if(BYTE_COUNT_ENABLE > 0)
                assign axis_data_count                      = ((fifo_write_overf == fifo_read_overf
                                                                ? fifo_write_ptr - fifo_read_ptr
                                                                : fifo_write_ptr - fifo_read_ptr + DATA_BUFFER_DEPTH)
                                                                + (FETCH_STATE == FETCH_STATE_PASSTHROUGH ? 1 : 0)
                                                                + STAB_STACK  + (stage_count_axis_tvalid == 1'b1 ? 1 : 0))
                                                                & {GetWidth(DATA_BUFFER_DEPTH){1'b1}};
            else
                assign axis_data_count                      = ((fifo_write_overf == fifo_read_overf
                                                                ? fifo_write_ptr - fifo_read_ptr
                                                                : fifo_write_ptr - fifo_read_ptr + DATA_BUFFER_DEPTH)
                                                                + (FETCH_STATE == FETCH_STATE_PASSTHROUGH ? 1 : 0)
                                                                + STAB_STACK) & {GetWidth(DATA_BUFFER_DEPTH){1'b1}};
    endgenerate
endmodule