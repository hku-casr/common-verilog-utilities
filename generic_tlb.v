`timescale 1ns/1ps

/**
 *
 * generic_tlb.v
 *
 * Short Description: This module implemented a content-addressed memory module
 *  that is supported and initialised by an associated block RAM structure.
 *
 * Long Description: This module implemented a content-addressed memory module
 *  that is supported and initialised by an associated block RAM structure. When
 *  the module finishes initialisation from the associated block RAM, the pin
 *  init_complete toggles to high.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

/**
 *
 * GENERIC_TLB
 *
 * GENERIC PARAMETERS
 * --------------------
 * USE_VALID_BIT       BOOLEAN VARIABLE TO DETERMINE IF VALID BIT IS USED IN
 *                      THE BACK-END MEMORY REFLECTING ANY UNIQUE ADDRESS IS
 *                      PROPERLY INITIALISED OR RESIDING VALID DATA
 * RETURN_CONTENT      BOOLEAN VARIABLE TO INSTRUCT THE MODULE TO RETURN THE
 *                      INDEX/ADDRESS OF THE BACK-END MEMORY RESIDING DATA IN-
 *                      TENDED FROM A LOOKUP, TOGETHER WITH THE CONTENT. NON-0
 *                      PROMPT THE MODULE TO RETURN THE CONTENT
 * BKEND_ADDR_WIDTH    THE BIT WIDTH OF THE ADDRESS FOR ACCESSING MEMORY CON-
 *                      TENT FROM A BACK-END MEMORY
 * BKEND_DATA_WIDTH    THE BIT WIDTH OF THE COMPLETE DATA CONTENT IN THE BACK-
 *                      END MEMORY REGARDLESS OF THEIR RELEVANCE TO THE SEARCH
 *                      OR INTENDED CONTENT
 * TAG_START_BIT       THE INDEX BIT OF THE STARTING LOCATION OF THE TAG FIELD
 *                      IN THE BACK-END MEMORY LINE
 * TAG_BIT_WIDTH       THE BIT WIDTH OF THE TAG FIELD FOR CONTENT ADDRESSING
 * CONTENT_START_BIT   THE INDEX BIT OF THE STARTING LOCATION OF THE CONTENT
 *                      INTENDED TO RETURNED IN THE BACK-END MEMORY LINE
 * CONTENT_BIT_WIDTH   THE BIT WIDTH OF THE CONTENT INTENDED TO BE RETURNED
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * search_tag          IN    THE TAG TO SEARCH FROM THE TRANSLATION-LOOKASIDE
 *                            BUFFER
 * resolve_index       OUT   THE INDEX/ADDRESS OF THE BACK-END MEMORY RESIDING
 *                            THE DATA INTENDED FROM A LOOKUP
 * resolve_content     OUT   THE CONTENT RESULTED FROM A LOOKUP
 * resolve_hit         OUT   THE HIT/MISS RESOLVED FROM A LOOKUP
 * init_complete       OUT   THE MODULE IS READY WHEN THE PIN IS PULLED HIGH
 * bkend_mem_addr      OUT   OUTPUT TO THE ADDRESS BUS OF THE BACK-END MEMORY
 *                            FOR LOOKING UP FROM THE ADDRESS DETERMINED TO RE-
 *                            TAIN RELEVANT INFORMATION
 * bkend_mem_data      IN    INPUT FROM THE DATA BUS OF THE BACK-END MEMORY FOR
 *                            INFORMATION FROM A LOOKUP
 *
 */
module generic_tlb #(
        parameter USE_VALID_BIT                             = 1,
        parameter RETURN_CONTENT                            = 0,
        parameter BKEND_ADDR_WIDTH                          = 8,
        parameter BKEND_DATA_WIDTH                          = 32,
        parameter TAG_START_BIT                             = 16,
        parameter TAG_BIT_WIDTH                             = 16,
        parameter CONTENT_START_BIT                         = 0,
        parameter CONTENT_BIT_WIDTH                         = 16
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire [TAG_BIT_WIDTH        - 1 : 0]          search_tag,
        output reg  [BKEND_ADDR_WIDTH     - 1 : 0]          resolve_index,
        output wire [CONTENT_BIT_WIDTH    - 1 : 0]          resolve_content,
        output wire                                         resolve_hit,
        output wire                                         init_complete,
        output reg  [BKEND_ADDR_WIDTH     - 1 : 0]          bkend_mem_addr,
        input  wire [BKEND_DATA_WIDTH + (USE_VALID_BIT > 0 ? 1 : 0) - 1 : 0]
                                                            bkend_mem_data
    );

    // (* RAM_STYLE = "DISTRIBUTED" *)
    reg    [TAG_BIT_WIDTH       - 1 : 0]                    tag_array[2 ** BKEND_ADDR_WIDTH - 1 : 0];

    reg    [(USE_VALID_BIT > 0 ? 2 ** BKEND_ADDR_WIDTH - 1 : 1) : 0]
                                                            valid_array;
    reg    [CONTENT_BIT_WIDTH     - 1 : 0]                  content_array[(RETURN_CONTENT > 0 ? 2 ** BKEND_ADDR_WIDTH - 1 : 1) : 0];
    reg    [0 : 1]                                          halves[2 ** BKEND_ADDR_WIDTH - 1 : 0];
    reg    [2 ** BKEND_ADDR_WIDTH - 1 : 0]                  halves_tr[0 : 1];

    reg    [1 : 0]                                          STATE;
    localparam STATE_RESET                                  = 2'b00;
    localparam STATE_INIT                                   = 2'b01;
    localparam STATE_IDLE                                   = 2'b10;
    localparam STATE_CONFIRM                                = 2'b11;

    reg    [2 ** BKEND_ADDR_WIDTH - 1 : 0]                  hit_array;
    integer                                                 INIT_COUNTER;

// BEGIN

    initial
    begin
        STATE                                               = STATE_RESET;

        INIT_COUNTER                                        = 0;
        bkend_mem_addr                                      = 0;
    end

    always @(posedge clk or negedge aresetn) begin
        if(aresetn == 1'b0) begin
            STATE                                           <= STATE_RESET;

            INIT_COUNTER                                    <= 0;
        end
        else begin
            case(STATE)
                STATE_RESET:
                begin
                    STATE                                   <= STATE_INIT;

                    bkend_mem_addr                          <= 0;
                end
                STATE_INIT:
                begin
                    valid_array[INIT_COUNTER - 1]           <= USE_VALID_BIT > 0 ? bkend_mem_data[BKEND_DATA_WIDTH] : 1'b0;
                    tag_array[INIT_COUNTER - 1]             <= bkend_mem_data[TAG_START_BIT +: TAG_BIT_WIDTH];
                    content_array[INIT_COUNTER - 1]         <= RETURN_CONTENT > 0 ? bkend_mem_data[CONTENT_START_BIT +: CONTENT_BIT_WIDTH] : {CONTENT_BIT_WIDTH{1'b0}};

                    if(INIT_COUNTER < 2 ** BKEND_ADDR_WIDTH) begin
                        INIT_COUNTER                        <= INIT_COUNTER + 1;
                        bkend_mem_addr                      <= INIT_COUNTER + 1;
                    end

                    // THERE WILL BE AN EXTRA CYCLE FOR THE 2 ** BKEND_ADDR_WIDTH WHICH
                    //  THE READ REQUEST IS DUMMY. BUT THE SAME CYCLE RETURN CONTENT IS
                    //  VALID, BEING THE LAST READ REQUEST ISSUED.
`ifndef CONFIRM_REVLINE
                    else
                        STATE                               <= STATE_IDLE;
`else
                    else begin
                        INIT_COUNTER                        <= 0;
                        bkend_mem_addr                      <= 0;

                        STATE                               <= STATE_CONFIRM;
                    end
`endif
                end
                STATE_CONFIRM:
                begin
                    $display("%X", tag_array[INIT_COUNTER - 1]);

                    if(INIT_COUNTER < 2 ** BKEND_ADDR_WIDTH) begin
                        INIT_COUNTER                        <= INIT_COUNTER + 1;
                        bkend_mem_addr                      <= INIT_COUNTER;
                    end
                    else
                        STATE                               <= STATE_IDLE;
                end
            endcase
        end
    end

    assign init_complete                                    = (STATE == STATE_IDLE);

    generate
        genvar i;

        if(USE_VALID_BIT > 0)
            for(i = 0; i < 2 ** BKEND_ADDR_WIDTH; i = i + 1) begin
                initial
                    hit_array[i]                            = 1'b0;

                always @(posedge clk or negedge aresetn) begin
                    if(aresetn == 1'b0)
                        hit_array[i]                        <= 1'b0;
                    else
                        if(valid_array[i] == 1'b1 && tag_array[i] == search_tag)
                            hit_array[i]                    <= 1'b1;
                        else
                            hit_array[i]                    <= 1'b0;
                end
            end
        else
            for(i = 0; i < 2 ** BKEND_ADDR_WIDTH; i = i + 1) begin
                initial
                    hit_array[i]                            = 1'b0;

                always @(posedge clk or negedge aresetn) begin
                    if(aresetn == 1'b0)
                        hit_array[i]                        <= 1'b0;
                    else
                        if(tag_array[i] == search_tag)
                            hit_array[i]                    <= 1'b1;
                        else
                            hit_array[i]                    <= 1'b0;
                end
            end
    endgenerate

    assign resolve_hit                                      = ^hit_array;

    generate
        genvar order_i, order_j;

        for(order_i = BKEND_ADDR_WIDTH; order_i > 0; order_i = order_i - 1) begin
            for(order_j = 0; order_j < 2 ** (BKEND_ADDR_WIDTH - order_i); order_j = order_j + 1) begin
                localparam full_w                           = 2 ** order_i;
                localparam half_w                           = 2 ** (order_i - 1);
                localparam offset                           = order_j * full_w;

                localparam set_offset                       = 2 ** (BKEND_ADDR_WIDTH - order_i) - 1;

                always @(*) begin
                    halves[set_offset + order_j][0]         = |hit_array[offset + 0 * half_w +: half_w];
                    halves[set_offset + order_j][1]         = |hit_array[offset + 1 * half_w +: half_w];
                end
            end

            localparam set_offset                           = 2 ** (BKEND_ADDR_WIDTH - order_i) - 1;
            localparam set_count                            = 2 ** (BKEND_ADDR_WIDTH - order_i);

            always @(*) begin
                case({|halves_tr[0][set_offset +: set_count], |halves_tr[1][set_offset +: set_count]})
                    2'b01:
                        resolve_index[order_i - 1]         = 1'b1;
                    2'b10:
                        resolve_index[order_i - 1]         = 1'b0;
                    default:
                        resolve_index[order_i - 1]         = 1'b0;
                endcase
            end
        end
    endgenerate

    generate
        genvar tr_i;

        for(tr_i = 0; tr_i < 2 ** BKEND_ADDR_WIDTH - 1; tr_i = tr_i + 1) begin
            always @(*) begin
                halves_tr[0][tr_i]                          = halves[tr_i][0];
                halves_tr[1][tr_i]                          = halves[tr_i][1];
            end
        end
    endgenerate

    generate
        if(RETURN_CONTENT > 0)
            assign resolve_content                          = STATE == STATE_IDLE ? content_array[resolve_index] : {CONTENT_BIT_WIDTH{1'b0}};
        else
            assign resolve_content                          = {CONTENT_BIT_WIDTH{1'bZ}};
    endgenerate
endmodule