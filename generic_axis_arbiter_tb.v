`timescale 1ns/1ps

/**
 *
 * generic_axis_arbiter_tb.v
 *
 * Short Description: 
 *
 * Long Description: 
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

module generic_axis_arbiter_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    parameter CLK_PERIOD                                    = 10;
    parameter MSB_ALIGN                                     = 1;

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter NUM_WAY                                       = 2;
    parameter ARB_HEURISTICS                                = 1;
    parameter AXIS_TDEST_EXT                                = 1;
    parameter AXIS_TSBAND_ENABLE                            = 1;
    parameter AXIS_TWSBAND_ENABLE                           = 1;
    parameter AXIS_TDATA_WIDTH                              = 8;
    parameter AXIS_TDEST_WIDTH                              = 8;
    parameter AXIS_TSBAND_WIDTH                             = 32;
    parameter AXIS_TWSBAND_WIDTH                            = 32;

    reg                                                     clk;
    reg                                                     aresetn;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     in_axis_tdata[NUM_WAY - 1 : 0];
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     in_axis_tkeep[NUM_WAY - 1 : 0];
    wire                                                    in_axis_tlast[NUM_WAY - 1 : 0];
    reg  [AXIS_TDEST_WIDTH     - 1 : 0]                     in_axis_tdest[NUM_WAY - 1 : 0];
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     in_axis_tsband[NUM_WAY - 1 : 0];
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     in_axis_twsband[NUM_WAY - 1 : 0];
    wire                                                    in_axis_tready[NUM_WAY - 1 : 0];
    wire                                                    in_axis_tvalid[NUM_WAY - 1 : 0];
    wire [NUM_WAY * AXIS_TDATA_WIDTH * 8 - 1 : 0]           tmp_axis_tdata;
    wire [NUM_WAY * AXIS_TDATA_WIDTH     - 1 : 0]           tmp_axis_tkeep;
    wire [NUM_WAY - 1 : 0]                                  tmp_axis_tlast;
    wire [NUM_WAY * AXIS_TDEST_WIDTH     - 1 : 0]           tmp_axis_tdest;
    wire [NUM_WAY * AXIS_TSBAND_WIDTH    - 1 : 0]           tmp_axis_tsband;
    wire [NUM_WAY * AXIS_TWSBAND_WIDTH   - 1 : 0]           tmp_axis_twsband;
    wire [NUM_WAY - 1 : 0]                                  tmp_axis_tready;
    wire [NUM_WAY - 1 : 0]                                  tmp_axis_tvalid;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     out_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     out_axis_tkeep;
    wire                                                    out_axis_tlast;
    wire [AXIS_TDEST_WIDTH     - 1 : 0]                     out_axis_tdest;
    wire [AXIS_TSBAND_WIDTH    - 1 : 0]                     out_axis_tsband;
    wire [AXIS_TWSBAND_WIDTH   - 1 : 0]                     out_axis_twsband;
    reg                                                     out_axis_tready;
    wire                                                    out_axis_tvalid;

    integer                                                 schedule_j;

// BEGIN

`include "axis_data_gen.vh"

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    // CYCLE-BASED STATIC SCHEDULE
    // ----------------------------------------
    //
    initial
    begin
        schedule_j                                          = 0;

        out_axis_tready                                     = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            9:  out_axis_tready                             <= 1'b1;
            15: out_axis_tready                             <= 1'b0;
            18: out_axis_tready                             <= 1'b1;
            60: $stop;
        endcase
    end

    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE #0
    // ----------------------------------------
    // TODO: TRANSACTION 1, ...
    //
    initial
        in_axis_tdest[0]                                    = 7;

    always @(posedge clk)
    begin
        if(aresetn == 1'b0)
            `bus_null(0)
        else begin
            `bus_adv(0)

            case(`bus_step(0))
                0: `bus_out(0, 64, 2 * AXIS_TDATA_WIDTH, 0)
                1: `bus_out(0, 30, 1 * AXIS_TDATA_WIDTH, 0)
                2: `bus_out(0, 85, 1 * AXIS_TDATA_WIDTH, 0)
                4: `bus_out(0, 12, 3 * AXIS_TDATA_WIDTH, 0)
                default: `bus_null(0)
            endcase
        end
    end

    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE #1
    // ----------------------------------------
    // TODO: TRANSACTION 1, ...
    //
    initial
        in_axis_tdest[1]                                    = 4;

    always @(posedge clk)
    begin
        if(aresetn == 1'b0)
            `bus_null(1)
        else begin
            `bus_adv(1)

            case(`bus_step(1))
                0:  `bus_out(1, 7, 2 * AXIS_TDATA_WIDTH, 0)
                1:  `bus_out(1, 54, 1 * AXIS_TDATA_WIDTH, 0)
                10: `bus_out(1, 10, 3 * AXIS_TDATA_WIDTH, 0)
                14: $stop;
                default: `bus_null(1)
            endcase
        end
    end

    generate
        genvar sband_i;

        for(sband_i = 0; sband_i < NUM_WAY; sband_i = sband_i + 1) begin
            assign in_axis_tsband[sband_i]                  = schedule_j + sband_i;
            assign in_axis_twsband[sband_i]                 = schedule_j + sband_i;
        end
    endgenerate

    generate
        if(AXIS_TDEST_EXT > 0)
            generic_axis_arbiter #(
                .NUM_WAY                                    (NUM_WAY),
                .ARB_HEURISTICS                             (ARB_HEURISTICS),
                .AXIS_TDEST_EXT                             (1),
                .AXIS_TSBAND_ENABLE                         (AXIS_TSBAND_ENABLE),
                .AXIS_TWSBAND_ENABLE                        (AXIS_TWSBAND_ENABLE),
                .AXIS_TDATA_WIDTH                           (AXIS_TDATA_WIDTH),
                .AXIS_TDEST_WIDTH                           (AXIS_TDEST_WIDTH),
                .AXIS_TSBAND_WIDTH                          (AXIS_TSBAND_WIDTH),
                .AXIS_TWSBAND_WIDTH                         (AXIS_TWSBAND_WIDTH)
            ) UUT (
                .clk                                        (clk),
                .aresetn                                    (aresetn),
                .s_axis_tdata                               (tmp_axis_tdata),
                .s_axis_tkeep                               (tmp_axis_tkeep),
                .s_axis_tlast                               (tmp_axis_tlast),
                .s_axis_tdest                               (tmp_axis_tdest),
                .s_axis_tsband                              (tmp_axis_tsband),
                .s_axis_twsband                             (tmp_axis_twsband),
                .s_axis_tready                              (tmp_axis_tready),
                .s_axis_tvalid                              (tmp_axis_tvalid),
                .m_axis_tdata                               (out_axis_tdata),
                .m_axis_tkeep                               (out_axis_tkeep),
                .m_axis_tlast                               (out_axis_tlast),
                .m_axis_tdest                               (out_axis_tdest),
                .m_axis_tsband                              (out_axis_tsband),
                .m_axis_twsband                             (out_axis_twsband),
                .m_axis_tready                              (out_axis_tready),
                .m_axis_tvalid                              (out_axis_tvalid)
            );
        else
            generic_axis_arbiter #(
                .NUM_WAY                                    (NUM_WAY),
                .ARB_HEURISTICS                             (ARB_HEURISTICS),
                .AXIS_TSBAND_ENABLE                         (AXIS_TSBAND_ENABLE),
                .AXIS_TWSBAND_ENABLE                        (AXIS_TWSBAND_ENABLE),
                .AXIS_TDATA_WIDTH                           (AXIS_TDATA_WIDTH),
                .AXIS_TDEST_WIDTH                           (AXIS_TDEST_WIDTH),
                .AXIS_TSBAND_WIDTH                          (AXIS_TSBAND_WIDTH),
                .AXIS_TWSBAND_WIDTH                         (AXIS_TWSBAND_WIDTH)
            ) UUT (
                .clk                                        (clk),
                .aresetn                                    (aresetn),
                .s_axis_tdata                               (tmp_axis_tdata),
                .s_axis_tkeep                               (tmp_axis_tkeep),
                .s_axis_tlast                               (tmp_axis_tlast),
                .s_axis_tsband                              (tmp_axis_tsband),
                .s_axis_twsband                             (tmp_axis_twsband),
                .s_axis_tready                              (tmp_axis_tready),
                .s_axis_tvalid                              (tmp_axis_tvalid),
                .m_axis_tdata                               (out_axis_tdata),
                .m_axis_tkeep                               (out_axis_tkeep),
                .m_axis_tlast                               (out_axis_tlast),
                .m_axis_tdest                               (out_axis_tdest),
                .m_axis_tsband                              (out_axis_tsband),
                .m_axis_twsband                             (out_axis_twsband),
                .m_axis_tready                              (out_axis_tready),
                .m_axis_tvalid                              (out_axis_tvalid)
            );
    endgenerate

    generate
        genvar siggen_i;

        for(siggen_i = 0; siggen_i < NUM_WAY; siggen_i = siggen_i + 1) begin
            axis_data_gen #(
                .MSB_ALIGN                                  (MSB_ALIGN),
                .CLK_ALIGN                                  (1),
                .AXIS_TDATA_WIDTH                           (AXIS_TDATA_WIDTH)
            ) SIG_GEN (
                .clk                                        (clk),
                .aresetn                                    (aresetn),
                .cmd_bus                                    (`cmd_bus(siggen_i)),
                .axis_tdata                                 (in_axis_tdata[siggen_i]),
                .axis_tkeep                                 (in_axis_tkeep[siggen_i]),
                .axis_tlast                                 (in_axis_tlast[siggen_i]),
                .axis_tready                                (in_axis_tready[siggen_i]),
                .axis_tvalid                                (in_axis_tvalid[siggen_i])
            );

            assign tmp_axis_tdata[siggen_i * AXIS_TDATA_WIDTH * 8 +: AXIS_TDATA_WIDTH * 8]
                                                            = in_axis_tdata[siggen_i];
            assign tmp_axis_tkeep[siggen_i * AXIS_TDATA_WIDTH     +: AXIS_TDATA_WIDTH]
                                                            = in_axis_tkeep[siggen_i];
            assign tmp_axis_tlast[siggen_i]                 = in_axis_tlast[siggen_i];
            assign tmp_axis_tdest[siggen_i * AXIS_TDEST_WIDTH     +: AXIS_TDEST_WIDTH]
                                                            = in_axis_tdest[siggen_i];
            assign tmp_axis_tsband[siggen_i * AXIS_TSBAND_WIDTH   +: AXIS_TSBAND_WIDTH]
                                                            = in_axis_tsband[siggen_i];
            assign tmp_axis_twsband[siggen_i * AXIS_TWSBAND_WIDTH   +: AXIS_TWSBAND_WIDTH]
                                                            = in_axis_twsband[siggen_i];
            assign in_axis_tready[siggen_i]                 = tmp_axis_tready[siggen_i];
            assign tmp_axis_tvalid[siggen_i]                = in_axis_tvalid[siggen_i];
        end
    endgenerate
endmodule