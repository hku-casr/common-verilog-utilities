`timescale 1ns/1ps

/**
 *
 * axis_data_gen.v
 *
 * Short Description: [SIMULATION Only] The utility source file contains module
 *  in Verilog for generation of AXI-Stream data with consideration of byte
 *  non-alignment in a word.
 *
 * Long Description: [SIMULATION Only] The utility source file contains module
 *  in Verilog for generation of AXI-Stream data with consideration of byte
 *  non-alignment in a word.
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

/**
 *
 * AXIS_DATA_GEN
 *
 * GENERIC PARAMETERS
 * --------------------
 * MSB_ALIGN                 ALIGNED TO THE SIDE OF THE MOST SIGNIFICANT BYTE
 * CLK_ALIGN                 CLOCK IS TRIGGERED AT POSITIVE, NEGATIVE OR BOTH
 *                            EDGES WHEN PROVIDED WITH POSITIVE, NEGATIVE
 *                            INTEGER OR NUMERAL ZERO RESPECTIVELY
 * AXIS_TDATA_WIDTH          THE NUMBER OF BYTES IN THE AXI-STREAM DATA BUS
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    THE SIMULATION CLOCK
 * aresetn             IN    ACTIVE-LOW RESET
 * cmd_bus             VAR   COMMAND BUS WITH THE FOLLOWING SUB-CHANNEL BUSES,
 *                            THAT INSTRUCT THE MODULE TO GENERATE TRANSACTION,
 *                            BELOW LISTS SUB-CHANNEL BUSES IN THE ORDER FROM
 *                            LSB TO MSB
 *  .valid             IN    SIGNAL TRANSACTION PARAMETERS IN RESPECTIVE WIRES
 *                            ARE VALID
 *  .ready             IN    SIGNAL TRANSACTION PARAMETERS CAN BE ACCEPTED
 *  .start_value       IN    A TRANSACTION PARAMETER THAT SPECIFIES THE BASE
 *                            VALUE THAT DATA GENERATED IN EACH BYTE OF THE
 *                            STREAM BE INCREMENTED UPON
 *  .byte_count        IN    A TRANSACTION PARAMETER THAT SPECIFIES THE NUMBER
 *                            OF VALID BYTES TO BE OUTPUT TO THE BUS STREAM
 *  .align_offset      IN    A TRANSACTION PARAMETER THAT SPECIFIES THE OFFSET
 *                            INDICATING PRECEDING INVALID BYTES IN A DATA
 *                            STREAM. IT IS ACCEPTABLED FOR AN OFFSET THAT IS
 *                            LARGER THAN THE NUMBER OF BYTES IN A SINGLE WORD
 * axis_*              OUT   AXI-STREAM INTERFACE WITH KEEP, LAST AND READY
 *                            SIGNAL
 *
 */
module axis_data_gen #(
        parameter MSB_ALIGN                                 = 1,
        parameter CLK_ALIGN                                 = 1,
        parameter AXIS_TDATA_WIDTH                          = 8
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        inout  wire [3 * `INTEGER_WIDTH * 8 + 2 - 1 : 0]    cmd_bus,
        output reg  [AXIS_TDATA_WIDTH   * 8     - 1 : 0]    axis_tdata,
        output reg  [AXIS_TDATA_WIDTH           - 1 : 0]    axis_tkeep,
        output wire                                         axis_tlast,
        input  wire                                         axis_tready,
        output wire                                         axis_tvalid
    );

    wire                                                    cmd_valid;
    wire                                                    cmd_ready;
    wire [`INTEGER_WIDTH * 8 - 1 : 0]                       cmd_start_value;
    wire [`INTEGER_WIDTH * 8 - 1 : 0]                       cmd_byte_count;
    wire [`INTEGER_WIDTH * 8 - 1 : 0]                       cmd_align_offset;

    reg  [2 : 0]                                            STATE;
    localparam STATE_IDLE                                   = 3'b000;
    localparam STATE_GEN                                    = 3'b001;
    localparam STATE_ONESHOT                                = 3'b010;
    localparam STATE_LAST                                   = 3'b011;
    localparam STATE_RESET                                  = 3'b111;

    reg  [7  : 0]                                           byte_order [AXIS_TDATA_WIDTH - 1 : 0];
    integer                                                 i;

    integer                                                 t_counter;
    integer                                                 t_byte_count;
    integer                                                 t_align_offset;

// BEGIN

    assign cmd_valid                                        = cmd_bus[0];
    assign cmd_bus[1]                                       = cmd_ready;
    assign cmd_start_value                                  = cmd_bus[2 +: `INTEGER_WIDTH * 8];
    assign cmd_byte_count                                   = cmd_bus[`INTEGER_WIDTH * 8 + 2 +: `INTEGER_WIDTH * 8];
    assign cmd_align_offset                                 = cmd_bus[2 * `INTEGER_WIDTH * 8 + 2 +: `INTEGER_WIDTH * 8];

    initial
    begin
        t_counter                                           <= 0;
        t_byte_count                                        <= 0;
        t_align_offset                                      <= 0;
    end

`define TRANS_STAGE                                                                 \
    begin                                                                           \
        t_counter                                           <= cmd_start_value;     \
        t_byte_count                                        <= cmd_byte_count;      \
        t_align_offset                                      <= cmd_align_offset;    \
    end

`define FSM_SWITCH                                                                  \
    case(STATE)                                                                     \
        STATE_RESET:                                                                \
            STATE                                           <= STATE_IDLE;          \
        STATE_IDLE:                                                                 \
            if(cmd_valid == 1'b1) begin                                             \
                if(cmd_byte_count + cmd_align_offset <= AXIS_TDATA_WIDTH)           \
                    STATE                                   <= STATE_ONESHOT;       \
                else                                                                \
                    STATE                                   <= STATE_GEN;           \
                                                                                    \
                i                                           <= 0;                   \
                                                                                    \
                `TRANS_STAGE                                                        \
            end                                                                     \
        STATE_GEN:                                                                  \
            if(axis_tready == 1'b1) begin                                           \
                i                                           <= i                    \
                                                                + AXIS_TDATA_WIDTH; \
                                                                                    \
                if(t_byte_count + t_align_offset - i - 2 * AXIS_TDATA_WIDTH <= 0)   \
                    STATE                                   <= STATE_LAST;          \
            end                                                                     \
        STATE_ONESHOT, STATE_LAST:                                                  \
            if(axis_tready == 1'b1 && cmd_valid == 1'b1) begin                      \
                if(cmd_byte_count + cmd_align_offset <= AXIS_TDATA_WIDTH)           \
                    STATE                                   <= STATE_ONESHOT;       \
                else                                                                \
                    STATE                                   <= STATE_GEN;           \
                                                                                    \
                i                                           <= 0;                   \
                                                                                    \
                `TRANS_STAGE                                                        \
            end                                                                     \
            else if(axis_tready == 1'b1)                                            \
                STATE                                       <= STATE_IDLE;          \
    endcase

    assign cmd_ready                                        = STATE == STATE_IDLE | axis_tready
                                                                & (STATE == STATE_ONESHOT || STATE == STATE_LAST);

    generate
        if(CLK_ALIGN > 0)
            always @(posedge clk or negedge aresetn)
                if(aresetn == 1'b0)
                    STATE                                   <= STATE_RESET;
                else
                    `FSM_SWITCH
        else if(CLK_ALIGN == 0)
            always @(posedge clk or negedge clk or negedge aresetn)
                if(aresetn == 1'b0)
                    STATE                                   <= STATE_RESET;
                else
                    `FSM_SWITCH
        else
            always @(negedge clk or negedge aresetn)
                if(aresetn == 1'b0)
                    STATE                                   <= STATE_RESET;
                else
                    `FSM_SWITCH
    endgenerate

    generate
        genvar iter_gen;

        for(iter_gen = 0; iter_gen < AXIS_TDATA_WIDTH; iter_gen = iter_gen + 1)
            always @(*)
                byte_order[iter_gen]                        = t_counter + i - t_align_offset + iter_gen;
    endgenerate

    generate
        integer iter_end;

        if(MSB_ALIGN > 0)
            always @(*)
                if(STATE == STATE_RESET || STATE == STATE_IDLE) begin
                    axis_tdata                              = {AXIS_TDATA_WIDTH{8'h0}};
                    axis_tkeep                              = {AXIS_TDATA_WIDTH{1'b0}};
                end
                else begin
                    for(iter_end = 0; iter_end < AXIS_TDATA_WIDTH; iter_end = iter_end + 1)
                        axis_tdata[iter_end * 8 +: 8]       = byte_order[AXIS_TDATA_WIDTH - iter_end - 1];

                    //-- CONDITION TO CHECK FOR THE FIRST (OR LAST IF ONESHOT)
                    //--  BEAT IF THE FIRST BEAT IS MIS-ALIGNED.
                    if(t_align_offset - i > 0)
                        axis_tkeep                          = ({AXIS_TDATA_WIDTH{1'b1}} << (t_byte_count - i < AXIS_TDATA_WIDTH ? AXIS_TDATA_WIDTH - (t_byte_count - i) : 0)) >> t_align_offset;
                    //-- CONDITION TO CHECK FOR THE LAST (OR FIRST IF ONESHOT)
                    //--  BEAT IF THE LAST BEAT IS NOT FULL OR FIRST BEAT THAT
                    //--  IS BOTH NOT MIS-ALIGNED AND NOT FULL.
                    else if(t_byte_count + t_align_offset - i < AXIS_TDATA_WIDTH)
                        axis_tkeep                          = {AXIS_TDATA_WIDTH{1'b1}} << (AXIS_TDATA_WIDTH - (t_byte_count + t_align_offset - i));
                    //-- CONDITION FOR OTHER PROGRESSING BEATS AFTER THE FIRST
                    //--  AND BEFORE THE LAST.
                    else
                        axis_tkeep                          = {AXIS_TDATA_WIDTH{1'b1}};
                end
        else
            always @(*)
                if(STATE == STATE_RESET || STATE == STATE_IDLE) begin
                    axis_tdata                              = {AXIS_TDATA_WIDTH{8'h0}};
                    axis_tkeep                              = {AXIS_TDATA_WIDTH{1'b0}};
                end
                else begin
                    for(iter_end = 0; iter_end < AXIS_TDATA_WIDTH; iter_end = iter_end + 1)
                        axis_tdata[iter_end * 8 +: 8]       = byte_order[iter_end];

                    //-- CONDITION TO CHECK FOR THE FIRST (OR LAST IF ONESHOT)
                    //--  BEAT IF THE FIRST BEAT IS MIS-ALIGNED.
                    if(t_align_offset - i > 0)
                        axis_tkeep                          = ({AXIS_TDATA_WIDTH{1'b1}} >> (t_byte_count - i < AXIS_TDATA_WIDTH ? AXIS_TDATA_WIDTH - (t_byte_count - i) : 0)) << t_align_offset;
                    //-- CONDITION TO CHECK FOR THE LAST (OR FIRST IF ONESHOT)
                    //--  BEAT IF THE LAST BEAT IS NOT FULL OR FIRST BEAT THAT
                    //--  IS BOTH NOT MIS-ALIGNED AND NOT FULL.
                    else if(t_byte_count + t_align_offset - i < AXIS_TDATA_WIDTH)
                        axis_tkeep                          = {AXIS_TDATA_WIDTH{1'b1}} >> (AXIS_TDATA_WIDTH - (t_byte_count + t_align_offset - i));
                    //-- CONDITION FOR OTHER PROGRESSING BEATS AFTER THE FIRST
                    //--  AND BEFORE THE LAST.
                    else
                        axis_tkeep                          = {AXIS_TDATA_WIDTH{1'b1}};
                end
    endgenerate

    assign axis_tlast                                       = (STATE == STATE_LAST || STATE == STATE_ONESHOT)
                                                                ? 1'b1 : 1'b0;
    assign axis_tvalid                                      = (STATE == STATE_RESET || STATE == STATE_IDLE)
                                                                ? 1'b0 : 1'b1;
endmodule