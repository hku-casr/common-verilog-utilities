`timescale 1ns/1ps

/**
 *
 * generic_virtual_output.v
 *
 * Short Description: The module implemented a synthesise-able virtual output
 *  module for running pre-programmed bus content into module-under-test with
 *  live FPGA board design.
 *
 * Long Description: The module implemented a synthesise-able virtual output
 *  module for running pre-programmed bus content into module-under-test within
 *  live FPGA design. Pre-programmed contents are stored into an instantiated
 *  block RAM and are provided by user from a memory configuration file (.hex).
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

/**
 *
 * GENERIC VIRTUAL OUTPUT
 *
 * GENERIC PARAMETERS
 * --------------------
 * DATA_WIDTH          THE NUMBER OF BITS IN THE DATA CHANNEL
 * DATA_DEPTH          THE NUMBER OF TRANSACTION WORDS THAT CAN BE STORED BY THE
 *                      PRELOAD MEMORY
 * REPEAT_ENABLE       BOOLEAN VARIABLE TO DETERMINE IF A NON-STOPPING, REPEATING
 *                      BURST OF TRANSACTIONS SHOULD BE OUTPUT TO DRIVE CONNECTED
 *                      MODULES
 * PROTO_HDSHAKE       BOOLEAN VARIABLE TO DICTATE OUTPUT BUS IS OPERATING WITH
 *                      A HANDSHAKING PROTOCOL. WHEN THE VARIABLE IS ASSERTED,
 *                      THE OUTPUT PROGRAMME IS OBSERVED IN LOGICIAL CYCLE ORDER
 *                      REFERENCING VALID TRANSFER CYCLES INSTEAD OF A STRICT
 *                      TIME ORDER THAT ABIDE STRICTLY TO ACTUAL CYCLE COUNT
 * PROTO_HDSHAKE_INVAL BOOLEAN VARIABLE TO DETERMINE IF A CYCLE OF INVALID DATA
 *                      SHOULD STALL OUTPUT PIPELINE, THE VARIABLE IS OBSERVED
 *                      WHEN GENERIC PROTO_HDSHAKE IS BOOLEAN TRUE
 * PROG_OUTPUT_FILE    MEMORY CONFIGURATION FILE, A BLOCK RAM .HEX MEMORY DUMP
 *                      FILE BEARING THE BUS OUTPUT PROGRAMME INTENDED TO DRIVE
 *                      TEST DESIGNS BY THE MODULE
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * m_axis_*            VAR   OUT-GOING AXI-STREAM INTERFACE
 * axis_data_count     OUT   THE NUMBER OF TRANSACTION WORDS RESIDING IN THE AXI
 *                            STREAM FIFO
 *
 */
module generic_virtual_output #(
        parameter DATA_WIDTH                                = 8,
        parameter DATA_DEPTH                                = 10,
        parameter REPEAT_ENABLE                             = 0,
        parameter PROTO_HDSHAKE                             = 1,
        parameter PROTO_HDSHAKE_INVAL                       = 0,
        parameter PROG_OUTPUT_FILE                          = "prog_file.hex"
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        output wire [DATA_WIDTH - 1 : 0]                    m_data,
        input  wire                                         m_ready,
        output wire                                         m_valid
    );

    localparam USE_VALID_BIT                                = 1;

    function integer GetWidth;
        input integer value;

        begin
            for(GetWidth = 0; value > 0; GetWidth = GetWidth + 1)
                value = value >> 1;
        end
    endfunction

    reg  [1 : 0]                                            FETCH_STATE;
    localparam FETCH_STATE_IDLE                             = 2'b00;
    localparam FETCH_STATE_PIPE                             = 2'b01;
    localparam FETCH_STATE_DONE                             = 2'b10;
    localparam FETCH_STATE_RESET                            = 2'b11;

    (* keep = "true" *)
    reg  [GetWidth(DATA_DEPTH) - 1 : 0]                     FETCH_COUNTER;
    reg  [1 : 0]                                            STAB_STACK;

    wire                                                    fetch_valid;
    wire                                                    fetch_ready;

    wire [(USE_VALID_BIT > 0 ? 1 : 0) + DATA_WIDTH - 1 : 0] bkend_mem_data;
    reg  [DATA_WIDTH - 1 : 0]                               pf_fetch_data[1 : 0];
    reg                                                     pf_fetch_valid[1 : 0];

// BEGIN

    generate
        if(PROTO_HDSHAKE > 0) begin
            initial
            begin
                FETCH_STATE                                 = FETCH_STATE_RESET;

                FETCH_COUNTER                               = 0;
            end

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0)
                    FETCH_STATE                             <= FETCH_STATE_RESET;
                else
                    case(FETCH_STATE)
                        FETCH_STATE_RESET:
                        begin
                            FETCH_STATE                     <= FETCH_STATE_IDLE;

                            FETCH_COUNTER                   <= 0;
                        end
                        FETCH_STATE_IDLE:
                            if(fetch_valid == 1'b1) begin
                                FETCH_STATE                 <= FETCH_STATE_PIPE;

                                FETCH_COUNTER               <= FETCH_COUNTER + 1;
                            end
                        FETCH_STATE_PIPE:
                            if(fetch_ready == 1'b1) begin
                                if(fetch_valid != 1'b1)
                                    FETCH_STATE             <= FETCH_STATE_DONE;

                                FETCH_COUNTER               <= FETCH_COUNTER + 1;
                            end
                    endcase
            end

            if(REPEAT_ENABLE > 0)
                assign fetch_valid                          = 1'b1;
            else
                assign fetch_valid                          = FETCH_COUNTER < DATA_DEPTH;

            if(USE_VALID_BIT > 0 && PROTO_HDSHAKE_INVAL > 0)
                assign fetch_ready                          = STAB_STACK == 0 || STAB_STACK > 0 && (pf_fetch_valid[0] == 1'b0 || m_ready == 1'b1);
            else
                assign fetch_ready                          = STAB_STACK == 0 || STAB_STACK > 0 && m_ready == 1'b1;

            if(USE_VALID_BIT > 0) begin
                initial
                begin
                    pf_fetch_data[0]                        = {DATA_WIDTH{1'b0}};
                    pf_fetch_data[1]                        = {DATA_WIDTH{1'b0}};

                    pf_fetch_valid[0]                       = 1'b0;
                    pf_fetch_valid[1]                       = 1'b0;

                    STAB_STACK                              = 0;
                end

                if(PROTO_HDSHAKE_INVAL > 0) begin
                    always @(posedge clk or negedge aresetn)
                    begin
                        if(aresetn == 1'b0) begin
                            pf_fetch_data[0]                <= {DATA_WIDTH{1'b0}};
                            pf_fetch_data[1]                <= {DATA_WIDTH{1'b0}};

                            pf_fetch_valid[0]               <= 1'b0;
                            pf_fetch_valid[1]               <= 1'b0;

                            STAB_STACK                      <= 0;
                        end
                        else
                            case(STAB_STACK)
                                0:  if(FETCH_STATE == FETCH_STATE_PIPE) begin
                                        pf_fetch_data[0]    <= bkend_mem_data[0 +: DATA_WIDTH];
                                        pf_fetch_valid[0]   <= bkend_mem_data[DATA_WIDTH];

                                        STAB_STACK          <= STAB_STACK + 1;
                                    end
                                1:  if(FETCH_STATE == FETCH_STATE_PIPE) begin
                                        if(pf_fetch_valid[0] == 1'b0 || m_ready == 1'b1) begin
                                            pf_fetch_data[0]
                                                            <= bkend_mem_data[0 +: DATA_WIDTH];
                                            pf_fetch_valid[0]
                                                            <= bkend_mem_data[DATA_WIDTH];

                                            STAB_STACK      <= STAB_STACK + 1 - 1;
                                        end
                                        else begin
                                            pf_fetch_data[1]
                                                            <= bkend_mem_data[0 +: DATA_WIDTH];
                                            pf_fetch_valid[1]
                                                            <= bkend_mem_data[DATA_WIDTH];

                                            STAB_STACK      <= STAB_STACK + 1;
                                        end
                                    end
                                    else
                                        if(pf_fetch_valid[0] == 1'b0 || m_ready == 1'b1)
                                            STAB_STACK      <= STAB_STACK - 1;
                                    // NOTE: pf_fetch_valid[0] == 1'b0 will never happen
                                2:  if(pf_fetch_valid[0] == 1'b0 || m_ready == 1'b1) begin
                                        pf_fetch_data[0]    <= pf_fetch_data[1];
                                        pf_fetch_valid[0]   <= pf_fetch_valid[1];

                                        STAB_STACK          <= STAB_STACK - 1;
                                    end
                            endcase
                    end
                end
                else begin
                    always @(posedge clk or negedge aresetn)
                    begin
                        if(aresetn == 1'b0) begin
                            pf_fetch_data[0]                <= {DATA_WIDTH{1'b0}};
                            pf_fetch_data[1]                <= {DATA_WIDTH{1'b0}};

                            pf_fetch_valid[0]               <= 1'b0;
                            pf_fetch_valid[1]               <= 1'b0;

                            STAB_STACK                      <= 0;
                        end
                        else
                            case(STAB_STACK)
                                0:  if(FETCH_STATE == FETCH_STATE_PIPE) begin
                                        pf_fetch_data[0]    <= bkend_mem_data[0 +: DATA_WIDTH];
                                        pf_fetch_valid[0]   <= bkend_mem_data[DATA_WIDTH];

                                        STAB_STACK          <= STAB_STACK + 1;
                                    end
                                1:  if(FETCH_STATE == FETCH_STATE_PIPE) begin
                                        if(m_ready == 1'b1) begin
                                            pf_fetch_data[0]
                                                            <= bkend_mem_data[0 +: DATA_WIDTH];
                                            pf_fetch_valid[0]
                                                            <= bkend_mem_data[DATA_WIDTH];

                                            STAB_STACK      <= STAB_STACK + 1 - 1;
                                        end
                                        else begin
                                            pf_fetch_data[1]
                                                            <= bkend_mem_data[0 +: DATA_WIDTH];
                                            pf_fetch_valid[1]
                                                            <= bkend_mem_data[DATA_WIDTH];

                                            STAB_STACK      <= STAB_STACK + 1;
                                        end
                                    end
                                    else
                                        if(m_ready == 1'b1)
                                            STAB_STACK      <= STAB_STACK - 1;
                                2:  if(m_ready == 1'b1) begin
                                        pf_fetch_data[0]    <= pf_fetch_data[1];
                                        pf_fetch_valid[0]   <= pf_fetch_valid[1];

                                        STAB_STACK          <= STAB_STACK - 1;
                                    end
                            endcase
                    end
                end

                assign m_data                               = STAB_STACK > 0 ? pf_fetch_data[0] : {DATA_WIDTH{1'b0}};
                assign m_valid                              = STAB_STACK > 0 ? pf_fetch_valid[0] : 1'b0;
            end
            else begin
                initial
                begin
                    pf_fetch_data[0]                        = {DATA_WIDTH{1'b0}};
                    pf_fetch_data[1]                        = {DATA_WIDTH{1'b0}};
                end

                always @(posedge clk or negedge aresetn)
                begin
                    if(aresetn == 1'b0) begin
                        pf_fetch_data[0]                    <= {DATA_WIDTH{1'b0}};
                        pf_fetch_data[1]                    <= {DATA_WIDTH{1'b0}};
                    end
                    else
                        case(STAB_STACK)
                            0:  if(FETCH_STATE == FETCH_STATE_PIPE) begin
                                    pf_fetch_data[0]        <= bkend_mem_data;

                                    STAB_STACK              <= STAB_STACK + 1;
                                end
                            1:  if(FETCH_STATE == FETCH_STATE_PIPE) begin
                                    if(m_ready == 1'b1) begin
                                        pf_fetch_data[0]    <= bkend_mem_data;

                                        STAB_STACK          <= STAB_STACK + 1 - 1;
                                    end
                                    else begin
                                        pf_fetch_data[1]    <= bkend_mem_data;

                                        STAB_STACK          <= STAB_STACK + 1;
                                    end
                                end
                                else
                                    if(m_ready == 1'b1)
                                        STAB_STACK          <= STAB_STACK - 1;
                            2:  if(m_ready == 1'b1) begin
                                    pf_fetch_data[0]        <= pf_fetch_data[1];

                                    STAB_STACK              <= STAB_STACK - 1;
                                end
                        endcase
                end

                assign m_data                               = STAB_STACK > 0 ? pf_fetch_data[0] : {DATA_WIDTH{1'b0}};
                assign m_valid                              = STAB_STACK > 0 ? 1'b1 : 1'b0;
            end
        end
        else begin
            reg                                             fetch_valid_d = 0;

            initial
            begin
                FETCH_STATE                                 = FETCH_STATE_RESET;

                FETCH_COUNTER                               = 0;
            end

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0)
                    FETCH_STATE                             <= FETCH_STATE_RESET;
                else
                    case(FETCH_STATE)
                        FETCH_STATE_RESET:
                        begin
                            FETCH_STATE                     <= FETCH_STATE_IDLE;

                            FETCH_COUNTER                   <= 0;
                        end
                        FETCH_STATE_IDLE:
                        begin
                            FETCH_STATE                     <= FETCH_STATE_PIPE;

                            FETCH_COUNTER                   <= FETCH_COUNTER + 1;
                        end
                        FETCH_STATE_PIPE:
                        begin
                            if(fetch_valid == 1'b0)
                                FETCH_STATE                 <= FETCH_STATE_DONE;

                            FETCH_COUNTER                   <= FETCH_COUNTER + 1;
                        end
                    endcase

                fetch_valid_d                               <= fetch_valid;
            end

            if(REPEAT_ENABLE > 0)
                assign fetch_valid                          = 1'b1;
            else
                assign fetch_valid                          = FETCH_COUNTER < DATA_DEPTH;

            if(USE_VALID_BIT > 0) begin
                assign m_data                               = fetch_valid_d == 1'b1 ? bkend_mem_data[0 +: DATA_WIDTH]
                                                                : {DATA_WIDTH{1'b0}};
                assign m_valid                              = fetch_valid_d == 1'b1 ? bkend_mem_data[DATA_WIDTH] : 1'b0;
            end
            else begin
                assign m_data                               = fetch_valid_d == 1'b1 ? bkend_mem_data : {DATA_WIDTH{1'b0}};
                assign m_valid                              = fetch_valid_d;
            end
        end
    endgenerate

    // ENABLE AND WRITE ENABLE PINS OF PORT B TIED TO LITERAL TO RESOLVE RULE VIOLATION (REQP-197)
    //  THAT COMPLAINS ON WRITE ENABLE PINS MUST BE CONNECTED.
    //
    generic_bram #(
        .USE_VALID_BIT                                      (USE_VALID_BIT),
        .MEM_ADDR_WIDTH                                     (GetWidth(DATA_DEPTH - 1)),
        .MEM_DATA_WIDTH                                     (DATA_WIDTH),
        .MEM_INITIALISE                                     (1),
        .MEM_CONF_FILE                                      (PROG_OUTPUT_FILE)
    ) bkend_mem (
        .clka                                               (clk),
        .porta_addr                                         (FETCH_COUNTER[GetWidth(DATA_DEPTH - 1) - 1 : 0]),
        .porta_en                                           (1'b1),
        .porta_wr_en                                        (1'b0),
        .porta_data_in                                      (),
        .porta_data_out                                     (bkend_mem_data),
        .portb_en                                           (1'b0),
        .portb_wr_en                                        (1'b0)
    );
endmodule