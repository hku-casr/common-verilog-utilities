`timescale 1ns/1ps

/**
 *
 * generic_axis_sband_fifo_tb.v
 *
 * Short Description:
 *
 * Long Description:
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

`define ALL_ONESHOT

module generic_axis_sband_fifo_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    parameter CLK_PERIOD                                    = 10;
    parameter MSB_ALIGN                                     = 1;

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter AXIS_TDATA_WIDTH                              = 8;
    parameter AXIS_TDEST_WIDTH                              = 8;

    parameter DATA_BUFFER_DEPTH                             = 1024;
    parameter TRANS_BUFFER_DEPTH                            = 10;

    // PARAMETERS FOR VERIFICATION FILE
    //
    parameter INPUT_TRANS                                   = "generic_axis_sband_fifo_input.csv";
    parameter OUTPUT_TRANS                                  = "generic_axis_sband_fifo_output.csv";

    reg                                                     clk;
    reg                                                     aresetn;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     in_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     in_axis_tkeep;
    wire                                                    in_axis_tlast;
    wire [AXIS_TDEST_WIDTH     - 1 : 0]                     in_axis_tdest;
    wire                                                    in_axis_tready;
    wire                                                    in_axis_tvalid;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     out_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     out_axis_tkeep;
    wire                                                    out_axis_tlast;
    wire [AXIS_TDEST_WIDTH     - 1 : 0]                     out_axis_tdest;
    reg                                                     out_axis_tready;
    wire                                                    out_axis_tvalid;
    wire [`INTEGER_WIDTH   * 8 - 1 : 0]                     axis_trans_count;
    wire [`INTEGER_WIDTH   * 8 - 1 : 0]                     axis_data_count;

    integer                                                 schedule_j;

    integer                                                 param_start_value[100 - 1 : 0];
    integer                                                 param_byte_count[100 - 1 : 0];
    integer                                                 param_align_offset[100 - 1 : 0];

    integer                                                 inputf, outputf;
    integer                                                 inputf_row, outputf_row;

// BEGIN

`include "axis_data_gen.vh"

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    // CYCLE-BASED STATIC SCHEDULE
    // ----------------------------------------
    //
    initial
    begin
        schedule_j                                          = 0;

        out_axis_tready                                     = 1'b0;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            15: out_axis_tready                             <= 1'b1;
            21: out_axis_tready                             <= 1'b0;
            24: out_axis_tready                             <= 1'b1;
        endcase
    end

`ifndef ALL_ONESHOT
    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // TODO: DESCRIBE TRANSACTIONS
    // 1. TRANSACTION 1, READY = ...
    //
    initial
    begin
        param_start_value[0]                                = 64;
        param_byte_count[0]                                 = 2 * AXIS_TDATA_WIDTH;
        param_align_offset[0]                               = 3;

        param_start_value[1]                                = 30;
        param_byte_count[1]                                 = 2 * AXIS_TDATA_WIDTH;
        param_align_offset[1]                               = 0;

        param_start_value[2]                                = 85;
        param_byte_count[2]                                 = 4 * AXIS_TDATA_WIDTH;
        param_align_offset[2]                               = 5;

        param_start_value[3]                                = 12;
        param_byte_count[3]                                 = 3 * AXIS_TDATA_WIDTH;
        param_align_offset[3]                               = 0;

        param_start_value[4]                                = 55;
        param_byte_count[4]                                 = 2 * AXIS_TDATA_WIDTH;
        param_align_offset[4]                               = 0;

        param_start_value[5]                                = 70;
        param_byte_count[5]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[5]                               = 7;

        param_start_value[6]                                = 26;
        param_byte_count[6]                                 = 2 * AXIS_TDATA_WIDTH;
        param_align_offset[6]                               = 0;

        param_start_value[7]                                = 89;
        param_byte_count[7]                                 = 3 * AXIS_TDATA_WIDTH;
        param_align_offset[7]                               = 4;

        param_start_value[8]                                = 52;
        param_byte_count[8]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[8]                               = 0;

        param_start_value[9]                                = 0;
        param_byte_count[9]                                 = 4 * AXIS_TDATA_WIDTH;
        param_align_offset[9]                               = 2;

        param_start_value[10]                               = 37;
        param_byte_count[10]                                = 4 * AXIS_TDATA_WIDTH;
        param_align_offset[10]                              = 0;

        param_start_value[11]                               = 48;
        param_byte_count[11]                                = 3 * AXIS_TDATA_WIDTH;
        param_align_offset[11]                              = 1;
    end
`else
    // AXI STREAM HANDSHAKE-BASED STATIC SCHEDULE
    // ----------------------------------------
    // TODO: DESCRIBE TRANSACTIONS
    // 1. TRANSACTION 1, READY = ...
    //
    initial
    begin
        param_start_value[0]                                = 64;
        param_byte_count[0]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[0]                               = 0;

        param_start_value[1]                                = 30;
        param_byte_count[1]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[1]                               = 0;

        param_start_value[2]                                = 85;
        param_byte_count[2]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[2]                               = 0;

        param_start_value[3]                                = 12;
        param_byte_count[3]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[3]                               = 0;

        param_start_value[4]                                = 55;
        param_byte_count[4]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[4]                               = 0;

        param_start_value[5]                                = 70;
        param_byte_count[5]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[5]                               = 0;

        param_start_value[6]                                = 26;
        param_byte_count[6]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[6]                               = 0;

        param_start_value[7]                                = 89;
        param_byte_count[7]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[7]                               = 0;

        param_start_value[8]                                = 52;
        param_byte_count[8]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[8]                               = 0;

        param_start_value[9]                                = 0;
        param_byte_count[9]                                 = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[9]                               = 0;

        param_start_value[10]                               = 37;
        param_byte_count[10]                                = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[10]                              = 0;

        param_start_value[11]                               = 48;
        param_byte_count[11]                                = 1 * AXIS_TDATA_WIDTH;
        param_align_offset[11]                              = 0;
    end
`endif

    always @(posedge clk)
    begin
        if(aresetn == 1'b0)
            `bus_null(0)
        else begin
            `bus_adv(0)

            case(`bus_step(0))
                0:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                1:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                2:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                3:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                4:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                5:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                6:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                8:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                9:  `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                10: `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                11: `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                12: `bus_out(0, param_start_value[`trans_step(0)], param_byte_count[`trans_step(0)], param_align_offset[`trans_step(0)])
                default: `bus_null(0)
            endcase
        end
    end

    assign in_axis_tdest                                    = schedule_j % 7 << 2 | schedule_j % 3;

    initial
    begin
        inputf = $fopen(INPUT_TRANS, "w");

        $fwrite(inputf, "STEP | AXIS TDATA | AXIS TKEEP | AXIS TLAST\n");

        inputf_row                                          = 0;
    end

    always @(posedge clk)
        if(in_axis_tvalid == 1'b1 && in_axis_tready == 1'b1) begin
            inputf_row                                      <= inputf_row + 1;

            $fwrite(inputf, "%03d %08x %08b %0b %08b\n", inputf_row, in_axis_tdata,
                in_axis_tkeep, in_axis_tlast, in_axis_tdest);
        end

    initial
    begin
        outputf = $fopen(OUTPUT_TRANS, "w");

        $fwrite(outputf, "STEP | AXIS TDATA | AXIS TKEEP | AXIS TLAST\n");

        outputf_row                                         = 0;
    end

    always @(posedge clk)
        if(out_axis_tvalid == 1'b1 && out_axis_tready == 1'b1) begin
            outputf_row                                     <= outputf_row + 1;

            $fwrite(outputf, "%03d %08x %08b %0b %08b\n", outputf_row, out_axis_tdata,
                out_axis_tkeep, out_axis_tlast, out_axis_tdest);
        end

    generic_axis_sband_fifo #(
        .AXIS_TSBAND_WIDTH                                  (AXIS_TDEST_WIDTH),
        .DATA_BUFFER_DEPTH                                  (TRANS_BUFFER_DEPTH)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_axis_tsideband                                   (in_axis_tdest),
        .m_axis_tsideband                                   (out_axis_tdest),
        .tap_s_axis_tlast                                   (in_axis_tlast),
        .tap_s_axis_tready                                  (in_axis_tready),
        .tap_s_axis_tvalid                                  (in_axis_tvalid),
        .tap_m_axis_tlast                                   (out_axis_tlast),
        .tap_m_axis_tready                                  (out_axis_tready),
        .tap_m_axis_tvalid                                  (out_axis_tvalid),
        .axis_trans_count                                   (axis_trans_count)
    );

    generic_axis_fifo #(
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH),
        .DATA_BUFFER_DEPTH                                  (DATA_BUFFER_DEPTH)
    ) UUT_ASSIST (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_axis_tdata                                       (in_axis_tdata),
        .s_axis_tkeep                                       (in_axis_tkeep),
        .s_axis_tlast                                       (in_axis_tlast),
        .s_axis_tready                                      (in_axis_tready),
        .s_axis_tvalid                                      (in_axis_tvalid),
        .m_axis_tdata                                       (out_axis_tdata),
        .m_axis_tkeep                                       (out_axis_tkeep),
        .m_axis_tlast                                       (out_axis_tlast),
        .m_axis_tready                                      (out_axis_tready),
        .m_axis_tvalid                                      (out_axis_tvalid),
        .axis_data_count                                    (axis_data_count)
    );

    axis_data_gen #(
        .MSB_ALIGN                                          (MSB_ALIGN),
        .CLK_ALIGN                                          (1),
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH)
    ) SIG_GEN (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .cmd_bus                                            (`cmd_bus(0)),
        .axis_tdata                                         (in_axis_tdata),
        .axis_tkeep                                         (in_axis_tkeep),
        .axis_tlast                                         (in_axis_tlast),
        .axis_tready                                        (in_axis_tready),
        .axis_tvalid                                        (in_axis_tvalid)
    );
endmodule