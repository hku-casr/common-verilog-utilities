`timescale 1ns/1ps

/**
 *
 * generic_virtual_output_tb.v
 *
 * Short Description: 
 *
 * Long Description: 
 *
 * Target Flows: Simulation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

module generic_virtual_output_tb(
    );

    // PARAMETERS FOR THE TESTBENCH
    //
    parameter CLK_PERIOD                                    = 10;

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter DATA_WIDTH                                    = 8;
    parameter DATA_DEPTH                                    = 256;
    parameter PROTO_HDSHAKE                                 = 1;
    parameter PROTO_HDSHAKE_INVAL                           = 0;
    parameter PROG_OUTPUT_FILE                              = "prog_file-counter.hex";

    reg                                                     clk;
    reg                                                     aresetn;
    wire [DATA_WIDTH - 1 : 0]                               in_data;
    reg                                                     in_ready;
    wire                                                    in_valid;

    integer                                                 schedule_j;

// BEGIN

    initial
    begin
        clk                                                 = 1'b0;
        aresetn                                             = 1'b0;

        forever #(CLK_PERIOD / 2) clk                       = ~clk;
    end

    initial
    begin
        schedule_j                                          = 0;

        in_ready                                            = 1'b1;
    end

    always @(posedge clk)
    begin
        schedule_j                                          <= schedule_j + 1;

        case(schedule_j)
            3:  aresetn                                     <= 1'b1;
            12: in_ready                                    <= 1'b0;
            13: in_ready                                    <= 1'b1;
            21: in_ready                                    <= 1'b0;
            22: in_ready                                    <= 1'b1;
            33: in_ready                                    <= 1'b0;
            34: in_ready                                    <= 1'b1;
            39: in_ready                                    <= 1'b0;
            41: in_ready                                    <= 1'b1;
            46: in_ready                                    <= 1'b0;
            48: in_ready                                    <= 1'b1;
        endcase
    end

    generic_virtual_output #(
        .DATA_WIDTH                                         (DATA_WIDTH),
        .DATA_DEPTH                                         (DATA_DEPTH),
        .PROTO_HDSHAKE                                      (PROTO_HDSHAKE),
        .PROTO_HDSHAKE_INVAL                                (PROTO_HDSHAKE_INVAL),
        .PROG_OUTPUT_FILE                                   (PROG_OUTPUT_FILE)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .m_data                                             (in_data),
        .m_ready                                            (in_ready),
        .m_valid                                            (in_valid)
    );
endmodule