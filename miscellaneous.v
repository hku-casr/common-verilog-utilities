`timescale 1ns/1ps

/**
 *
 * miscellaneous.v
 *
 * Short Description: This source contains a collection of common miscellanesous
 *  Verilog modules.
 *
 * Long Description: This source contains a collection of common miscellanesous
 *  Verilog modules.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

/**
 *
 * GENERIC_TIMER
 *
 * GENERIC PARAMETERS
 * --------------------
 * MODE_WATCHDOG       BOOLEAN VARIABLE TO CONFIGURE THE MODULE TO BEHAVE AS A
 *                      WATCHDOG TIMER, WHICH A PULSE IS PERIODICALLY ASSERTED
 *                      IF THE TIMER IS NOT RESET BEFORE THRESHOLD
 * DEFAULT_LIMIT       THE DEFAULT TIMER VALUE WHEN A PROPER TIMER VALUE IS NOT
 *                      PRESENTED ON THE S_LIMIT BUS AT THE TRIGGERING OF TIMER
 * TIMER_MAGLITUDE     THE MAGLITUDE OF THE TIMER AS A MULTIPLE OF 32-BIT. A
 *                      LIMIT IS ONLY SET ON THE HIGHER 32-BIT OF THE INTERNAL
 *                      COUNTER, MAKING THE LOWER ORDER BITS TRANSPARENT, I.E.,
 *                      PROLONGING A 1-BIT TIMESPAN ON THE S_LIMIT PERSPECTIVE
 * PULSE_HDSHAKE       BOOLEAN VARIABLE FOR DICTATING THE OUTPUT M_PULSE PIN
 *                      REQUIRES 2-WAY HANDSHAKE TO DEASSERT
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_arm               IN    ASSERT TO ARM TIMER, REPEATED ASSERTION OVERRIDES
 *                            THE ORIGINAL TIMER SESSION
 * s_disarm            IN    ASSERT TO DISARM TIMER, IN WATCHDOG MODE, ASSERT TO
 *                            SURPRESS THE PULSE OF THE CURRENT TIMER SESSION
 *                            AND RESTARTS THE TIMER
 * s_limit             IN    A NON-ZERO 32-BIT TIMER VALUE, ELSE THE DEFAULT
 *                            TIMER VALUE WILL BE TAKEN, ONLY RESPECTED WHEN ARM
 *                            PIN IS ASSERTED
 * m_pulse*            VAR   ASSERTED WHEN TIMER SESSION EXPIRED, DEASSERT UPON
 *                            ACKNOWLEDGEMENT IF HANDSHAKE MODE IS ENABLE
 * m_assert            OUT   ASSERTED WHEN TIMER IS IN SESSION
 * timer_cycle         OUT   THE HIGHER 32-BIT OF THE INTERNAL TIMER VALUE
 *
 */
module generic_timer #(
        parameter MODE_WATCHDOG                             = 0, // ONESHOT / PERIODIC
        parameter [`INTEGER_WIDTH * 8 - 1 : 0] DEFAULT_LIMIT
                                                            = 1024,
        parameter TIMER_MAGLITUDE                           = 1,
        parameter PULSE_HDSHAKE                             = 0
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire                                         s_arm,
        input  wire                                         s_disarm,
        input  wire [`INTEGER_WIDTH * 8 - 1 : 0]            s_limit,
        input  wire                                         m_pulse_ack,
        output wire                                         m_pulse,
        output wire                                         m_assert,
        output wire [`INTEGER_WIDTH * 8 - 1 : 0]            timer_cycle
    );

    reg  [0 : 0]                                            STATE;
    localparam STATE_IDLE                                   = 1'b0;
    localparam STATE_ARM                                    = 1'b1;

    reg  [TIMER_MAGLITUDE * `INTEGER_WIDTH * 8 - 1 : 0]     COUNTER;
    reg  [`INTEGER_WIDTH * 8 - 1 : 0]                       REQ_TIMER;

    wire                                                    relay_pulse_ack;

// BEGIN

    generate
        if(MODE_WATCHDOG > 0) begin
            initial
                COUNTER                                     = (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0) begin
                    STATE                                   <= STATE_IDLE;

                    COUNTER                                 <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                end
                else
                    case(STATE)
                        STATE_IDLE:
                            if(s_arm == 1'b1) begin
                                STATE                       <= STATE_ARM;

                                if(s_limit == {`INTEGER_WIDTH{8'h0}}) begin
                                    REQ_TIMER               <= DEFAULT_LIMIT;
                                    COUNTER                 <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                                else begin
                                    REQ_TIMER               <= s_limit;
                                    COUNTER                 <= (s_limit << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                            end
                        STATE_ARM:
                            if(s_arm == 1'b1)
                                if(s_limit == {`INTEGER_WIDTH{8'h0}}) begin
                                    REQ_TIMER               <= DEFAULT_LIMIT;
                                    COUNTER                 <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                                else begin
                                    REQ_TIMER               <= s_limit;
                                    COUNTER                 <= (s_limit << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                            else if(s_disarm == 1'b1)
                                COUNTER                     <= DEFAULT_LIMIT;
                            else
                                if(COUNTER > 0)
                                    COUNTER                 <= COUNTER - 1;
                                else
                                    if(relay_pulse_ack == 1'b1)
                                        if(s_arm == 1'b1)
                                            if(s_limit == {`INTEGER_WIDTH{8'h0}}) begin
                                                REQ_TIMER   <= DEFAULT_LIMIT;
                                                COUNTER     <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                            end
                                            else begin
                                                REQ_TIMER   <= s_limit;
                                                COUNTER     <= (s_limit << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                            end
                                        else begin
                                            REQ_TIMER       <= DEFAULT_LIMIT;
                                            COUNTER         <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                        end
                    endcase
            end
        end
        else begin
            initial
                COUNTER                                     = (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0) begin
                    STATE                                   <= STATE_IDLE;

                    COUNTER                                 <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                end
                else
                    case(STATE)
                        STATE_IDLE:
                            if(s_arm == 1'b1) begin
                                STATE                       <= STATE_ARM;

                                if(s_limit == {`INTEGER_WIDTH{8'h0}}) begin
                                    REQ_TIMER               <= DEFAULT_LIMIT;
                                    COUNTER                 <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                                else begin
                                    REQ_TIMER               <= s_limit;
                                    COUNTER                 <= (s_limit << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                            end
                        STATE_ARM:
                            if(s_arm == 1'b1)
                                if(s_limit == {`INTEGER_WIDTH{8'h0}}) begin
                                    REQ_TIMER               <= DEFAULT_LIMIT;
                                    COUNTER                 <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                                else begin
                                    REQ_TIMER               <= s_limit;
                                    COUNTER                 <= (s_limit << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                end
                            else if(s_disarm == 1'b1)
                                STATE                       <= STATE_IDLE;
                            else
                                if(COUNTER > 0)
                                    COUNTER                 <= COUNTER - 1;
                                else
                                    if(relay_pulse_ack == 1'b1)
                                        if(s_arm == 1'b1) begin
                                            STATE           <= STATE_ARM;

                                            if(s_limit == {`INTEGER_WIDTH{8'h0}}) begin
                                                REQ_TIMER   <= DEFAULT_LIMIT;
                                                COUNTER     <= (DEFAULT_LIMIT << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                            end
                                            else begin
                                                REQ_TIMER   <= s_limit;
                                                COUNTER     <= (s_limit << ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8))
                                                                | {TIMER_MAGLITUDE * `INTEGER_WIDTH{8'h0}};
                                            end
                                        end
                                        else
                                            STATE           <= STATE_IDLE;
                    endcase
            end
        end
    endgenerate

    generate
        if(PULSE_HDSHAKE > 0)
            assign relay_pulse_ack                          = m_pulse_ack;
        else
            assign relay_pulse_ack                          = 1'b1;
    endgenerate

    assign m_pulse                                          = STATE != STATE_IDLE ? (COUNTER == 0) : 1'b0;
    assign m_assert                                         = STATE != STATE_IDLE ? (COUNTER > 0) : 1'b0;
    assign timer_cycle                                      = STATE != STATE_IDLE ? ({REQ_TIMER, {(TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH{8'h0}}}
                                                                - COUNTER) >> ((TIMER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8) : {`INTEGER_WIDTH{8'h00}};
endmodule

/**
 *
 * GENERIC_COUNTER
 *
 * GENERIC PARAMETERS
 * --------------------
 * COUNTER_MAGLITUDE   THE MAGLITUDE OF THE COUNTER AS A MULTIPLE OF 32-BIT
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_incre             IN    ASSERT TO INCREMENT COUNTER
 * s_reset             IN    ASSERT TO RESET COUNTER
 * counter_val         OUT   THE HIGHER 32-BIT OF THE COUNTER
 *
 */
module generic_counter #(
        parameter COUNTER_MAGLITUDE                         = 1
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire                                         s_incre,
        input  wire                                         s_reset,
        output wire [`INTEGER_WIDTH * 8 - 1 : 0]            counter_val
    );

    reg  [COUNTER_MAGLITUDE * `INTEGER_WIDTH * 8 - 1 : 0]   COUNTER;

// BEGIN

    initial
        COUNTER                                             = 0;

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            COUNTER                                         <= 0;
        else
            if(s_reset == 1'b1)
                COUNTER                                     <= 0;
            else if(s_incre == 1'b1)
                COUNTER                                     <= COUNTER + 1;
    end

    assign counter_val                                      = COUNTER >> ((COUNTER_MAGLITUDE - 1) * `INTEGER_WIDTH * 8);
endmodule

/**
 *
 * GENERIC_SR
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_set               IN    ASSERT TO SET
 * s_reset             IN    ASSERT TO RESET
 * m_assert            OUT   ASSERTED WHEN FLIP-FLOP IS SET
 *
 */
module generic_SR(
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire                                         s_set,
        input  wire                                         s_reset,
        output wire                                         m_assert
    );

    reg  [0 : 0]                                            STATE;
    localparam STATE_IDLE                                   = 1'b0;
    localparam STATE_ASSERT                                 = 1'b1;

// BEGIN

    initial
        STATE                                               = STATE_IDLE;

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            STATE                                           <= STATE_IDLE;
        else
            if(s_set == 1'b1)
                STATE                                       <= STATE_ASSERT;
            else if(s_reset == 1'b1)
                STATE                                       <= STATE_IDLE;
    end

    assign m_assert                                         = STATE == STATE_ASSERT;
endmodule

/**
 *
 * GENERIC_T
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_set               IN    ASSERT TO TOGGLE
 * m_assert            OUT   ASSERT WHEN FLIP-FLOP IS AT SET
 *
 */
module generic_T(
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire                                         s_set,
        output wire                                         m_assert
    );

    reg  [0 : 0]                                            STATE;
    localparam STATE_IDLE                                   = 1'b0;
    localparam STATE_ASSERT                                 = 1'b1;

// BEGIN

    initial
        STATE                                               = STATE_IDLE;

    always @(posedge clk or negedge aresetn)
    begin
        if(aresetn == 1'b0)
            STATE                                           <= STATE_IDLE;
        else
            case(STATE)
                STATE_IDLE:
                    if(s_set == 1'b1)
                        STATE                               <= STATE_ASSERT;
                STATE_ASSERT:
                    if(s_set == 1'b1)
                        STATE                               <= STATE_IDLE;
            endcase
    end

    assign m_assert                                         = STATE == STATE_ASSERT;
endmodule