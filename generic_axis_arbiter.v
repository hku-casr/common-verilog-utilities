`timescale 1ns/1ps

/**
 *
 * generic_axis_arbiter.v
 *
 * Short Description: This module arbitrates AXI Stream transactions originated
 *  from multiple input sources and multiplex such transactions into a single
 *  output channel.
 *
 * Long Description: This module arbitrates AXI Stream transactions originated
 *  from multiple input sources and multiplex such transactions into a single
 *  output channel. Round-robin policy and priority-based policy are selectable
 *  currently implemented arbitration policies.
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

/**
 *
 * GENERIC_AXIS_ARBITER
 *
 * GENERIC PARAMETERS
 * --------------------
 * NUM_WAY             THE NUMBER OF DRIVING INPUTS THE MODULE HAS TO ARBITRATE
 * ARB_HEURISTICS      SELECTOR OF ARBITRATION HEURISTICS, DEFAULT IS 0, ROUND-
 *                      ROBIN HEURISTICS. ARB_HEURISTICS OF 1 REFERS TO PRIORITY
 *                      SELECTION, WITH THE LOWEST INDEXED CHANNEL HAVING HIGHER
 *                      PRIORITY TO LARGER INDEXED CHANNELS AND TAKES PRECEDENCE
 *                      TO TRANSMIT WHENEVER CHANNEL IS AVAILABLE
 * AXIS_TDEST_EXT      BOOLEAN VARIABLE TO DETERMINE IF THE DEST SIDEBAND CHAN-
 *                      NEL AT INPUT INTERFACE IS HONOURED BY THE MODULE AND
 *                      PASSED TO OUTPUT DEST SIDEBAND CHANNEL FOR AN ACCEPTED
 *                      TRANSACTION
 * AXIS_TSBAND_ENABLE  BOOLEAN VARIABLE TO ENABLE THE MODULE TO PROCESS CUSTOM
 *                      AXI-STREAM TRANSACTION-BASED SIDEBAND CHANNEL, TRANS-
 *                      ACTION-BASED SIDEBAND CHANNEL TRANSFERS VALUE THAT KEEPS
 *                      CONSTANT IN A TRANSACTION SPAN
 * AXIS_TWSBAND_ENABLE BOOLEAN VARIABLE TO ENABLE THE MODULE TO PROCESS CUSTOM
 *                      AXI-STREAM WORD-BASED SIDEBAND CHANNEL, WORD-BASED SIDE-
 *                      BAND CHANNEL RUNS VARYING DATA THAT ALIGNS TO INDIVIDUAL
 *                      AXI-STREAM WORDS ACROSS A TRANSACTION SPAN
 * AXIS_TDATA_WIDTH    THE NUMBER OF BYTES IN THE AXI-STREAM DATA CHANNEL
 * AXIS_TDEST_WIDTH    THE NUMBER OF BITS IN THE AXI-STREAM DEST SIDEBAND CHAN-
 *                      NEL
 * AXIS_TSBAND_WIDTH   THE NUMBER OF BITS IN THE CORRESPONDING TRANSACTION-BASED
 *                      SIDEBAND CHANNEL
 * AXIS_TWSBAND_WIDTH  THE NUMBER OF BITS IN THE CORRESPONDING WORD-BASED SIDE-
 *                      BAND CHANNEL
 *
 * INTERFACE PINS
 * --------------------
 * clk                 IN    DRIVING CLOCK
 * aresetn             IN    ACTIVE-LOW ASYNCHRONOUS RESET
 * s_axis_*            VAR   CONCATENATED AXI-STREAM INPUT INTERFACE ACCEPTING
 *                            DRIVING STREAMS, DE-MULTIPLEXED INTERNALLY AS
 *                            DIFFERENT CHANNELS BY THEIR CONNECTION TO THE
 *                            RESPECTIVE MULTIPLE OF AXIS_T*_WIDTH PINS
 * s_axis_tsband       IN    CONCATENATED CUSTOM AXI-STREAM TRANSACTION-BASED
 *                            SIDEBAND CHANNEL, TRANSACTION-BASED SIDEBAND
 *                            CHANNEL TRANSFERS VALUES THAT REMAIN CONSTANT IN
 *                            THE WHOLE TRANSACTION SPAN
 * s_axis_twsband      IN    CONCATENATED CUSTOM AXI-STREAM WORD-BASED SIDEBAND
 *                            CHANNEL, WORD-BASED SIDEBAND CHANNEL RUNS VARYING
 *                            DATA THAT ALIGNS TO AXI-STREAM WORDS THROUGHOUT
 *                            THE TRANSACTION SPAN
 * m_axis_*            VAR   AXI-STREAM INTERFACE OUTPUT OF ACCEPTED TRANS-
 *                            ACTION ARBITRATED FROM DRIVING CHANNELS
 * m_axis_tsband       OUT   CUSTOM AXI-STREAM TRANSACTION-BASED SIDEBAND
 *                             CHANNEL OUTPUT OF ACCEPTED TRANSACTION ARBITRATED
 * m_axis_twsband      OUT   CUSTOM AXI-STREAM WORD-BASED SIDEBAND CHANNEL OUT-
 *                            PUT OF ACCEPTED TRANSACTION ARBITRATED
 *
 */
module generic_axis_arbiter #(
        parameter NUM_WAY                                   = 2,
        parameter ARB_HEURISTICS                            = 0,
        parameter AXIS_TDEST_EXT                            = 0,
        parameter AXIS_TSBAND_ENABLE                        = 0,
        parameter AXIS_TWSBAND_ENABLE                       = 0,
        parameter AXIS_TDATA_WIDTH                          = 8,
        parameter AXIS_TDEST_WIDTH                          = 8,
        parameter AXIS_TSBAND_WIDTH                         = 64,
        parameter AXIS_TWSBAND_WIDTH                        = 64
    )
    (
        input  wire                                         clk,
        input  wire                                         aresetn,
        input  wire [NUM_WAY * AXIS_TDATA_WIDTH * 8 - 1 : 0]
                                                            s_axis_tdata,
        input  wire [NUM_WAY * AXIS_TDATA_WIDTH     - 1 : 0]
                                                            s_axis_tkeep,
        input  wire [NUM_WAY - 1 : 0]                       s_axis_tlast,
        input  wire [NUM_WAY * AXIS_TDEST_WIDTH     - 1 : 0]
                                                            s_axis_tdest,
        input  wire [NUM_WAY * AXIS_TSBAND_WIDTH    - 1 : 0]
                                                            s_axis_tsband,
        input  wire [NUM_WAY * AXIS_TWSBAND_WIDTH   - 1 : 0]
                                                            s_axis_twsband,
        output wire [NUM_WAY - 1 : 0]                       s_axis_tready,
        input  wire [NUM_WAY - 1 : 0]                       s_axis_tvalid,
        output wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]          m_axis_tdata,
        output wire [AXIS_TDATA_WIDTH     - 1 : 0]          m_axis_tkeep,
        output wire                                         m_axis_tlast,
        output wire [AXIS_TDEST_WIDTH     - 1 : 0]          m_axis_tdest,
        output wire [AXIS_TSBAND_WIDTH    - 1 : 0]          m_axis_tsband,
        output wire [AXIS_TWSBAND_WIDTH   - 1 : 0]          m_axis_twsband,
        input  wire                                         m_axis_tready,
        output wire                                         m_axis_tvalid
    );

    localparam ARB_ROUNDROBIN                               = 0;
    localparam ARB_PRIORITY                                 = 1;

    function integer GetWidth;
        input integer value;

        begin
            for(GetWidth = 0; value > 0; GetWidth = GetWidth + 1)
                value = value >> 1;
        end
    endfunction

    (* keep = "yes" *)
    reg  [2 : 0]                                            STATE[NUM_WAY - 1 : 0];
    localparam STATE_IDLE                                   = 3'b000;
    localparam STATE_WAIT                                   = 3'b001;
    localparam STATE_ONESHOT                                = 3'b010;
    localparam STATE_PASSTHROUGH                            = 3'b011;
    localparam STATE_LAST                                   = 3'b100;
    localparam STATE_RESET                                  = 3'b111;

    reg  [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     stage_in_axis_tdata[NUM_WAY - 1 : 0];
    reg  [AXIS_TDATA_WIDTH     - 1 : 0]                     stage_in_axis_tkeep[NUM_WAY - 1 : 0];
    reg  [NUM_WAY - 1 : 0]                                  stage_in_axis_tlast;
    reg  [AXIS_TDEST_WIDTH     - 1 : 0]                     stage_in_axis_tdest[NUM_WAY - 1 : 0];
    reg  [AXIS_TSBAND_WIDTH   - 1 : 0]                      stage_in_axis_tsband[NUM_WAY - 1 : 0];
    reg  [AXIS_TWSBAND_WIDTH  - 1 : 0]                      stage_in_axis_twsband[NUM_WAY - 1 : 0];
    reg  [NUM_WAY - 1 : 0]                                  stage_in_axis_tvalid;
    wire [NUM_WAY - 1 : 0]                                  relay_in_axis_tready;

    // FIRST_READY
    //
    (* keep = "yes" *)
    reg  [GetWidth(NUM_WAY - 1) - 1 : 0]                    grant_axis_src;
    (* keep = "yes" *)
    reg  [GetWidth(NUM_WAY - 1) - 1 : 0]                    stage_grant_axis_src;
    wire                                                    arb_valid;
    wire                                                    arb_ready[NUM_WAY - 1 : 0];

// BEGIN

    generate
        genvar stage_i;

        for(stage_i = 0; stage_i < NUM_WAY; stage_i = stage_i + 1) begin
            initial
            begin
                stage_in_axis_tdata[stage_i]                <= {AXIS_TDATA_WIDTH{8'h0}};
                stage_in_axis_tkeep[stage_i]                <= {AXIS_TDATA_WIDTH{1'b0}};
                stage_in_axis_tlast[stage_i]                <= 1'b0;
                stage_in_axis_tvalid[stage_i]               <= 1'b0;
            end

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0) begin
                    stage_in_axis_tdata[stage_i]            <= {AXIS_TDATA_WIDTH{8'h0}};
                    stage_in_axis_tkeep[stage_i]            <= {AXIS_TDATA_WIDTH{1'b0}};
                    stage_in_axis_tlast[stage_i]            <= 1'b0;
                    stage_in_axis_tvalid[stage_i]           <= 1'b0;
                end
                else
                    if(relay_in_axis_tready[stage_i] == 1'b1) begin
                        stage_in_axis_tdata[stage_i]        <= s_axis_tdata[stage_i * AXIS_TDATA_WIDTH * 8 +: AXIS_TDATA_WIDTH * 8];
                        stage_in_axis_tkeep[stage_i]        <= s_axis_tkeep[stage_i * AXIS_TDATA_WIDTH     +: AXIS_TDATA_WIDTH];
                        stage_in_axis_tlast[stage_i]        <= s_axis_tlast[stage_i];
                        stage_in_axis_tvalid[stage_i]       <= s_axis_tvalid[stage_i];
                    end
            end

            // STAGING OF CUSTOM WORD-BASED SIDEBAND
            //
            if(AXIS_TWSBAND_ENABLE > 0) begin
                initial
                    stage_in_axis_twsband[stage_i]          = {AXIS_TWSBAND_WIDTH{1'b0}};

                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_in_axis_twsband[stage_i]      <= {AXIS_TWSBAND_WIDTH{1'b0}};
                    else
                        if(relay_in_axis_tready[stage_i] == 1'b1)
                            stage_in_axis_twsband[stage_i]  <= s_axis_twsband[stage_i * AXIS_TWSBAND_WIDTH +: AXIS_TWSBAND_WIDTH];
            end
        end
    endgenerate

    // PRESENT STAGE
    //
    initial
    begin
        grant_axis_src                                      <= 0;
        stage_grant_axis_src                                <= 0;
    end

    generate
        case(ARB_HEURISTICS)
            ARB_PRIORITY:
            begin
                genvar arb_i;

                wire [GetWidth(NUM_WAY - 1) - 1 : 0]        arb_grant_pass[NUM_WAY : 0];

                assign arb_grant_pass[NUM_WAY]              = 0;

                for(arb_i = 0; arb_i < NUM_WAY; arb_i = arb_i + 1)
                    assign arb_grant_pass[arb_i]            = ({{NUM_WAY - 1{1'b0}}, 1'b1} << arb_i) & s_axis_tvalid
                                                                ? arb_i : arb_grant_pass[arb_i + 1];

                always @(*)
                    grant_axis_src                          = arb_grant_pass[0];

                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_grant_axis_src                <= 0;
                    else
                        if(arb_valid == 1'b1)
                            stage_grant_axis_src            <= grant_axis_src;
            end
            ARB_ROUNDROBIN:
                always @(posedge clk or negedge aresetn) begin
                    if(aresetn == 1'b0) begin
                        grant_axis_src                      <= 0;
                        stage_grant_axis_src                <= 0;
                    end
                    else
                        if(arb_valid == 1'b1) begin
                            grant_axis_src                  <= (grant_axis_src + 1) % NUM_WAY;
                            stage_grant_axis_src            <= grant_axis_src;
                        end
                end
        endcase
    endgenerate

    generate
        genvar fsm_i;

        for(fsm_i = 0; fsm_i < NUM_WAY; fsm_i = fsm_i + 1) begin
            initial
                STATE[fsm_i]                                = STATE_RESET;

            always @(posedge clk or negedge aresetn)
            begin
                if(aresetn == 1'b0)
                    STATE[fsm_i]                            <= STATE_RESET;
                else begin
                    case(STATE[fsm_i])
                        STATE_RESET:
                            STATE[fsm_i]                    <= STATE_IDLE;
                        STATE_IDLE:
                            if(s_axis_tvalid[fsm_i] == 1'b1) begin
                                if(grant_axis_src == fsm_i && arb_valid == 1'b1) begin
                                    if(s_axis_tlast[fsm_i] == 1'b1)
                                        STATE[fsm_i]        <= STATE_ONESHOT;
                                    else
                                        STATE[fsm_i]        <= STATE_PASSTHROUGH;
                                end
                                else
                                    STATE[fsm_i]            <= STATE_WAIT;
                            end
                        STATE_WAIT:
                            if(grant_axis_src == fsm_i && arb_valid == 1'b1) begin
                                if(stage_in_axis_tvalid[fsm_i] == 1'b1 && stage_in_axis_tlast[fsm_i] == 1'b1)
                                    STATE[fsm_i]            <= STATE_LAST;
                                else
                                    STATE[fsm_i]            <= STATE_PASSTHROUGH;
                            end
                        STATE_PASSTHROUGH:
                            if(m_axis_tready == 1'b1)
                                if(s_axis_tvalid[fsm_i] == 1'b1 && s_axis_tlast[fsm_i] == 1'b1)
                                    STATE[fsm_i]            <= STATE_LAST;
                        STATE_ONESHOT, STATE_LAST:
                            if(m_axis_tready == 1'b1)
                                if(s_axis_tvalid[fsm_i] == 1'b1) begin
                                    if(grant_axis_src == fsm_i && arb_valid == 1'b1) begin
                                        if(s_axis_tlast[fsm_i] == 1'b1)
                                            STATE[fsm_i]    <= STATE_ONESHOT;
                                        else
                                            STATE[fsm_i]    <= STATE_PASSTHROUGH;
                                    end
                                    else
                                        STATE[fsm_i]        <= STATE_WAIT;
                                end
                                else
                                    STATE[fsm_i]            <= STATE_IDLE;
                    endcase
                end
            end

            if(AXIS_TDEST_EXT > 0) begin
                initial
                    stage_in_axis_tdest[fsm_i]              = {AXIS_TDEST_WIDTH{1'b0}};

                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_in_axis_tdest[fsm_i]          <= {AXIS_TDEST_WIDTH{1'b0}};
                    else
                        if(arb_ready[fsm_i] == 1'b1)
                            stage_in_axis_tdest[fsm_i]      <= s_axis_tdest[fsm_i * AXIS_TDEST_WIDTH +: AXIS_TDEST_WIDTH];
            end

            // STAGING OF CUSTOM TRANSACTION-BASED SIDEBAND
            //
            if(AXIS_TSBAND_ENABLE > 0) begin
                initial
                    stage_in_axis_tsband[fsm_i]             = {AXIS_TSBAND_WIDTH{1'b0}};

                always @(posedge clk or negedge aresetn)
                    if(aresetn == 1'b0)
                        stage_in_axis_tsband[fsm_i]         <= {AXIS_TSBAND_WIDTH{1'b0}};
                    else
                        if(arb_ready[fsm_i] == 1'b1)
                            stage_in_axis_tsband[fsm_i]     <= s_axis_tsband[fsm_i * AXIS_TSBAND_WIDTH +: AXIS_TSBAND_WIDTH];
            end

            assign arb_ready[fsm_i]                         = STATE[fsm_i] == STATE_IDLE || (STATE[fsm_i] == STATE_ONESHOT
                                                                || STATE[fsm_i] == STATE_LAST) && m_axis_tready == 1'b1;
        end
    endgenerate

    assign arb_valid                                        = arb_ready[stage_grant_axis_src];

    assign m_axis_tdata                                     = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? stage_in_axis_tdata[stage_grant_axis_src]
                                                                : {AXIS_TDATA_WIDTH{8'h0}};
    assign m_axis_tkeep                                     = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? stage_in_axis_tkeep[stage_grant_axis_src]
                                                                : {AXIS_TDATA_WIDTH{1'b0}};
    assign m_axis_tlast                                     = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? stage_in_axis_tlast[stage_grant_axis_src] : 1'b0;
    assign m_axis_tvalid                                    = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? stage_in_axis_tvalid[stage_grant_axis_src] : 1'b0;

    generate
        if(AXIS_TDEST_EXT > 0)
            assign m_axis_tdest                             = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? stage_in_axis_tdest[stage_grant_axis_src]
                                                                : {AXIS_TDEST_WIDTH{1'b0}};
        else
            assign m_axis_tdest                             = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? {{AXIS_TDEST_WIDTH - GetWidth(NUM_WAY - 1){1'b0}}, stage_grant_axis_src}
                                                                : {AXIS_TDEST_WIDTH{1'b0}};

        if(AXIS_TSBAND_ENABLE > 0)
            assign m_axis_tsband                            = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? stage_in_axis_tsband[stage_grant_axis_src]
                                                                : {AXIS_TSBAND_WIDTH{1'b0}};

        if(AXIS_TWSBAND_ENABLE > 0)
            assign m_axis_twsband                           = STATE[stage_grant_axis_src] != STATE_RESET
                                                                && STATE[stage_grant_axis_src] != STATE_IDLE
                                                                ? stage_in_axis_twsband[stage_grant_axis_src]
                                                                : {AXIS_TWSBAND_WIDTH{1'b0}};
    endgenerate

    generate
        genvar grant_i;

        for(grant_i = 0; grant_i < NUM_WAY; grant_i = grant_i + 1) begin
            assign relay_in_axis_tready[grant_i]            = STATE[grant_i] != STATE_RESET && STATE[grant_i] != STATE_WAIT
                                                                ? STATE[grant_i] == STATE_IDLE ? 1'b1 : m_axis_tready : 1'b0;
            assign s_axis_tready[grant_i]                   = relay_in_axis_tready[grant_i];
        end
    endgenerate
endmodule