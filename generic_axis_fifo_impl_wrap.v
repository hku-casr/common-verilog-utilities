`timescale 1ns/1ps

/**
 *
 * generic_axis_fifo_impl_wrap.v
 *
 * Short Description: 
 *
 * Long Description: 
 *
 * Target Flows: Simulation, Synthesis and Implementation
 *
 * Target Devices: Alpha-Data ADM-PCIE-7V3 (Xilinx Virtex-7 XC7VX690T)
 *
 * Author: C.-H. Dominic HUNG <chdhung@hku.hk>
 *  Computer Architecture and System Research,
 *  Department of Electrical and Electronic Engineering,
 *  The University of Hong Kong
 *
 */

`default_nettype none

`define INTEGER_WIDTH 4

module generic_axis_fifo_impl_wrap(
        input  wire                                         clk
    );

    // PARAMETERS FOR THE UNIT-UNDER-TEST
    //
    parameter AXIS_TDATA_WIDTH                              = 8;
    parameter DATA_BUFFER_DEPTH                             = 32;

    wire                                                    aresetn;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     s_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     s_axis_tkeep;
    wire                                                    s_axis_tlast;
    wire                                                    s_axis_tready;
    wire                                                    s_axis_tvalid;
    wire [AXIS_TDATA_WIDTH * 8 - 1 : 0]                     m_axis_tdata;
    wire [AXIS_TDATA_WIDTH     - 1 : 0]                     m_axis_tkeep;
    wire                                                    m_axis_tlast;
    wire                                                    m_axis_tready;
    wire                                                    m_axis_tvalid;
    wire [`INTEGER_WIDTH   * 8 - 1 : 0]                     axis_data_count;

// BEGIN

    generic_axis_fifo #(
        .AXIS_TDATA_WIDTH                                   (AXIS_TDATA_WIDTH),
        .DATA_BUFFER_DEPTH                                  (DATA_BUFFER_DEPTH)
    ) UUT (
        .clk                                                (clk),
        .aresetn                                            (aresetn),
        .s_axis_tdata                                       (s_axis_tdata),
        .s_axis_tkeep                                       (s_axis_tkeep),
        .s_axis_tlast                                       (s_axis_tlast),
        .s_axis_tready                                      (s_axis_tready),
        .s_axis_tvalid                                      (s_axis_tvalid),
        .m_axis_tdata                                       (m_axis_tdata),
        .m_axis_tkeep                                       (m_axis_tkeep),
        .m_axis_tlast                                       (m_axis_tlast),
        .m_axis_tready                                      (m_axis_tready),
        .m_axis_tvalid                                      (m_axis_tvalid),
        .axis_data_count                                    (axis_data_count)
    );

    vio_generic_axis_fifo line_keeper(
        .clk                                                (clk),
        .probe_out0                                         (aresetn),
        .probe_out1                                         (s_axis_tdata),
        .probe_out2                                         (s_axis_tkeep),
        .probe_out3                                         (s_axis_tlast),
        .probe_in4                                          (s_axis_tready),
        .probe_out4                                         (s_axis_tvalid),
        .probe_in0                                          (m_axis_tdata),
        .probe_in1                                          (m_axis_tkeep),
        .probe_in2                                          (m_axis_tlast),
        .probe_out5                                         (m_axis_tready),
        .probe_in3                                          (m_axis_tvalid),
        .probe_in5                                          (axis_data_count)
    );

    ila_generic_axis_fifo line_watcher(
        .clk                                                (clk),
        .probe0                                             (aresetn),
        .probe1                                             (s_axis_tdata),
        .probe2                                             (s_axis_tkeep),
        .probe3                                             (s_axis_tlast),
        .probe4                                             (s_axis_tready),
        .probe5                                             (s_axis_tvalid),
        .probe6                                             (m_axis_tdata),
        .probe7                                             (m_axis_tkeep),
        .probe8                                             (m_axis_tlast),
        .probe9                                             (m_axis_tready),
        .probe10                                            (m_axis_tvalid),
        .probe11                                            (axis_data_count)
    );
endmodule